#include "stdafx.h"
#include "Scrabble.h"

static const int32 letterDistribution[ 27u ] = { 9, 2, 2, 4, 12, 2, 3, 2, 9, 1, 1, 4, 2, 6, 8, 2, 1, 6, 4, 6, 4, 2, 2, 1, 2, 1, 2 };
static const int32 letterPointValues[ 26u ] = { 1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10 };

Scrabble::Scrabble( void )
{
	uint32 i;
	m_board = new char[ 225u ];
	for ( i = 0u; i < 225u; ++i )
		m_board[ i ] = NULL_CHAR;
	m_tileBag = new int32[ 27u ];
	for ( i = 0u; i < 27u; ++i )
		m_tileBag[ i ] = letterDistribution[ i ];
}
Scrabble::Scrabble( const Scrabble& _rhs ) :
	m_dict( _rhs.m_dict )
{
	uint32 i;
	m_board = new char[ 225u ];
	for ( i = 0u; i < 225u; ++i )
		m_board[ i ] = _rhs.m_board[ i ];
	m_tileBag = new int32[ 27u ];
	for ( i = 0u; i < 27u; ++i )
		m_tileBag[ i ] = _rhs.m_tileBag[ i ];
}
Scrabble::Scrabble( Scrabble&& _rhs ) :
	m_dict( std::move( _rhs.m_dict ) ),
	m_board( std::move( _rhs.m_board ) ),
	m_tileBag( std::move( _rhs.m_tileBag ) )
{
	_rhs.m_board = nullptr;
	_rhs.m_tileBag = nullptr;
}
Scrabble& Scrabble::operator=( const Scrabble& _rhs )
{
	if ( this != &_rhs )
	{
		uint32 i;
		delete[ ] m_board;
		delete[ ] m_tileBag;
		m_dict = _rhs.m_dict;
		m_board = new char[ 225u ];
		for ( i = 0u; i < 225u; ++i )
			m_board[ i ] = _rhs.m_board[ i ];
		m_tileBag = new int32[ 27u ];
		for ( i = 0u; i < 27u; ++i )
			m_tileBag[ i ] = _rhs.m_tileBag[ i ];
	}
	return *this;
}
Scrabble& Scrabble::operator=( Scrabble&& _rhs )
{
	if ( this != &_rhs )
	{
		delete[ ] m_board;
		delete[ ] m_tileBag;
		m_dict = std::move( _rhs.m_dict );
		m_board = std::move( _rhs.m_board );
		m_tileBag = std::move( _rhs.m_tileBag );
		_rhs.m_board = nullptr;
		_rhs.m_tileBag = nullptr;
	}
	return *this;
}
Scrabble::~Scrabble( void )
{
	delete[ ] m_board;
	delete[ ] m_tileBag;
}
void ImportConformer( String& _str )
{
	_str.OptimizeSpace();
	const uint32 len_ = _str.GetLength();
	for ( uint32 i = 0u; i < len_; ++i )
	{
		char& c_ = _str[ i ];
		if ( c_ >= 'a' && c_ <= 'z' )
			c_ -= ' ';
	}
}
bool ImportValidator( const String& _str )
{
	const uint32 len_ = _str.GetLength();
	if ( len_ < 2u || len_ > 15u )
		return false;
	int32 letterCount[ 26u ] = { 0 };
	uint32 i;
	for ( i = 0u; i < len_; ++i )
	{
		const char& c_ = _str[ i ];
		if ( c_ < 'A' || c_ > 'Z' )
			return false;
		++letterCount[ c_ - 'A' ];
	}
	int32 extraLetters = 0u;
	for ( i = 0u; i < 26u; ++i )
		if ( letterCount[ i ] > letterDistribution[ i ] )
			extraLetters += letterCount[ i ] - letterDistribution[ i ];
	return extraLetters <= letterDistribution[ 26u ];
}
bool CanMake( const String& _str, const int32* _letterTiles,
			  const int32* letterScores = nullptr, int32* _score = nullptr )
{
	int32 letterCount[ 26u ] = { 0 };
	uint32 i;
	const uint32 strlen_ = _str.GetLength();
	for ( i = 0u; i < strlen_; ++i )
		++letterCount[ _str[ i ] - 'A' ];
	int32 extraLetters = 0u;
	for ( i = 0u; i < 26u; ++i )
		if ( letterCount[ i ] > _letterTiles[ i ] )
			extraLetters += letterCount[ i ] - _letterTiles[ i ];
	const bool getscore = nullptr != letterScores && nullptr != _score;
	const bool canmake = extraLetters <= _letterTiles[ 26u ];
	if ( canmake && getscore )
	{
		int32& score_ = *_score;
		score_ = 0;
		int32 letters_[ 26u ];
		for ( i = 0u; i < 26u; ++i )
			letters_[ i ] = _letterTiles[ i ];
		for ( i = 0u; i < strlen_; ++i )
		{
			int32& tile = letters_[ _str[ i ] - 'A' ];
			if ( tile >= 0 )
			{
				--tile;
				score_ += letterScores[ _str[ i ] - 'A' ];
			}
		}
	}
	return canmake;
}
void Scrabble::ImportDictionary( const char* const _file )
{
	m_dict += WordDictionary::Import( _file, ImportConformer, ImportValidator );
}
void Scrabble::ExportDictionary( const char* const _file ) const
{
	WordDictionary::Export( m_dict, _file );
}
void Scrabble::PrintWordsFromLetters( const String& _letters ) const
{
	String letters_ = _letters;
	ImportConformer( letters_ );
	int32 tiles[ 27u ] = { 0 };
	{
		const uint32 len = letters_.GetLength();
		for ( uint32 i = 0u; i < len; ++i )
			if ( letters_[ i ] >= 'A' && letters_[ i ] <= 'Z' )
				++tiles[ letters_[ i ] - 'A' ];
			else
				++tiles[ 26u ];
	}
	const DynArray<String>* table = m_dict.m_words.GetTable();
	const uint32 tablesize = 1995u;
	int32 score;
	for ( uint32 i = 0u; i < tablesize; ++i )
	{
		const DynArray<String>& arr = table[ i ];
		const uint32 arrsize = arr.GetSize();
		for ( uint32 j = 0u; j < arrsize; ++j )
		{
			const String& str = arr[ j ];
			if ( CanMake( str, tiles, letterPointValues, &score ) )
				std::cout << str << " : " << score << std::endl;
		}
	}
}