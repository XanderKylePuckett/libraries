#pragma once
#include "Matrix4x4.h"
#pragma warning(disable:4201)

struct Quaternion
{
	union
	{
		float64 m_arr[ 4 ];
		struct
		{
			float64 a, b, c, d;
		};
		struct
		{
			float64 m_scalar;
			Vector3 m_vector;
		};
		struct
		{
			Vector4 v4;
		};
	};
	static const Quaternion ZERO;
	Quaternion( void );
	Quaternion( float64 );
	Quaternion( const Vector3& );
	Quaternion( float64, const Vector3& );
	Quaternion( float64, float64, float64, float64 );
	Quaternion( const Quaternion& );
	~Quaternion( void );
	Quaternion& operator=( const Quaternion& );
	static Quaternion Slerp( const Quaternion&, const Quaternion&, float64 );
	Quaternion operator+( void ) const;
	Quaternion operator-( void ) const;
	Quaternion operator+( const Quaternion& ) const;
	Quaternion operator-( const Quaternion& ) const;
	Quaternion operator+( const Vector3& ) const;
	Quaternion operator-( const Vector3& ) const;
	Quaternion operator+( float64 ) const;
	Quaternion operator-( float64 ) const;
	Quaternion operator*( const Quaternion& ) const;
	Quaternion operator/( const Quaternion& ) const;
	Quaternion operator*( float64 ) const;
	Quaternion operator/( float64 ) const;
	Quaternion& operator+=( const Quaternion& );
	Quaternion& operator-=( const Quaternion& );
	Quaternion& operator+=( float64 );
	Quaternion& operator-=( float64 );
	Quaternion& operator+=( const Vector3& );
	Quaternion& operator-=( const Vector3& );
	Quaternion& operator*=( const Quaternion& );
	Quaternion& operator/=( const Quaternion& );
	Quaternion& operator*=( float64 );
	Quaternion& operator/=( float64 );
	Quaternion GetConjugate( void ) const;
	float64 GetNorm( void ) const;
	float64 GetNormSq( void ) const;
	Quaternion GetInverse( void ) const;
	static Quaternion GetConjugate( const Quaternion& );
	static float64 GetNorm( const Quaternion& );
	static float64 GetNormSq( const Quaternion& );
	static Quaternion GetInverse( const Quaternion& );
	Quaternion& SetToConjugate( void );
	Quaternion& SetToInverse( void );
	static Quaternion& SetToConjugate( Quaternion& );
	static Quaternion& SetToInverse( Quaternion& );
	bool operator==( const Quaternion& ) const;
	bool operator!=( const Quaternion& ) const;
	Matrix3x3 GetRotationMatrix3x3( void ) const;
	Matrix4x4 GetRotationMatrix4x4( void ) const;
	static Matrix3x3 GetRotationMatrix3x3( const Quaternion& );
	static Matrix4x4 GetRotationMatrix4x4( const Quaternion& );
	static Quaternion GetQuaternionFromMatrix( const Matrix3x3& );
	static Quaternion GetQuaternionFromMatrix( const Matrix4x4& );
	static Quaternion AxisAngleRotation( const Vector3& axis, float64 angle );
};

Quaternion operator+( float64, const Quaternion& );
Quaternion operator-( float64, const Quaternion& );
Quaternion operator+( const Vector3&, const Quaternion& );
Quaternion operator-( const Vector3&, const Quaternion& );
Quaternion operator+( const Vector3&, float64 );
Quaternion operator-( const Vector3&, float64 );
Quaternion operator+( float64, const Vector3& );
Quaternion operator-( float64, const Vector3& );
Quaternion operator*( float64, const Quaternion& );
Quaternion operator/( float64, const Quaternion& );
std::ostream& operator<<( std::ostream&, const Quaternion& );