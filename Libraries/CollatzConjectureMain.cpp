#include "stdafx.h"
#include "Random.h"

int CollatzConjectureMain( int, char** )
{
	for ( uint32 i = 1u; i <= 100u; ++i )
	{
		uint64 n = g_random.GetRandom( 2ui64, 0x000000000000ffffui64 ), j;
		std::cout << i << ": N = " << n;
		for ( j = 0ui64; 1ui64 != n; ++j )
			if ( 0x0000000000000001ui64 & n )
				n += ( n << 1 ) + 1ui64;
			else
				n >>= 1;
		std::cout << ", " << j << " iterations" << std::endl;
	}
	return EXIT_SUCCESS;
}