#pragma once
#include "Vector3.h"
#include "Color.h"
#pragma warning(disable:4201)

struct PositionColorVertex
{
	Vector3 m_position;
	Color m_color;
	PositionColorVertex( void );
	PositionColorVertex( const Vector3&, const Color& = Color::WHITE );
	PositionColorVertex( const PositionColorVertex& );
	bool operator==( const PositionColorVertex& ) const;
	bool operator!=( const PositionColorVertex& ) const;
};

struct Line
{
	PositionColorVertex m_start, m_end;
	Line( void );
	Line( const PositionColorVertex&, const PositionColorVertex& );
	Line( const Line& );
	bool operator==( const Line& ) const;
	bool operator!=( const Line& ) const;
};

struct RasterPoint
{
	int32 m_posX, m_posY;
	RasterPoint( void );
	RasterPoint( int32, int32 );
	RasterPoint( const RasterPoint& );
	bool operator==( const RasterPoint& ) const;
	bool operator!=( const RasterPoint& ) const;
};

struct RasterLine
{
	RasterPoint m_start, m_end;
	RasterLine( void );
	RasterLine( const RasterPoint&, const RasterPoint& );
	RasterLine( const RasterLine& );
	bool operator==( const RasterLine& ) const;
	bool operator!=( const RasterLine& ) const;
};

struct PositionColorTriangle
{
	PositionColorVertex m_vertA, m_vertB, m_vertC;
	PositionColorTriangle( void );
	PositionColorTriangle( const PositionColorVertex&, const PositionColorVertex&, const PositionColorVertex& );
	PositionColorTriangle( const PositionColorTriangle& );
	bool operator==( const PositionColorTriangle& ) const;
	bool operator!=( const PositionColorTriangle& ) const;
};
