#pragma once
#include "Color.h"
#include "String.h"

#define CONST_DEFINE(X)extern const float64&Const##X;extern const float32&ConstF##X
CONST_DEFINE( _pi );
CONST_DEFINE( _piDiv2 );
CONST_DEFINE( _piDiv4 );
CONST_DEFINE( _piDiv180 );
CONST_DEFINE( _180DivPi );
CONST_DEFINE( _1DivPi );
CONST_DEFINE( _2pi );
CONST_DEFINE( _e );
CONST_DEFINE( _sqrt2 );
CONST_DEFINE( _sqrt2Div2 );
CONST_DEFINE( _sqrt3 );
CONST_DEFINE( _sqrt3Div2 );
CONST_DEFINE( _sqrt3Div3 );
CONST_DEFINE( _1Div6 );
CONST_DEFINE( _1Div3 );
CONST_DEFINE( _2Div3 );
CONST_DEFINE( _goldenRatio );
CONST_DEFINE( _wau );
CONST_DEFINE( _12thRoot2 );
CONST_DEFINE( _coulombs );
CONST_DEFINE( _gravitational );
CONST_DEFINE( _permittivity );
CONST_DEFINE( _permeability );
CONST_DEFINE( _lightSpeed );
CONST_DEFINE( _lightSpeedSquared );
CONST_DEFINE( _elementaryCharge );
CONST_DEFINE( _avogadro );
#undef CONST_DEFINE

class Math
{
public:
	static int8 Abs( int8 );
	static int16 Abs( int16 );
	static int32 Abs( int32 );
	static int64 Abs( int64 );
	static float32 Abs( float32 );
	static float64 Abs( float64 );
	static int8 Min( int8, int8 );
	static int16 Min( int16, int16 );
	static int32 Min( int32, int32 );
	static int64 Min( int64, int64 );
	static uint8 Min( uint8, uint8 );
	static uint16 Min( uint16, uint16 );
	static uint32 Min( uint32, uint32 );
	static uint64 Min( uint64, uint64 );
	static float32 Min( float32, float32 );
	static float64 Min( float64, float64 );
	static int8 Max( int8, int8 );
	static int16 Max( int16, int16 );
	static int32 Max( int32, int32 );
	static int64 Max( int64, int64 );
	static uint8 Max( uint8, uint8 );
	static uint16 Max( uint16, uint16 );
	static uint32 Max( uint32, uint32 );
	static uint64 Max( uint64, uint64 );
	static float32 Max( float32, float32 );
	static float64 Max( float64, float64 );
	static int8 Min( int8, int8, int8 );
	static int16 Min( int16, int16, int16 );
	static int32 Min( int32, int32, int32 );
	static int64 Min( int64, int64, int64 );
	static uint8 Min( uint8, uint8, uint8 );
	static uint16 Min( uint16, uint16, uint16 );
	static uint32 Min( uint32, uint32, uint32 );
	static uint64 Min( uint64, uint64, uint64 );
	static float32 Min( float32, float32, float32 );
	static float64 Min( float64, float64, float64 );
	static int8 Max( int8, int8, int8 );
	static int16 Max( int16, int16, int16 );
	static int32 Max( int32, int32, int32 );
	static int64 Max( int64, int64, int64 );
	static uint8 Max( uint8, uint8, uint8 );
	static uint16 Max( uint16, uint16, uint16 );
	static uint32 Max( uint32, uint32, uint32 );
	static uint64 Max( uint64, uint64, uint64 );
	static float32 Max( float32, float32, float32 );
	static float64 Max( float64, float64, float64 );
	static int8 Min( const int8* arr, uint32 siz );
	static int16 Min( const int16* arr, uint32 siz );
	static int32 Min( const int32* arr, uint32 siz );
	static int64 Min( const int64* arr, uint32 siz );
	static uint8 Min( const uint8* arr, uint32 siz );
	static uint16 Min( const uint16* arr, uint32 siz );
	static uint32 Min( const uint32* arr, uint32 siz );
	static uint64 Min( const uint64* arr, uint32 siz );
	static float32 Min( const float32* arr, uint32 siz );
	static float64 Min( const float64* arr, uint32 siz );
	static int8 Max( const int8* arr, uint32 siz );
	static int16 Max( const int16* arr, uint32 siz );
	static int32 Max( const int32* arr, uint32 siz );
	static int64 Max( const int64* arr, uint32 siz );
	static uint8 Max( const uint8* arr, uint32 siz );
	static uint16 Max( const uint16* arr, uint32 siz );
	static uint32 Max( const uint32* arr, uint32 siz );
	static uint64 Max( const uint64* arr, uint32 siz );
	static float32 Max( const float32* arr, uint32 siz );
	static float64 Max( const float64* arr, uint32 siz );
	static int8 Avg( const int8* arr, uint32 siz );
	static int16 Avg( const int16* arr, uint32 siz );
	static int32 Avg( const int32* arr, uint32 siz );
	static int64 Avg( const int64* arr, uint32 siz );
	static uint8 Avg( const uint8* arr, uint32 siz );
	static uint16 Avg( const uint16* arr, uint32 siz );
	static uint32 Avg( const uint32* arr, uint32 siz );
	static uint64 Avg( const uint64* arr, uint32 siz );
	static float32 Avg( const float32* arr, uint32 siz );
	static float64 Avg( const float64* arr, uint32 siz );
	static int8 Lerp( int8 a, int8 b, float32 r );
	static int16 Lerp( int16 a, int16 b, float32 r );
	static int32 Lerp( int32 a, int32 b, float32 r );
	static int64 Lerp( int64 a, int64 b, float32 r );
	static uint8 Lerp( uint8 a, uint8 b, float32 r );
	static uint16 Lerp( uint16 a, uint16 b, float32 r );
	static uint32 Lerp( uint32 a, uint32 b, float32 r );
	static uint64 Lerp( uint64 a, uint64 b, float32 r );
	static float32 Lerp( float32 a, float32 b, float32 r );
	static float64 Lerp( float64 a, float64 b, float32 r );
	static int8 Lerp( int8 a, int8 b, float64 r );
	static int16 Lerp( int16 a, int16 b, float64 r );
	static int32 Lerp( int32 a, int32 b, float64 r );
	static int64 Lerp( int64 a, int64 b, float64 r );
	static uint8 Lerp( uint8 a, uint8 b, float64 r );
	static uint16 Lerp( uint16 a, uint16 b, float64 r );
	static uint32 Lerp( uint32 a, uint32 b, float64 r );
	static uint64 Lerp( uint64 a, uint64 b, float64 r );
	static float32 Lerp( float32 a, float32 b, float64 r );
	static float64 Lerp( float64 a, float64 b, float64 r );
	static float32 Sqrt( float32 );
	static float64 Sqrt( float64 );
	static float32 DegreesToRadians( float32 deg );
	static float64 DegreesToRadians( float64 deg );
	static float32 RadiansToDegrees( float32 rad );
	static float64 RadiansToDegrees( float64 rad );
	static Color Lerp( const Color& a, const Color& b, float32 r );
	static Color Lerp( const Color& a, const Color& b, float64 r );
	static Color Lerp_SqrtFormula( const Color& a, const Color& b, float32 r );
	static Color Lerp_SqrtFormula( const Color& a, const Color& b, float64 r );
	static float32 Clamp01( float32 x );
	static float64 Clamp01( float64 x );
	static float32 Wrap01( float32 x );
	static float64 Wrap01( float64 x );
	static float32 Wrap01ex( float32 x );
	static float64 Wrap01ex( float64 x );
	static float64 Sin( float64 deg );
	static float64 Cos( float64 deg );
	static float32 Sin( float32 deg );
	static float32 Cos( float32 deg );
	static float64 Sin_Rad( float64 );
	static float64 Cos_Rad( float64 );
	static float32 Sin_Rad( float32 );
	static float32 Cos_Rad( float32 );
	static uint16 ReverseByteOrder( uint16 );
	static uint32 ReverseByteOrder( uint32 );
	static uint64 ReverseByteOrder( uint64 );
	static uint128 HexStringToUint128( const String& );
	static uint128 DecStringToUint128( const String& );
	static uint128 BinStringToUint128( const String& );
	static uint128 OctStringToUint128( const String& );
};