#pragma once
template <typename T>
class Stack
{
private:
	struct Node
	{
		T m_data;
		typename Stack<T>::Node* m_next;
		Node( void );
		Node( const T&, typename Stack<T>::Node* = nullptr );
		Node( T&&, typename Stack<T>::Node* = nullptr );
		Node( const typename Stack<T>::Node& );
		Node( typename Stack<T>::Node&& );
		typename Stack<T>::Node& operator=( const typename Stack<T>::Node& );
		typename Stack<T>::Node& operator=( typename Stack<T>::Node&& );
		~Node( void );
	};
	uint64 m_size;
	typename Stack<T>::Node* m_top;
public:
	struct ConstIterator;
	struct Iterator
	{
		friend class Stack<T>;
	private:
		typename Stack<T>::Node* m_curr;
		typename Stack<T>::Node* m_prev;
		Iterator( typename Stack<T>::Node*, typename Stack<T>::Node* = nullptr );
	public:
		Iterator( void );
		Iterator( const typename Stack<T>::Iterator& );
		Iterator( typename Stack<T>::Iterator&& );
		typename Stack<T>::Iterator& operator=( const typename Stack<T>::Iterator& );
		typename Stack<T>::Iterator& operator=( typename Stack<T>::Iterator&& );
		~Iterator( void );
		typename Stack<T>::Iterator& Next( void );
		T& Get( void );
		const T& Get( void ) const;
		T& operator*( void );
		const T& operator*( void ) const;
		bool End( void ) const;
		bool operator!( void ) const;
		typename Stack<T>::Iterator& operator++( void );
		typename Stack<T>::Iterator operator++( int );
		typename Stack<T>::Iterator& operator+=( uint64 );
		typename Stack<T>::ConstIterator GetConstIterator( void ) const;
	};
	struct ConstIterator
	{
		friend class Stack<T>;
	private:
		const typename Stack<T>::Node* m_curr;
		const typename Stack<T>::Node* m_prev;
		ConstIterator( const typename Stack<T>::Node*, const typename Stack<T>::Node* = nullptr );
	public:
		ConstIterator( void );
		ConstIterator( const typename Stack<T>::ConstIterator& );
		ConstIterator( typename Stack<T>::ConstIterator&& );
		typename Stack<T>::ConstIterator& operator=( const typename Stack<T>::ConstIterator& );
		typename Stack<T>::ConstIterator& operator=( typename Stack<T>::ConstIterator&& );
		ConstIterator( const typename Stack<T>::Iterator& );
		ConstIterator( typename Stack<T>::Iterator&& );
		typename Stack<T>::ConstIterator& operator=( const typename Stack<T>::Iterator& );
		typename Stack<T>::ConstIterator& operator=( typename Stack<T>::Iterator&& );
		~ConstIterator( void );
		typename Stack<T>::ConstIterator& Next( void );
		const T& Get( void ) const;
		const T& operator*( void ) const;
		bool End( void ) const;
		bool operator!( void ) const;
		typename Stack<T>::ConstIterator& operator++( void );
		typename Stack<T>::ConstIterator operator++( int );
		typename Stack<T>::ConstIterator& operator+=( uint64 );
	};
	Stack( void );
	Stack( const Stack<T>& );
	Stack( Stack<T>&& );
	Stack<T>& operator=( const Stack<T>& );
	Stack<T>& operator=( Stack<T>&& );
	~Stack( void );
	Stack<T>& Pop( void );
	Stack<T>& Pop( uint64 );
	Stack<T>& Push( const T& );
	Stack<T>& Push( T&& );
	Stack<T>& Push( const T*, uint64 );
	T& Top( void );
	const T& Top( void ) const;
	uint64 GetSize( void ) const;
	Stack<T>& Clear( void );
	typename Stack<T>::Iterator Begin( void );
	typename Stack<T>::ConstIterator Begin( void ) const;
	typename Stack<T>::Iterator& Remove( typename Stack<T>::Iterator& );
	typename Stack<T>::Iterator& Remove( typename Stack<T>::Iterator&, uint64 );
	typename Stack<T>::Iterator& Insert( typename Stack<T>::Iterator&, const T& );
	typename Stack<T>::Iterator& Insert( typename Stack<T>::Iterator&, T&& );
	typename Stack<T>::Iterator& Insert( typename Stack<T>::Iterator&, const T*, uint64 );
	Stack<T>& operator+=( const T& );
	Stack<T>& operator+=( T&& );
	Stack<T>& operator--( void );
	Stack<T> operator--( int );
	Stack<T>& operator-=( uint64 );
};
template <typename T>
Stack<T>::Node::Node( void ) :
	m_next( nullptr )
{
}
template <typename T>
Stack<T>::Node::Node( const T& _data, typename Stack<T>::Node* _next ) :
	m_data( _data ),
	m_next( _next )
{
}
template <typename T>
Stack<T>::Node::Node( T&& _data, typename Stack<T>::Node* _next ) :
	m_data( std::move( _data ) ),
	m_next( _next )
{
}
template <typename T>
Stack<T>::Node::Node( const typename Stack<T>::Node& _rhs ) :
	m_data( _rhs.m_data ),
	m_next( _rhs.m_next )
{
}
template <typename T>
Stack<T>::Node::Node( typename Stack<T>::Node&& _rhs ) :
	m_data( std::move( _rhs.m_data ) ),
	m_next( std::move( _rhs.m_next ) )
{
	_rhs.m_next = nullptr;
}
template <typename T>
typename Stack<T>::Node& Stack<T>::Node::operator=( const typename Stack<T>::Node& _rhs )
{
	if ( this != &_rhs )
	{
		m_data = _rhs.m_data;
		m_next = _rhs.m_next;
	}
	return *this;
}
template <typename T>
typename Stack<T>::Node& Stack<T>::Node::operator=( typename Stack<T>::Node&& _rhs )
{
	if ( this != &_rhs )
	{
		m_data = std::move( _rhs.m_data );
		m_next = std::move( _rhs.m_next );
		_rhs.m_next = nullptr;
	}
	return *this;
}
template <typename T>
Stack<T>::Node::~Node( void )
{
}
template <typename T>
Stack<T>::Iterator::Iterator( typename Stack<T>::Node* _curr, typename Stack<T>::Node* _prev ) :
	m_curr( _curr ),
	m_prev( _prev )
{
}
template <typename T>
Stack<T>::Iterator::Iterator( void ) :
	m_curr( nullptr ),
	m_prev( nullptr )
{
}
template <typename T>
Stack<T>::Iterator::Iterator( const typename Stack<T>::Iterator& _rhs ) :
	m_curr( _rhs.m_curr ),
	m_prev( _rhs.m_prev )
{
}
template <typename T>
Stack<T>::Iterator::Iterator( typename Stack<T>::Iterator&& _rhs ) :
	m_curr( std::move( _rhs.m_curr ) ),
	m_prev( std::move( _rhs.m_prev ) )
{
	_rhs.m_curr = nullptr;
	_rhs.m_prev = nullptr;
}
template <typename T>
typename Stack<T>::Iterator& Stack<T>::Iterator::operator=( const typename Stack<T>::Iterator& _rhs )
{
	if ( this != &_rhs )
	{
		m_curr = _rhs.m_curr;
		m_prev = _rhs.m_prev;
	}
	return *this;
}
template <typename T>
typename Stack<T>::Iterator& Stack<T>::Iterator::operator=( typename Stack<T>::Iterator&& _rhs )
{
	if ( this != &_rhs )
	{
		m_curr = std::move( _rhs.m_curr );
		m_prev = std::move( _rhs.m_prev );
		_rhs.m_curr = nullptr;
		_rhs.m_prev = nullptr;
	}
	return *this;
}
template <typename T>
Stack<T>::Iterator::~Iterator( void )
{
}
template <typename T>
typename Stack<T>::Iterator& Stack<T>::Iterator::Next( void )
{
	if ( nullptr == m_curr )
		m_prev = nullptr;
	else
	{
		m_prev = m_curr;
		m_curr = m_curr->m_next;
	}
	return *this;
}
template <typename T>
T& Stack<T>::Iterator::Get( void )
{
	return m_curr->m_data;
}
template <typename T>
const T& Stack<T>::Iterator::Get( void ) const
{
	return m_curr->m_data;
}
template <typename T>
T& Stack<T>::Iterator::operator*( void )
{
	return m_curr->m_data;
}
template <typename T>
const T& Stack<T>::Iterator::operator*( void ) const
{
	return m_curr->m_data;
}
template <typename T>
bool Stack<T>::Iterator::End( void ) const
{
	return nullptr == m_curr;
}
template <typename T>
bool Stack<T>::Iterator::operator!( void ) const
{
	return nullptr == m_curr;
}
template <typename T>
typename Stack<T>::Iterator& Stack<T>::Iterator::operator++( void )
{
	return Next();
}
template <typename T>
typename Stack<T>::Iterator Stack<T>::Iterator::operator++( int )
{
	typename Stack<T>::Iterator ret_ = *this;
	Next();
	return ret_;
}
template <typename T>
typename Stack<T>::Iterator& Stack<T>::Iterator::operator+=( uint64 _num )
{
	for ( uint64 i = 0ull; i < _num; ++i )
		Next();
	return *this;
}
template <typename T>
typename Stack<T>::ConstIterator Stack<T>::Iterator::GetConstIterator( void ) const
{
	return Stack<T>::ConstIterator( m_curr, m_prev );
}
template <typename T>
Stack<T>::ConstIterator::ConstIterator( const typename Stack<T>::Node* _curr, const typename Stack<T>::Node* _prev ) :
	m_curr( _curr ),
	m_prev( _prev )
{
}
template <typename T>
Stack<T>::ConstIterator::ConstIterator( void ) :
	m_curr( nullptr ),
	m_prev( nullptr )
{
}
template <typename T>
Stack<T>::ConstIterator::ConstIterator( const typename Stack<T>::ConstIterator& _rhs ) :
	m_curr( _rhs.m_curr ),
	m_prev( _rhs.m_prev )
{
}
template <typename T>
Stack<T>::ConstIterator::ConstIterator( typename Stack<T>::ConstIterator&& _rhs ) :
	m_curr( std::move( _rhs.m_curr ) ),
	m_prev( std::move( _rhs.m_prev ) )
{
	_rhs.m_curr = nullptr;
	_rhs.m_prev = nullptr;
}
template <typename T>
typename Stack<T>::ConstIterator& Stack<T>::ConstIterator::operator=( const typename Stack<T>::ConstIterator& _rhs )
{
	if ( this != &_rhs )
	{
		m_curr = _rhs.m_curr;
		m_prev = _rhs.m_prev;
	}
	return *this;
}
template <typename T>
typename Stack<T>::ConstIterator& Stack<T>::ConstIterator::operator=( typename Stack<T>::ConstIterator&& _rhs )
{
	if ( this != &_rhs )
	{
		m_curr = std::move( _rhs.m_curr );
		m_prev = std::move( _rhs.m_prev );
		_rhs.m_curr = nullptr;
		_rhs.m_prev = nullptr;
	}
	return *this;
}
template <typename T>
Stack<T>::ConstIterator::ConstIterator( const typename Stack<T>::Iterator& _rhs ) :
	m_curr( _rhs.m_curr ),
	m_prev( _rhs.m_prev )
{
}
template <typename T>
Stack<T>::ConstIterator::ConstIterator( typename Stack<T>::Iterator&& _rhs ) :
	m_curr( std::move( _rhs.m_curr ) ),
	m_prev( std::move( _rhs.m_prev ) )
{
	_rhs.m_curr = nullptr;
	_rhs.m_prev = nullptr;
}
template <typename T>
typename Stack<T>::ConstIterator& Stack<T>::ConstIterator::operator=( const typename Stack<T>::Iterator& _rhs )
{
	m_curr = _rhs.m_curr;
	m_prev = _rhs.m_prev;
	return *this;
}
template <typename T>
typename Stack<T>::ConstIterator& Stack<T>::ConstIterator::operator=( typename Stack<T>::Iterator&& _rhs )
{
	m_curr = std::move( _rhs.m_curr );
	m_prev = std::move( _rhs.m_prev );
	_rhs.m_curr = nullptr;
	_rhs.m_prev = nullptr;
	return *this;
}
template <typename T>
Stack<T>::ConstIterator::~ConstIterator( void )
{
}
template <typename T>
typename Stack<T>::ConstIterator& Stack<T>::ConstIterator::Next( void )
{
	if ( nullptr == m_curr )
		m_prev = nullptr;
	else
	{
		m_prev = m_curr;
		m_curr = m_curr->m_next;
	}
	return *this;
}
template <typename T>
const T& Stack<T>::ConstIterator::Get( void ) const
{
	return m_curr->m_data;
}
template <typename T>
const T& Stack<T>::ConstIterator::operator*( void ) const
{
	return m_curr->m_data;
}
template <typename T>
bool Stack<T>::ConstIterator::End( void ) const
{
	return nullptr == m_curr;
}
template <typename T>
bool Stack<T>::ConstIterator::operator!( void ) const
{
	return nullptr == m_curr;
}
template <typename T>
typename Stack<T>::ConstIterator& Stack<T>::ConstIterator::operator++( void )
{
	return Next();
}
template <typename T>
typename Stack<T>::ConstIterator Stack<T>::ConstIterator::operator++( int )
{
	typename Stack<T>::Iterator ret_ = *this;
	Next();
	return ret_;
}
template <typename T>
typename Stack<T>::ConstIterator& Stack<T>::ConstIterator::operator+=( uint64 _num )
{
	for ( uint64 i = 0ull; i < _num; ++i )
		Next();
	return *this;
}
template <typename T>
Stack<T>::Stack( void ) :
	m_top( nullptr ),
	m_size( 0ull )
{
}
template <typename T>
Stack<T>::Stack( const Stack<T>& _rhs ) :
	m_top( nullptr ),
	m_size( 0ull )
{
	if ( nullptr != _rhs.m_top )
		for ( typename Stack<T>::ConstIterator iter = _rhs.Begin(); !iter.End(); iter.Next() )
			Push( iter.Get() );
}
template <typename T>
Stack<T>::Stack( Stack<T>&& _rhs ) :
	m_top( std::move( _rhs.m_top ) ),
	m_size( std::move( _rhs.m_size ) )
{
	_rhs.m_top = nullptr;
	_rhs.m_size = 0ull;
}
template <typename T>
Stack<T>& Stack<T>::operator=( const Stack<T>& _rhs )
{
	if ( this != &_rhs )
	{
		Clear();
		if ( nullptr != _rhs.m_top )
			for ( typename Stack<T>::ConstIterator iter = _rhs.Begin(); !iter.End(); iter.Next() )
				Push( iter.Get() );
	}
	return *this;
}
template <typename T>
Stack<T>& Stack<T>::operator=( Stack<T>&& _rhs )
{
	if ( this != &_rhs )
	{
		Clear();
		m_top = std::move( _rhs.m_top );
		m_size = std::move( _rhs.m_size );
		_rhs.m_top = nullptr;
		_rhs.m_size = 0ull;
	}
	return *this;
}
template <typename T>
Stack<T>::~Stack( void )
{
	Clear();
}
template <typename T>
Stack<T>& Stack<T>::Pop( void )
{
	if ( nullptr != m_top )
	{
		typename Stack<T>::Node* delNode_ = m_top;
		m_top = m_top->m_next;
		delete delNode_;
		--m_size;
	}
	return *this;
}
template <typename T>
Stack<T>& Stack<T>::Pop( uint64 _num )
{
	for ( uint64 i = 0ull; i < _num; ++i )
		Pop();
	return *this;
}
template <typename T>
Stack<T>& Stack<T>::Push( const T& _data )
{
	m_top = new Stack<T>::Node( _data, m_top );
	++m_size;
	return *this;
}
template <typename T>
Stack<T>& Stack<T>::Push( T&& _data )
{
	m_top = new Stack<T>::Node( std::move( _data ), m_top );
	++m_size;
	return *this;
}
template <typename T>
Stack<T>& Stack<T>::Push( const T* _arr, uint64 _siz )
{
	for ( uint64 i = 0ull; i < _siz; ++i )
		Push( _arr[ i ] );
	return *this;
}
template <typename T>
T& Stack<T>::Top( void )
{
	return m_top->m_data;
}
template <typename T>
const T& Stack<T>::Top( void ) const
{
	return m_top->m_data;
}
template <typename T>
uint64 Stack<T>::GetSize( void ) const
{
	return m_size;
}
template <typename T>
Stack<T>& Stack<T>::Clear( void )
{
	while ( nullptr != m_top )
		Pop();
	return *this;
}
template <typename T>
typename Stack<T>::Iterator Stack<T>::Begin( void )
{
	return Stack<T>::Iterator( m_top );
}
template <typename T>
typename Stack<T>::ConstIterator Stack<T>::Begin( void ) const
{
	return Stack<T>::ConstIterator( m_top );
}
template <typename T>
typename Stack<T>::Iterator& Stack<T>::Remove( typename Stack<T>::Iterator& _iter )
{
	if ( nullptr != _iter.m_curr )
	{
		if ( _iter.m_curr == m_top )
		{
			Pop();
			_iter.m_curr = m_top;
		}
		else
		{
			_iter.m_prev->m_next = _iter.m_curr->m_next;
			delete _iter.m_curr;
			_iter.m_curr = _iter.m_prev->m_next;
			--m_size;
		}
	}
	return _iter;
}
template <typename T>
typename Stack<T>::Iterator& Stack<T>::Remove( typename Stack<T>::Iterator& _iter, uint64 _num )
{
	for ( uint64 i = 0ull; i < _num; ++i )
		Remove( _iter );
	return _iter;
}
template <typename T>
typename Stack<T>::Iterator& Stack<T>::Insert( typename Stack<T>::Iterator& _iter, const T& _data )
{
	if ( nullptr != _iter.m_curr || nullptr != _iter.m_prev )
	{
		if ( m_top == _iter.m_curr )
		{
			Push( _data );
			_iter.m_curr = m_top;
		}
		else
		{
			_iter.m_curr = _iter.m_prev->m_next = new Stack<T>::Node( _data, _iter.m_curr );
			++m_size;
		}
	}
	return _iter;
}
template <typename T>
typename Stack<T>::Iterator& Stack<T>::Insert( typename Stack<T>::Iterator& _iter, T&& _data )
{
	if ( nullptr != _iter.m_curr || nullptr != _iter.m_prev )
	{
		if ( m_top == _iter.m_curr )
		{
			Push( std::move( _data ) );
			_iter.m_curr = m_top;
		}
		else
		{
			_iter.m_curr = _iter.m_prev->m_next = new Stack<T>::Node( std::move( _data ), _iter.m_curr );
			++m_size;
		}
	}
	return _iter;
}
template <typename T>
typename Stack<T>::Iterator& Stack<T>::Insert( typename Stack<T>::Iterator& _iter, const T* _arr, uint64 _siz )
{
	for ( uint64 i = 0ull; i < _siz; ++i )
		Insert( _iter, _arr[ i ] );
	return _iter;
}
template <typename T>
Stack<T>& Stack<T>::operator+=( const T& _data )
{
	return Push( _data );
}
template <typename T>
Stack<T>& Stack<T>::operator+=( T&& _data )
{
	return Push( std::move( _data ) );
}
template <typename T>
Stack<T>& Stack<T>::operator--( void )
{
	return Pop();
}
template <typename T>
Stack<T> Stack<T>::operator--( int )
{
	Stack<T> ret_ = *this;
	Pop();
	return ret_;
}
template <typename T>
Stack<T>& Stack<T>::operator-=( uint64 _num )
{
	for ( uint64 i = 0ull; i < _num; ++i )
		Pop();
	return *this;
}