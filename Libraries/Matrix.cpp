#include "stdafx.h"
#include "Matrix.h"
#pragma warning(disable:6386)

static float64 g_badIndex;

Matrix::Matrix( void ) :
	m_data( nullptr ),
	m_rows( 0u ),
	m_cols( 0u )
{
}
Matrix::Matrix( uint32 _size ) :
	m_data( nullptr ),
	m_rows( _size ),
	m_cols( _size )
{
	if ( 0u == _size )
		return;
	const uint32 size_ = _size * _size;
	++_size;
	m_data = new float64[ size_ ];
	uint32 i;
	for ( i = 0u; i < size_; ++i )
		m_data[ i ] = 0.0;
	for ( i = 0u; i < size_; i += _size )
		m_data[ i ] = 1.0;
}
Matrix::Matrix( uint32 _size, float64 _init ) :
	m_data( nullptr ),
	m_rows( _size ),
	m_cols( _size )
{
	if ( 0u == _size )
		return;
	m_data = new float64[ _size *= _size ];
	for ( uint32 i = 0u; i < _size; ++i )
		m_data[ i ] = _init;
}
Matrix::Matrix( uint32 _rows, uint32 _cols, float64 _init ) :
	m_data( nullptr ),
	m_rows( _rows ),
	m_cols( _cols )
{
	if ( 0u == m_rows )
		m_cols = 0u;
	else if ( 0u == m_cols )
		m_rows = 0u;
	else
	{
		m_data = new float64[ _rows *= _cols ];
		for ( uint32 i = 0u; i < _rows; ++i )
			m_data[ i ] = _init;
	}
}
Matrix::Matrix( const Matrix& _rhs ) :
	m_data( nullptr ),
	m_rows( _rhs.m_rows ),
	m_cols( _rhs.m_cols )
{
	if ( nullptr != _rhs.m_data )
	{
		const uint32 size_ = m_rows * m_cols;
		m_data = new float64[ size_ ];
		for ( uint32 i = 0u; i < size_; ++i )
			m_data[ i ] = _rhs.m_data[ i ];
	}
}
Matrix::Matrix( Matrix&& _rhs ) :
	m_data( std::move( _rhs.m_data ) ),
	m_rows( std::move( _rhs.m_rows ) ),
	m_cols( std::move( _rhs.m_cols ) )
{
	_rhs.m_data = nullptr;
}
Matrix& Matrix::operator=( const Matrix& _rhs )
{
	if ( this != &_rhs )
	{
		delete[ ] m_data;
		m_rows = _rhs.m_rows;
		m_cols = _rhs.m_cols;
		if ( nullptr == _rhs.m_data )
			m_data = nullptr;
		else
		{
			const uint32 size_ = m_rows * m_cols;
			m_data = new float64[ size_ ];
			for ( uint32 i = 0u; i < size_; ++i )
				m_data[ i ] = _rhs.m_data[ i ];
		}
	}
	return *this;
}
Matrix& Matrix::operator=( Matrix&& _rhs )
{
	if ( this != &_rhs )
	{
		delete[ ] m_data;
		m_data = std::move( _rhs.m_data );
		m_rows = std::move( _rhs.m_rows );
		m_cols = std::move( _rhs.m_cols );
		_rhs.m_data = nullptr;
	}
	return *this;
}
Matrix::Matrix( const Matrix3x3& _rhs ) :
	m_rows( 3u ), m_cols( 3u )
{
	m_data = new float64[ 9u ];
	for ( uint32 i = 0u; i < 9u; ++i )
		m_data[ i ] = _rhs.m_arr[ i ];
}
Matrix& Matrix::operator=( const Matrix3x3& _rhs )
{
	delete[ ] m_data;
	m_rows = m_cols = 3u;
	m_data = new float64[ 9u ];
	for ( uint32 i = 0u; i < 9u; ++i )
		m_data[ i ] = _rhs.m_arr[ i ];
	return *this;
}
Matrix::Matrix( const Matrix4x4& _rhs ) :
	m_rows( 4u ), m_cols( 4u )
{
	m_data = new float64[ 16u ];
	for ( uint32 i = 0u; i < 16u; ++i )
		m_data[ i ] = _rhs.m_arr[ i ];
}
Matrix& Matrix::operator=( const Matrix4x4& _rhs )
{
	delete[ ] m_data;
	m_rows = m_cols = 4u;
	m_data = new float64[ 16u ];
	for ( uint32 i = 0u; i < 16u; ++i )
		m_data[ i ] = _rhs.m_arr[ i ];
	return *this;
}
Matrix::~Matrix( void )
{
	delete[ ] m_data;
}
Matrix Matrix::operator+( void ) const
{
	return *this;
}
Matrix Matrix::operator-( void ) const
{
	if ( nullptr == m_data )
		return Matrix();
	const uint32 size_ = m_rows * m_cols;
	Matrix outMat_;
	outMat_.m_rows = m_rows;
	outMat_.m_cols = m_cols;
	outMat_.m_data = new float64[ size_ ];
	for ( uint32 i = 0u; i < size_; ++i )
		outMat_.m_data[ i ] = -m_data[ i ];
	return outMat_;
}
Matrix Matrix::operator+( const Matrix& _rhs ) const
{
	if ( m_rows != _rhs.m_rows || m_cols != _rhs.m_cols || nullptr == m_data )
		return Matrix();
	const uint32 size_ = m_rows * m_cols;
	Matrix outMat_;
	outMat_.m_rows = m_rows;
	outMat_.m_cols = m_cols;
	outMat_.m_data = new float64[ size_ ];
	for ( uint32 i = 0u; i < size_; ++i )
		outMat_.m_data[ i ] = m_data[ i ] + _rhs.m_data[ i ];
	return outMat_;
}
Matrix Matrix::operator-( const Matrix& _rhs ) const
{
	if ( m_rows != _rhs.m_rows || m_cols != _rhs.m_cols || nullptr == m_data )
		return Matrix();
	const uint32 size_ = m_rows * m_cols;
	Matrix outMat_;
	outMat_.m_rows = m_rows;
	outMat_.m_cols = m_cols;
	outMat_.m_data = new float64[ size_ ];
	for ( uint32 i = 0u; i < size_; ++i )
		outMat_.m_data[ i ] = m_data[ i ] - _rhs.m_data[ i ];
	return outMat_;
}
Matrix Matrix::operator*( const Matrix& _rhs ) const
{
	if ( m_cols != _rhs.m_rows || nullptr == m_data )
		return Matrix();
	const uint32& cols_ = _rhs.m_cols;
	Matrix outMat_( m_rows, cols_, 0.0 );
	uint32 i_, j_, k_;
	for ( i_ = 0u; i_ < m_rows; ++i_ ) for ( j_ = 0u; j_ < m_cols; ++j_ ) for ( k_ = 0u; k_ < cols_; ++k_ )
		outMat_.m_data[ i_ * cols_ + k_ ] += m_data[ i_ * m_cols + j_ ] * _rhs.m_data[ j_ * cols_ + k_ ];
	return outMat_;
}
Matrix Matrix::operator/( const Matrix& _rhs ) const
{
	if ( m_data != nullptr && m_rows == m_cols && _rhs.m_rows == _rhs.m_cols && _rhs.m_rows == m_rows )
		return ( *this ) * _rhs.GetInverse( _rhs.GetDeterminant() );
	return Matrix();
}
Matrix Matrix::operator*( float64 _rhs ) const
{
	if ( nullptr == m_data )
		return Matrix();
	const uint32 size_ = m_rows * m_cols;
	Matrix outMat_;
	outMat_.m_rows = m_rows;
	outMat_.m_cols = m_cols;
	outMat_.m_data = new float64[ size_ ];
	for ( uint32 i = 0u; i < size_; ++i )
		outMat_.m_data[ i ] = m_data[ i ] * _rhs;
	return outMat_;
}
Matrix Matrix::operator/( float64 _rhs ) const
{
	if ( nullptr == m_data )
		return Matrix();
	const uint32 size_ = m_rows * m_cols;
	Matrix outMat_;
	outMat_.m_rows = m_rows;
	outMat_.m_cols = m_cols;
	outMat_.m_data = new float64[ size_ ];
	_rhs = 1.0 / _rhs;
	for ( uint32 i = 0u; i < size_; ++i )
		outMat_.m_data[ i ] = m_data[ i ] * _rhs;
	return outMat_;
}
Matrix& Matrix::operator+=( const Matrix& _rhs )
{
	if ( m_rows == _rhs.m_rows && m_cols == _rhs.m_cols && nullptr != m_data )
	{
		const uint32 size_ = m_rows * m_cols;
		for ( uint32 i = 0u; i < size_; ++i )
			m_data[ i ] += _rhs.m_data[ i ];
	}
	return *this;
}
Matrix& Matrix::operator-=( const Matrix& _rhs )
{
	if ( m_rows == _rhs.m_rows && m_cols == _rhs.m_cols && nullptr != m_data )
	{
		const uint32 size_ = m_rows * m_cols;
		for ( uint32 i = 0u; i < size_; ++i )
			m_data[ i ] -= _rhs.m_data[ i ];
	}
	return *this;
}
Matrix& Matrix::operator*=( const Matrix& _rhs )
{
	Matrix outMat_ = ( *this ) * _rhs;
	if ( nullptr != outMat_.m_data )
		return ( *this ) = outMat_;
	return *this;
}
Matrix& Matrix::operator/=( const Matrix& _rhs )
{
	if ( m_data != nullptr && m_rows == m_cols && _rhs.m_rows == _rhs.m_cols && _rhs.m_rows == m_rows )
		return ( *this ) *= _rhs.GetInverse( GetDeterminant() );
	return *this;
}
Matrix& Matrix::operator*=( float64 _rhs )
{
	if ( nullptr != m_data )
	{
		const uint32 size_ = m_rows * m_cols;
		for ( uint32 i = 0u; i < size_; ++i )
			m_data[ i ] *= _rhs;
	}
	return *this;
}
Matrix& Matrix::operator/=( float64 _rhs )
{
	if ( nullptr != m_data )
	{
		const uint32 size_ = m_rows * m_cols;
		_rhs = 1.0 / _rhs;
		for ( uint32 i = 0u; i < size_; ++i )
			m_data[ i ] *= _rhs;
	}
	return *this;
}
Matrix operator*( float64 _lhs, const Matrix& _rhs )
{
	if ( nullptr == _rhs.m_data )
		return Matrix();
	const uint32 size_ = _rhs.m_rows * _rhs.m_cols;
	Matrix outMat_;
	outMat_.m_rows = _rhs.m_rows;
	outMat_.m_cols = _rhs.m_cols;
	outMat_.m_data = new float64[ size_ ];
	for ( uint32 i = 0u; i < size_; ++i )
		outMat_.m_data[ i ] = _lhs * _rhs.m_data[ i ];
	return outMat_;
}
bool Matrix::operator==( const Matrix& _rhs ) const
{
	if ( this == &_rhs )
		return true;
	if ( m_rows != _rhs.m_rows || m_cols != _rhs.m_cols || nullptr == m_data )
		return false;
	const uint32 size_ = m_rows * m_cols;
	for ( uint32 i = 0u; i < size_; ++i )
		if ( m_data[ i ] != _rhs.m_data[ i ] )
			return false;
	return true;
}
bool Matrix::operator!=( const Matrix& _rhs ) const
{
	if ( this == &_rhs )
		return false;
	if ( m_rows != _rhs.m_rows || m_cols != _rhs.m_cols || nullptr == m_data )
		return true;
	const uint32 size_ = m_rows * m_cols;
	for ( uint32 i = 0u; i < size_; ++i )
		if ( m_data[ i ] == _rhs.m_data[ i ] )
			return false;
	return true;
}
bool Matrix::operator!( void ) const
{
	return nullptr == m_data;
}
bool Matrix::IsValid( void ) const
{
	return nullptr != m_data;
}
bool Matrix::IsInvalid( void ) const
{
	return nullptr == m_data;
}
bool Matrix::IsSquare( void ) const
{
	return m_rows == m_cols && nullptr != m_data;
}
bool Matrix::IsInvertible( float64* _det ) const
{
	return ( 0.0 != ( ( *_det ) = GetDeterminant() ) );
}
Matrix& Matrix::Transpose( void )
{
	return ( *this ) = GetTranspose();
}
Matrix Matrix::GetTranspose( void ) const
{
	if ( nullptr == m_data )
		return Matrix();
	Matrix outMat_;
	outMat_.m_rows = m_cols;
	outMat_.m_cols = m_rows;
	outMat_.m_data = new float64[ m_cols * m_rows ];
	uint32 i, j; for ( i = 0u; i < m_rows; ++i ) for ( j = 0u; j < m_cols; ++j )
		outMat_.m_data[ j * m_rows + i ] = m_data[ i * m_cols + j ];
	return outMat_;
}
float64 Matrix::GetDeterminant( void ) const
{
	if ( m_rows != m_cols || nullptr == m_data )
		return 0.0;
	if ( 1u == m_rows )
		return m_data[ 0u ];
	if ( 2u == m_rows )
		return m_data[ 0 ] * m_data[ 3 ] - m_data[ 1 ] * m_data[ 2 ];
	float64 det_ = 0.0;
	for ( uint32 i = 0u; i < m_cols; ++i )
		if ( 0u == ( i & 1u ) )
			det_ += m_data[ i ] * GetMinor( 0u, i );
		else
			det_ -= m_data[ i ] * GetMinor( 0u, i );
	return det_;
}
Matrix& Matrix::Invert( void )
{
	return Invert( GetDeterminant() );
}
Matrix Matrix::GetInverse( void ) const
{
	return GetInverse( GetDeterminant() );
}
Matrix Matrix::GetInverseAndDeterminant( float64& _det ) const
{
	return GetInverse( _det = GetDeterminant() );
}
Matrix& Matrix::Invert( float64 _det )
{
	if ( 0.0 != _det )
		return ( *this ) = GetInverse( _det );
	return *this;
}
float64 Matrix::GetMinor( uint32 _row, uint32 _col ) const
{
	if ( m_rows != m_cols || _row >= m_rows || _col >= m_cols || nullptr == m_data )
		return 0.0;
	uint32 i, j, k = 0u;
	Matrix tmp_;
	tmp_.m_rows = tmp_.m_cols = m_rows - 1u;
	tmp_.m_data = new float64[ tmp_.m_rows * tmp_.m_cols ];
	for ( i = 0u; i < m_rows; ++i ) if ( _row != i )
		for ( j = 0u; j < m_cols; ++j ) if ( _col != j )
			tmp_.m_data[ k++ ] = m_data[ i * m_cols + j ];
	return tmp_.GetDeterminant();
}
float64 Matrix::GetMinor( uint32 _idx ) const
{
	const float64 size_ = m_rows * m_cols;
	if ( m_rows != m_cols || _idx >= size_ || nullptr == m_data )
		return 0.0;
	uint32 i, j = 0u;
	Matrix tmp_;
	tmp_.m_rows = tmp_.m_cols = m_rows - 1u;
	tmp_.m_data = new float64[ tmp_.m_rows * tmp_.m_cols ];
	const uint32 row_ = _idx / m_cols;
	const uint32 col_ = _idx % m_cols;
	for ( i = 0u; i < size_; ++i )
		if ( row_ != i / m_cols && col_ != i % m_cols )
			tmp_.m_data[ j++ ] = m_data[ i ];
	return tmp_.GetDeterminant();
}
Matrix Matrix::GetMinorMatrix( void ) const
{
	if ( m_rows != m_cols || nullptr == m_data )
		return Matrix();
	const uint32 size_ = m_rows * m_rows;
	Matrix outMat_;
	outMat_.m_rows = outMat_.m_cols = m_rows;
	outMat_.m_data = new float64[ size_ ];
	for ( uint32 i = 0u; i < size_; ++i )
		outMat_.m_data[ i ] = GetMinor( i );
	return outMat_;
}
Matrix Matrix::GetCofactorMatrix( void ) const
{
	Matrix outMat_ = GetMinorMatrix();
	if ( outMat_.IsValid() )
		for ( uint32 i = 0u, j; i < m_rows; ++i )
			for ( j = 0u; j < m_cols; ++j )
				if ( 0u != ( ( i + j ) & 1u ) )
					outMat_.m_data[ i * m_cols + j ] =
					-outMat_.m_data[ i * m_cols + j ];
	return outMat_;
}
Matrix Matrix::GetAdjugateMatrix( void ) const
{
	return GetCofactorMatrix().GetTranspose();
}
Matrix Matrix::GetMinorMatrix( const Matrix& _mat )
{
	return _mat.GetMinorMatrix();
}
Matrix Matrix::GetCofactorMatrix( const Matrix& _mat )
{
	return _mat.GetCofactorMatrix();
}
Matrix Matrix::GetAdjugateMatrix( const Matrix& _mat )
{
	return _mat.GetAdjugateMatrix();
}
Matrix Matrix::GetInverse( float64 _det ) const
{
	if ( 0.0 == _det || nullptr == m_data || m_rows != m_cols )
		return Matrix();
	if ( 1u == m_rows )
		return Matrix( 1u, m_data[ 0u ] / _det );
	if ( 2u == m_rows )
	{
		Matrix outMat_;
		outMat_.m_rows = outMat_.m_cols = 2u;
		outMat_.m_data = new float64[ 4u ];
		outMat_.m_data[ 0u ] = m_data[ 3u ];
		outMat_.m_data[ 1u ] = -m_data[ 1u ];
		outMat_.m_data[ 2u ] = -m_data[ 2u ];
		outMat_.m_data[ 3u ] = m_data[ 0u ];
		return outMat_ / _det;
	}
	return GetAdjugateMatrix() / _det;
}
bool Matrix::IsValid( const Matrix& _mat )
{
	return nullptr != _mat.m_data;
}
bool Matrix::IsInvalid( const Matrix& _mat )
{
	return nullptr == _mat.m_data;
}
bool Matrix::IsSquare( const Matrix& _mat )
{
	return _mat.m_rows == _mat.m_cols && nullptr != _mat.m_data;
}
bool Matrix::IsInvertible( const Matrix& _mat, float64* _det )
{
	return _mat.IsInvertible( _det );
}
Matrix& Matrix::Transpose( Matrix& _mat )
{
	return _mat.Transpose();
}
Matrix Matrix::GetTranspose( const Matrix& _mat )
{
	return _mat.GetTranspose();
}
float64 Matrix::GetDeterminant( const Matrix& _mat )
{
	return _mat.GetDeterminant();
}
Matrix& Matrix::Invert( Matrix& _mat )
{
	return _mat.Invert();
}
Matrix Matrix::GetInverse( const Matrix& _mat )
{
	return _mat.GetInverse();
}
Matrix Matrix::GetInverseAndDeterminant( const Matrix& _mat, float64& _det )
{
	return _mat.GetInverseAndDeterminant( _det );
}
Matrix& Matrix::Invert( Matrix& _mat, float64 _det )
{
	return _mat.Invert( _det );
}
Matrix Matrix::GetInverse( const Matrix& _mat, float64 _det )
{
	return _mat.GetInverse( _det );
}
uint32 Matrix::GetNumRows( void ) const
{
	return m_rows;
}
uint32 Matrix::GetNumCols( void ) const
{
	return m_cols;
}
float64 Matrix::GetElement( uint32 _idx ) const
{
	if ( nullptr != m_data && _idx < m_rows * m_cols )
		return m_data[ _idx ];
	return BAD_FLOAT64;
}
float64 Matrix::GetElement( uint32 _row, uint32 _col ) const
{
	if ( nullptr != m_data && _row < m_rows && _col < m_cols )
		return m_data[ _row * m_cols + _col ];
	return BAD_FLOAT64;
}
const float64& Matrix::GetElement_Ref( uint32 _idx ) const
{
	if ( nullptr != m_data && _idx < m_rows * m_cols )
		return m_data[ _idx ];
	return g_badIndex = BAD_FLOAT64;
}
const float64& Matrix::GetElement_Ref( uint32 _row, uint32 _col ) const
{
	if ( nullptr != m_data && _row < m_rows && _col < m_cols )
		return m_data[ _row * m_cols + _col ];
	return g_badIndex = BAD_FLOAT64;
}
float64& Matrix::GetElement_Ref( uint32 _idx )
{
	if ( nullptr != m_data && _idx < m_rows * m_cols )
		return m_data[ _idx ];
	return g_badIndex = BAD_FLOAT64;
}
float64& Matrix::GetElement_Ref( uint32 _row, uint32 _col )
{
	if ( nullptr != m_data && _row < m_rows && _col < m_cols )
		return m_data[ _row * m_cols + _col ];
	return g_badIndex = BAD_FLOAT64;
}
void Matrix::SetElement( uint32 _idx, float64 _val )
{
	if ( nullptr != m_data && _idx < m_rows * m_cols )
		m_data[ _idx ] = _val;
}
void Matrix::SetElement( uint32 _row, uint32 _col, float64 _val )
{
	if ( nullptr != m_data && _row < m_rows && _col < m_cols )
		m_data[ _row * m_cols + _col ] = _val;
}
float64* Matrix::operator[]( uint32 _row )
{
	if ( nullptr != m_data && _row < m_rows )
		return m_data + _row * m_cols;
	return nullptr;
}
const float64* Matrix::operator[]( uint32 _row ) const
{
	if ( nullptr != m_data && _row < m_rows )
		return m_data + _row * m_cols;
	return nullptr;
}
std::ostream& operator<<( std::ostream& _os, const Matrix& _rhs )
{
	uint32 i, j;
	for ( i = 0u; i < _rhs.m_rows; ++i )
	{
		for ( j = 0u; j < _rhs.m_cols; ++j )
		{
			_os << _rhs.m_data[ i * _rhs.m_cols + j ];
			if ( j < _rhs.m_cols - 1u )
				_os << '\t';
		}
		if ( i < _rhs.m_rows - 1u )
			_os << '\n';
	}
	return _os;
}