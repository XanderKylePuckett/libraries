#pragma once
#include <random>

class Random
{
private:
	std::mt19937_64 mt_rand;
public:
	Random( void );
	Random( uint64 );
	Random( const Random& );
	Random( Random&& );
	Random& operator=( const Random& );
	Random& operator=( Random&& );
	~Random( void );
	uint64 GetRandom( uint64 _min, uint64 _max );
	uint32 GetRandom( uint32 _min, uint32 _max );
	uint16 GetRandom( uint16 _min, uint16 _max );
	uint8 GetRandom( uint8 _min, uint8 _max );
	int64 GetRandom( int64 _min, int64 _max );
	int32 GetRandom( int32 _min, int32 _max );
	int16 GetRandom( int16 _min, int16 _max );
	int8 GetRandom( int8 _min, int8 _max );
	float32 GetRandom( float32 _min, float32 _max );
	float64 GetRandom( float64 _min, float64 _max );
	uint64 GetRandomUint64( void );
	uint32 GetRandomUint32( void );
	uint16 GetRandomUint16( void );
	uint8 GetRandomUint8( void );
	int64 GetRandomInt64( void );
	int32 GetRandomInt32( void );
	int16 GetRandomInt16( void );
	int8 GetRandomInt8( void );
	float32 GetRandomFloat32( void );
	float64 GetRandomFloat64( void );
	float32 GetRandomFloat32_01( void );
	float64 GetRandomFloat64_01( void );
};
extern Random g_random;