#pragma once
template <typename T>
class Queue
{
private:
	struct Node
	{
		T m_data;
		typename Queue<T>::Node* m_prev;
		typename Queue<T>::Node* m_next;
		Node( void );
		Node( const T&, typename Queue<T>::Node* = nullptr, typename Queue<T>::Node* = nullptr );
		Node( T&&, typename Queue<T>::Node* = nullptr, typename Queue<T>::Node* = nullptr );
		Node( const typename Queue<T>::Node& );
		Node( typename Queue<T>::Node&& );
		typename Queue<T>::Node& operator=( const typename Queue<T>::Node& );
		typename Queue<T>::Node& operator=( typename Queue<T>::Node&& );
		~Node( void );
	};
	uint64 m_size;
	Node* m_head;
	Node* m_tail;
public:
	struct ConstIterator;
	struct Iterator
	{
		friend class Queue<T>;
	private:
		typename Queue<T>::Node* m_node;
		Iterator( typename Queue<T>::Node* );
	public:
		inline typename Queue<T>::Iterator& Next( void );
		inline typename Queue<T>::Iterator& Prev( void );
		inline T& Get( void );
		inline const T& Get( void ) const;
		inline T& operator*( void );
		inline const T& operator*( void ) const;
		inline bool End( void ) const;
		inline bool operator!( void ) const;
		Iterator( void );
		Iterator( const typename Queue<T>::Iterator& );
		Iterator( typename Queue<T>::Iterator&& );
		typename Queue<T>::Iterator& operator=( const typename Queue<T>::Iterator& );
		typename Queue<T>::Iterator& operator=( typename Queue<T>::Iterator&& );
		~Iterator( void );
		inline typename Queue<T>::Iterator& operator++( void );
		typename Queue<T>::Iterator operator++( int );
		inline typename Queue<T>::Iterator& operator--( void );
		typename Queue<T>::Iterator operator--( int );
		typename Queue<T>::Iterator& operator+=( uint64 );
		typename Queue<T>::Iterator& operator-=( uint64 );
		inline typename Queue<T>::Iterator& operator+=( int64 );
		inline typename Queue<T>::Iterator& operator-=( int64 );
		inline typename Queue<T>::ConstIterator GetConstIterator( void ) const;
	};
	struct ConstIterator
	{
		friend class Queue<T>;
	private:
		const typename Queue<T>::Node* m_node;
		ConstIterator( const typename Queue<T>::Node* );
	public:
		inline typename Queue<T>::ConstIterator& Next( void );
		inline typename Queue<T>::ConstIterator& Prev( void );
		inline const T& Get( void ) const;
		inline const T& operator*( void ) const;
		inline bool End( void ) const;
		inline bool operator!( void ) const;
		ConstIterator( void );
		ConstIterator( const typename Queue<T>::ConstIterator& );
		ConstIterator( typename Queue<T>::ConstIterator&& );
		typename Queue<T>::ConstIterator& operator=( const typename Queue<T>::ConstIterator& );
		typename Queue<T>::ConstIterator& operator=( typename Queue<T>::ConstIterator&& );
		ConstIterator( const typename Queue<T>::Iterator& );
		ConstIterator( typename Queue<T>::Iterator&& );
		typename Queue<T>::ConstIterator& operator=( const typename Queue<T>::Iterator& );
		typename Queue<T>::ConstIterator& operator=( typename Queue<T>::Iterator&& );
		~ConstIterator( void );
		inline typename Queue<T>::ConstIterator& operator++( void );
		typename Queue<T>::ConstIterator operator++( int );
		inline typename Queue<T>::ConstIterator& operator--( void );
		typename Queue<T>::ConstIterator operator--( int );
		typename Queue<T>::ConstIterator& operator+=( uint64 );
		typename Queue<T>::ConstIterator& operator-=( uint64 );
		inline typename Queue<T>::ConstIterator& operator+=( int64 );
		inline typename Queue<T>::ConstIterator& operator-=( int64 );
	};
	Queue( void );
	Queue( const Queue<T>& );
	Queue( Queue<T>&& );
	Queue<T>& operator=( const Queue<T>& );
	Queue<T>& operator=( Queue<T>&& );
	~Queue( void );
	inline Queue<T>& Clear( void );
	Queue<T>& Enqueue( const T& );
	Queue<T>& Enqueue( T&& );
	Queue<T>& Dequeue( void );
	inline T& Front( void );
	inline const T& Front( void ) const;
	inline uint64 GetSize( void ) const;
	inline typename Queue<T>::Iterator Begin( void );
	inline typename Queue<T>::ConstIterator Begin( void ) const;
	typename Queue<T>::Iterator& Remove( typename Queue<T>::Iterator& );
	typename Queue<T>::Iterator& Insert( typename Queue<T>::Iterator&, const T& );
	typename Queue<T>::Iterator& Insert( typename Queue<T>::Iterator&, T&& );
	inline Queue<T>& operator+=( const T& );
	inline Queue<T>& operator+=( T&& );
	inline Queue<T>& operator+=( const Queue<T>& );
	Queue<T>& operator+=( Queue<T>&& );
	inline Queue<T>& Enqueue( const Queue<T>& );
	inline Queue<T>& Enqueue( Queue<T>&& );
	inline Queue<T>& Dequeue( uint64 );
	inline Queue<T>& Enqueue( const T*, uint64 );
	inline typename Queue<T>::Iterator& Remove( typename Queue<T>::Iterator&, uint64 );
	inline typename Queue<T>::Iterator& Insert( typename Queue<T>::Iterator&, const T*, uint64 );
};
template <typename T>
Queue<T>::Node::Node( void ) :
	m_prev( nullptr ),
	m_next( nullptr )
{
}
template <typename T>
Queue<T>::Node::Node( const T& _data, typename Queue<T>::Node* _prev, typename Queue<T>::Node* _next ) :
	m_data( _data ),
	m_prev( _prev ),
	m_next( _next )
{
}
template <typename T>
Queue<T>::Node::Node( T&& _data, typename  Queue<T>::Node* _prev, typename  Queue<T>::Node* _next ) :
	m_data( std::move( _data ) ),
	m_prev( _prev ),
	m_next( _next )
{
}
template <typename T>
Queue<T>::Node::Node( const typename Queue<T>::Node& _rhs ) :
	m_data( _rhs.m_data ),
	m_prev( _rhs.m_prev ),
	m_next( _rhs.m_next )
{
}
template <typename T>
Queue<T>::Node::Node( typename Queue<T>::Node&& _rhs ) :
	m_data( std::move( _rhs.m_data ) ),
	m_prev( std::move( _rhs.m_prev ) ),
	m_next( std::move( _rhs.m_next ) )
{
	_rhs.m_prev = nullptr;
	_rhs.m_next = nullptr;
}
template <typename T>
typename Queue<T>::Node& Queue<T>::Node::operator=( const typename Queue<T>::Node& _rhs )
{
	if ( this != &_rhs )
	{
		m_data = _rhs.m_data;
		m_prev = _rhs.m_prev;
		m_next = _rhs.m_next;
	}
	return *this;
}
template <typename T>
typename Queue<T>::Node& Queue<T>::Node::operator=( typename Queue<T>::Node&& _rhs )
{
	if ( this != &_rhs )
	{
		m_data = std::move( _rhs.m_data );
		m_prev = std::move( _rhs.m_prev );
		m_next = std::move( _rhs.m_next );
		_rhs.m_prev = nullptr;
		_rhs.m_next = nullptr;
	}
	return *this;
}
template <typename T>
Queue<T>::Node::~Node( void )
{
}
template <typename T>
Queue<T>::Iterator::Iterator( typename Queue<T>::Node* _node ) :
	m_node( _node )
{
}
template <typename T>
inline typename Queue<T>::Iterator& Queue<T>::Iterator::Next( void )
{
	if ( nullptr != m_node )
		m_node = m_node->m_next;
	return *this;
}
template <typename T>
inline typename Queue<T>::Iterator& Queue<T>::Iterator::Prev( void )
{
	if ( nullptr != m_node )
		m_node = m_node->m_prev;
	return *this;
}
template <typename T>
inline T& Queue<T>::Iterator::Get( void )
{
	return m_node->m_data;
}
template <typename T>
inline const T& Queue<T>::Iterator::Get( void ) const
{
	return m_node->m_data;
}
template <typename T>
inline T& Queue<T>::Iterator::operator*( void )
{
	return m_node->m_data;
}
template <typename T>
inline const T& Queue<T>::Iterator::operator*( void ) const
{
	return m_node->m_data;
}
template <typename T>
inline bool Queue<T>::Iterator::End( void ) const
{
	return nullptr == m_node;
}
template <typename T>
inline bool Queue<T>::Iterator::operator!( void ) const
{
	return nullptr == m_node;
}
template <typename T>
Queue<T>::Iterator::Iterator( void ) :
	m_node( nullptr )
{
}
template <typename T>
Queue<T>::Iterator::Iterator( const typename Queue<T>::Iterator& _rhs ) :
	m_node( _rhs.m_node )
{
}
template <typename T>
Queue<T>::Iterator::Iterator( typename Queue<T>::Iterator&& _rhs ) :
	m_node( std::move( _rhs.m_node ) )
{
	_rhs.m_node = nullptr;
}
template <typename T>
typename Queue<T>::Iterator& Queue<T>::Iterator::operator=( const typename Queue<T>::Iterator& _rhs )
{
	if ( this != &_rhs )
	{
		m_node = _rhs.m_node;
	}
	return *this;
}
template <typename T>
typename Queue<T>::Iterator& Queue<T>::Iterator::operator=( typename Queue<T>::Iterator&& _rhs )
{
	if ( this != &_rhs )
	{
		m_node = std::move( _rhs.m_node );
		_rhs.m_node = nullptr;
	}
	return *this;
}
template <typename T>
Queue<T>::Iterator::~Iterator( void )
{
}
template <typename T>
inline typename Queue<T>::Iterator& Queue<T>::Iterator::operator++( void )
{
	return Next();
}
template <typename T>
typename Queue<T>::Iterator Queue<T>::Iterator::operator++( int )
{
	typename Queue<T>::Iterator ret_ = *this;
	Next();
	return ret_;
}
template <typename T>
inline typename Queue<T>::Iterator& Queue<T>::Iterator::operator--( void )
{
	return Prev();
}
template <typename T>
typename Queue<T>::Iterator Queue<T>::Iterator::operator--( int )
{
	typename Queue<T>::Iterator ret_ = *this;
	Prev();
	return ret_;
}
template <typename T>
typename Queue<T>::Iterator& Queue<T>::Iterator::operator+=( uint64 _num )
{
	for ( uint64 i = 0ull; i < _num; ++i )
		Next();
	return *this;
}
template <typename T>
typename Queue<T>::Iterator& Queue<T>::Iterator::operator-=( uint64 _num )
{
	for ( uint64 i = 0ull; i < _num; ++i )
		Prev();
	return *this;
}
template <typename T>
inline typename Queue<T>::Iterator& Queue<T>::Iterator::operator+=( int64 _num )
{
	return ( ( _num < 0ll ) ? ( operator-=( ( uint64 )_num ) ) : ( operator+=( ( uint64 )_num ) ) );
}
template <typename T>
inline typename Queue<T>::Iterator& Queue<T>::Iterator::operator-=( int64 _num )
{
	return ( ( _num < 0ll ) ? ( operator+=( ( uint64 )_num ) ) : ( operator-=( ( uint64 )_num ) ) );
}
template <typename T>
inline typename Queue<T>::ConstIterator Queue<T>::Iterator::GetConstIterator( void ) const
{
	return Queue<T>::ConstIterator( m_node );
}
template <typename T>
Queue<T>::ConstIterator::ConstIterator( const typename Queue<T>::Node* _node ) :
	m_node( _node )
{
}
template <typename T>
inline typename Queue<T>::ConstIterator& Queue<T>::ConstIterator::Next( void )
{
	if ( nullptr != m_node )
		m_node = m_node->m_next;
	return *this;
}
template <typename T>
inline typename Queue<T>::ConstIterator& Queue<T>::ConstIterator::Prev( void )
{
	if ( nullptr != m_node )
		m_node = m_node->m_prev;
	return *this;
}
template <typename T>
inline const T& Queue<T>::ConstIterator::Get( void ) const
{
	return m_node->m_data;
}
template <typename T>
inline const T& Queue<T>::ConstIterator::operator*( void ) const
{
	return m_node->m_data;
}
template <typename T>
inline bool Queue<T>::ConstIterator::End( void ) const
{
	return nullptr == m_node;
}
template <typename T>
inline bool Queue<T>::ConstIterator::operator!( void ) const
{
	return nullptr == m_node;
}
template <typename T>
Queue<T>::ConstIterator::ConstIterator( void ) :
	m_node( nullptr )
{
}
template <typename T>
Queue<T>::ConstIterator::ConstIterator( const typename Queue<T>::ConstIterator& _rhs ) :
	m_node( _rhs.m_node )
{
}
template <typename T>
Queue<T>::ConstIterator::ConstIterator( typename Queue<T>::ConstIterator&& _rhs ) :
	m_node( std::move( _rhs.m_node ) )
{
	_rhs.m_node = nullptr;
}
template <typename T>
typename Queue<T>::ConstIterator& Queue<T>::ConstIterator::operator=( const typename Queue<T>::ConstIterator& _rhs )
{
	if ( this != &_rhs )
	{
		m_node = _rhs.m_node;
	}
	return *this;
}
template <typename T>
typename Queue<T>::ConstIterator& Queue<T>::ConstIterator::operator=( typename Queue<T>::ConstIterator&& _rhs )
{
	if ( this != &_rhs )
	{
		m_node = std::move( _rhs.m_node );
		_rhs.m_node = nullptr;
	}
	return *this;
}
template <typename T>
Queue<T>::ConstIterator::ConstIterator( const typename Queue<T>::Iterator& _rhs ) :
	m_node( _rhs.m_node )
{
}
template <typename T>
Queue<T>::ConstIterator::ConstIterator( typename Queue<T>::Iterator&& _rhs ) :
	m_node( std::move( _rhs.m_node ) )
{
}
template <typename T>
typename Queue<T>::ConstIterator& Queue<T>::ConstIterator::operator=( const typename Queue<T>::Iterator& _rhs )
{
	m_node = _rhs.m_node;
	return *this;
}
template <typename T>
typename Queue<T>::ConstIterator& Queue<T>::ConstIterator::operator=( typename Queue<T>::Iterator&& _rhs )
{
	m_node = std::move( _rhs.m_node );
	_rhs.m_node = nullptr;
	return *this;
}
template <typename T>
Queue<T>::ConstIterator::~ConstIterator( void )
{
}
template <typename T>
inline typename Queue<T>::ConstIterator& Queue<T>::ConstIterator::operator++( void )
{
	return Next();
}
template <typename T>
typename Queue<T>::ConstIterator Queue<T>::ConstIterator::operator++( int )
{
	typename Queue<T>::ConstIterator ret_ = *this;
	Next();
	return ret_;
}
template <typename T>
inline typename Queue<T>::ConstIterator& Queue<T>::ConstIterator::operator--( void )
{
	return Prev();
}
template <typename T>
typename Queue<T>::ConstIterator Queue<T>::ConstIterator::operator--( int )
{
	typename Queue<T>::ConstIterator ret_ = *this;
	Prev();
	return ret_;
}
template <typename T>
typename Queue<T>::ConstIterator& Queue<T>::ConstIterator::operator+=( uint64 _num )
{
	for ( uint64 i = 0ull; i < _num; ++i )
		Next();
	return *this;
}
template <typename T>
typename Queue<T>::ConstIterator& Queue<T>::ConstIterator::operator-=( uint64 _num )
{
	for ( uint64 i = 0ull; i < _num; ++i )
		Prev();
	return *this;
}
template <typename T>
inline typename Queue<T>::ConstIterator& Queue<T>::ConstIterator::operator+=( int64 _num )
{
	return ( ( _num < 0ll ) ? ( operator-=( ( uint64 )_num ) ) : ( operator+=( ( uint64 )_num ) ) );
}
template <typename T>
inline typename Queue<T>::ConstIterator& Queue<T>::ConstIterator::operator-=( int64 _num )
{
	return ( ( _num < 0ll ) ? ( operator+=( ( uint64 )_num ) ) : ( operator-=( ( uint64 )_num ) ) );
}
template <typename T>
Queue<T>::Queue( void ) :
	m_head( nullptr ),
	m_tail( nullptr ),
	m_size( 0ull )
{
}
template <typename T>
Queue<T>::Queue( const Queue<T>& _rhs ) :
	m_head( nullptr ),
	m_tail( nullptr ),
	m_size( 0ull )
{
	for ( typename Queue<T>::ConstIterator iter_ = _rhs.Begin(); !iter_.End(); iter_.Next() )
		Enqueue( iter_.Get() );
}
template <typename T>
Queue<T>::Queue( Queue<T>&& _rhs ) :
	m_head( std::move( _rhs.m_head ) ),
	m_tail( std::move( _rhs.m_tail ) ),
	m_size( std::move( _rhs.m_size ) )
{
	_rhs.m_head = nullptr;
	_rhs.m_tail = nullptr;
	_rhs.m_size = 0ull;
}
template <typename T>
Queue<T>& Queue<T>::operator=( const Queue<T>& _rhs )
{
	if ( this != &_rhs )
	{
		Clear();
		for ( typename Queue<T>::ConstIterator iter_ = _rhs.Begin(); !iter_.End(); iter_.Next() )
			Enqueue( iter_.Get() );
	}
	return *this;
}
template <typename T>
Queue<T>& Queue<T>::operator=( Queue<T>&& _rhs )
{
	if ( this != &_rhs )
	{
		Clear();
		m_head = std::move( _rhs.m_head );
		m_tail = std::move( _rhs.m_tail );
		m_size = std::move( _rhs.m_size );
		_rhs.m_head = nullptr;
		_rhs.m_tail = nullptr;
		_rhs.m_size = 0ull;
	}
	return *this;
}
template <typename T>
Queue<T>::~Queue( void )
{
	Clear();
}
template <typename T>
inline Queue<T>& Queue<T>::Clear( void )
{
	while ( nullptr != m_head )
		Dequeue();
	return *this;
}
template <typename T>
Queue<T>& Queue<T>::Enqueue( const T& _data )
{
	m_tail = new Queue<T>::Node( _data, m_tail );
	if ( nullptr != m_head )
		m_tail->m_prev->m_next = m_tail;
	else
		m_head = m_tail;
	++m_size;
	return *this;
}
template <typename T>
Queue<T>& Queue<T>::Enqueue( T&& _data )
{
	m_tail = new Queue<T>::Node( std::move( _data ), m_tail );
	if ( nullptr != m_head )
		m_tail->m_prev->m_next = m_tail;
	else
		m_head = m_tail;
	++m_size;
	return *this;
}
template <typename T>
Queue<T>& Queue<T>::Dequeue( void )
{
	if ( m_head == m_tail )
	{
		delete m_head;
		m_head = m_tail = nullptr;
		m_size = 0ull;
	}
	else
	{
		m_head = m_head->m_next;
		delete m_head->m_prev;
		m_head->m_prev = nullptr;
		--m_size;
	}
	return *this;
}
template <typename T>
inline T& Queue<T>::Front( void )
{
	return m_head->m_data;
}
template <typename T>
inline const T& Queue<T>::Front( void ) const
{
	return m_head->m_data;
}
template <typename T>
inline uint64 Queue<T>::GetSize( void ) const
{
	return m_size;
}
template <typename T>
inline typename Queue<T>::Iterator Queue<T>::Begin( void )
{
	return Queue<T>::Iterator( m_head );
}
template <typename T>
inline typename Queue<T>::ConstIterator Queue<T>::Begin( void ) const
{
	return Queue<T>::ConstIterator( m_head );
}
template <typename T>
typename Queue<T>::Iterator& Queue<T>::Remove( typename Queue<T>::Iterator& _iter )
{
	if ( m_head == _iter.m_node )
	{
		Dequeue();
		_iter.m_node = m_head;
	}
	else if ( m_tail == _iter.m_node )
	{
		m_tail = m_tail->m_prev;
		delete m_tail->m_next;
		m_tail->m_next = _iter.m_node = nullptr;
		--m_size;
	}
	else if ( nullptr != _iter.m_node )
	{
		typename Queue<T>::Node* delNode_ = _iter.m_node;
		_iter.m_node = delNode_->m_next;
		delNode_->m_next->m_prev = delNode_->m_prev;
		delNode_->m_prev->m_next = delNode_->m_next;
		delete delNode_;
		--m_size;
	}
	return _iter;
}
template <typename T>
typename Queue<T>::Iterator& Queue<T>::Insert( typename Queue<T>::Iterator& _iter, const T& _data )
{
	if ( nullptr != _iter.m_node )
	{
		_iter.m_node = new Queue<T>::Node( _data, _iter.m_node->m_prev, _iter.m_node );
		_iter.m_node->m_next->m_prev = _iter.m_node;
		if ( nullptr != _iter.m_node->m_prev )
			_iter.m_node->m_prev->m_next = _iter.m_node;
		else
			m_head = _iter.m_node;
		++m_size;
	}
	else
	{
		Enqueue( _data );
		_iter.m_node = m_tail;
	}
	return _iter;
}
template <typename T>
typename Queue<T>::Iterator& Queue<T>::Insert( typename Queue<T>::Iterator& _iter, T&& _data )
{
	if ( nullptr != _iter.m_node )
	{
		_iter.m_node = new Queue<T>::Node( std::move( _data ), _iter.m_node->m_prev, _iter.m_node );
		_iter.m_node->m_next->m_prev = _iter.m_node;
		if ( nullptr != _iter.m_node->m_prev )
			_iter.m_node->m_prev->m_next = _iter.m_node;
		else
			m_head = _iter.m_node;
		++m_size;
	}
	else
	{
		Enqueue( std::move( _data ) );
		_iter.m_node = m_tail;
	}
	return _iter;
}
template <typename T>
inline Queue<T>& Queue<T>::operator+=( const T& _data )
{
	return Enqueue( _data );
}
template <typename T>
inline Queue<T>& Queue<T>::operator+=( T&& _data )
{
	return Enqueue( std::move( _data ) );
}
template <typename T>
inline Queue<T>& Queue<T>::operator+=( const Queue<T>& _rhs )
{
	for ( typename Queue<T>::ConstIterator iter = _rhs.Begin(); !iter.End(); iter.Next() )
		Enqueue( iter.Get() );
	return *this;
}
template <typename T>
Queue<T>& Queue<T>::operator+=( Queue<T>&& _rhs )
{
	if ( this != &_rhs )
	{
		if ( nullptr != _rhs.m_head )
		{
			if ( nullptr == m_tail )
			{
				m_head = std::move( _rhs.m_head );
				m_tail = std::move( _rhs.m_tail );
				m_size = std::move( _rhs.m_size );
			}
			else
			{
				m_tail->m_next = _rhs.m_head;
				_rhs.m_head->m_prev = m_tail;
				m_tail = _rhs.m_tail;
				m_size += _rhs.m_size;
			}
		}
		_rhs.m_head = nullptr;
		_rhs.m_tail = nullptr;
		_rhs.m_size = 0ull;
	}
	else
	{
		const Queue<T> copy_ = *this;
		_rhs += copy_;
	}
	return *this;
}
template <typename T>
inline Queue<T>& Queue<T>::Enqueue( const Queue<T>& _rhs )
{
	return ( *this ) += _rhs;
}
template <typename T>
inline Queue<T>& Queue<T>::Enqueue( Queue<T>&& _rhs )
{
	return ( *this ) += std::move( _rhs );
}
template <typename T>
inline Queue<T>& Queue<T>::Dequeue( uint64 _num )
{
	for ( uint64 i = 0ull; i < _num; ++i )
		Dequeue();
	return *this;
}
template <typename T>
inline Queue<T>& Queue<T>::Enqueue( const T* _arr, uint64 _siz )
{
	for ( uint64 i = 0ull; i < _siz; ++i )
		Enqueue( _arr[ i ] );
	return *this;
}
template <typename T>
inline typename Queue<T>::Iterator& Queue<T>::Remove( typename Queue<T>::Iterator& _iter, uint64 _num )
{
	for ( uint64 i = 0ull; i < _num; ++i )
		Remove( _iter );
	return _iter;
}
template <typename T>
inline typename Queue<T>::Iterator& Queue<T>::Insert( typename Queue<T>::Iterator& _iter, const T* _arr, uint64 _siz )
{
	for ( uint64 i = 0ull; i < _siz; ++i )
		Insert( _iter, _arr[ _siz - i - 1ull ] );
	return _iter;
}