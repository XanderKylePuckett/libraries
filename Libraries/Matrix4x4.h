#pragma once
#include "Matrix3x3.h"
#include "Vector4.h"
#pragma warning(disable:4201)

class Matrix;
struct Matrix3x3;
struct Matrix4x4
{
	union
	{
		float64 e[ 4 ][ 4 ];
		float64 m_arr[ 16 ];
		Vector4 m_rows[ 4 ];
		struct
		{
			float64
				e11, e12, e13, e14,
				e21, e22, e23, e24,
				e31, e32, e33, e34,
				e41, e42, e43, e44;
		};
		struct
		{
			Vector3 m_xAxis;
			float64 m_paddingX;
			Vector3 m_yAxis;
			float64 m_paddingY;
			Vector3 m_zAxis;
			float64 m_paddingZ;
			union
			{
				struct
				{
					Vector3 m_wAxis;
					float64 m_paddingW;
				};
				struct
				{
					Vector3 m_position;
				};
			};
		};
		struct
		{
			Vector4 m_row1;
			Vector4 m_row2;
			Vector4 m_row3;
			Vector4 m_row4;
		};
	};
	static const Matrix4x4 IDENTITY, ZERO, ONE;
	Matrix4x4( void );
	Matrix4x4( float64 _11, float64 _12, float64 _13, float64 _14,
			   float64 _21, float64 _22, float64 _23, float64 _24,
			   float64 _31, float64 _32, float64 _33, float64 _34,
			   float64 _41, float64 _42, float64 _43, float64 _44 );
	Matrix4x4( const float64* _arr );
	Matrix4x4( float32 _11, float32 _12, float32 _13, float32 _14,
			   float32 _21, float32 _22, float32 _23, float32 _24,
			   float32 _31, float32 _32, float32 _33, float32 _34,
			   float32 _41, float32 _42, float32 _43, float32 _44 );
	Matrix4x4( const float32* _arr );
	Matrix4x4( const Vector4* _rows );
	Matrix4x4( const Vector4& r1,
			   const Vector4& r2,
			   const Vector4& r3,
			   const Vector4& r4 );
	Matrix4x4( const Matrix4x4& rhs );
	Matrix4x4( const Matrix3x3& rhs );
	Matrix4x4( const Matrix& rhs );
	Matrix4x4( Matrix4x4&& rhs );
	Matrix4x4& operator=( const float64* rhs );
	Matrix4x4& operator=( const float32* rhs );
	Matrix4x4& operator=( const Vector4* rhs );
	Matrix4x4& operator=( const Matrix4x4& rhs );
	Matrix4x4& operator=( const Matrix3x3& rhs );
	Matrix4x4& operator=( const Matrix& rhs );
	Matrix4x4& operator=( Matrix4x4&& rhs );
	~Matrix4x4( void );
	Matrix4x4 operator+( void ) const;
	Matrix4x4 operator-( void ) const;
	Matrix4x4 operator+( const Matrix4x4& rhs ) const;
	Matrix4x4 operator-( const Matrix4x4& rhs ) const;
	Matrix4x4 operator*( const Matrix4x4& rhs ) const;
	Matrix4x4 operator*( float64 rhs ) const;
	Matrix4x4 operator/( float64 rhs ) const;
	Matrix4x4& operator+=( const Matrix4x4& rhs );
	Matrix4x4& operator-=( const Matrix4x4& rhs );
	Matrix4x4& operator*=( const Matrix4x4& rhs );
	Matrix4x4& operator*=( float64 rhs );
	Matrix4x4& operator/=( float64 rhs );
	bool operator==( const Matrix4x4& rhs ) const;
	bool operator!=( const Matrix4x4& rhs ) const;
	Matrix3x3 get3x3( void ) const;
	Matrix4x4 getInverse( void ) const;
	static Matrix4x4 getInverse( const Matrix4x4& );
	Matrix4x4& Invert( void );
	static Matrix4x4& Invert( Matrix4x4& );
	float64 getDeterminant( void ) const;
	static float64 getDeterminant( const Matrix4x4& );
	Matrix4x4 getTranspose( void ) const;
	static Matrix4x4 getTranspose( const Matrix4x4& );
	Matrix4x4& Transpose( void );
	static Matrix4x4& Transpose( Matrix4x4& );
	Matrix4x4 getMinorMatrix( void ) const;
	Matrix4x4 getCofactorMatrix( void ) const;
	Matrix4x4 getAdjugateMatrix( void ) const;
	static Matrix4x4 getMinorMatrix( const Matrix4x4& );
	static Matrix4x4 getCofactorMatrix( const Matrix4x4& );
	static Matrix4x4 getAdjugateMatrix( const Matrix4x4& );
	static Matrix4x4 TranslationMatrix( const Vector3& );
	static Matrix4x4 ScaleMatrix( const Vector3& );
	static Matrix4x4 ScaleMatrix( float64 );
	static Matrix4x4 RotationXMatrix( float64 );
	static Matrix4x4 RotationYMatrix( float64 );
	static Matrix4x4 RotationZMatrix( float64 );
	static Matrix4x4 AxisAngleRotation( const Vector3& axis, float64 angle );
	Vector4 operator*( const Vector4& ) const;
	Vector3 operator*( const Vector3& ) const;
};

Matrix4x4 operator*( float64 lhs, const Matrix4x4& rhs );
Vector4 operator*( const Vector4&, const Matrix4x4& );
Vector3 operator*( const Vector3&, const Matrix4x4& );