#pragma once
#include "Vector3.h"
#include "Matrix.h"
#pragma warning(disable:4201)

class Matrix;
struct Matrix3x3
{
	union
	{
		float64 e[ 3 ][ 3 ];
		float64 m_arr[ 9 ];
		Vector3 m_rows[ 3 ];
		struct
		{
			float64
				e11, e12, e13,
				e21, e22, e23,
				e31, e32, e33;
		};
		struct
		{
			Vector3 m_xAxis;
			Vector3 m_yAxis;
			Vector3 m_zAxis;
		};
		struct
		{
			Vector3 m_row1;
			Vector3 m_row2;
			Vector3 m_row3;
		};
	};
	static const Matrix3x3 IDENTITY, ZERO, ONE;
	Matrix3x3( void );
	Matrix3x3( float64 _11, float64 _12, float64 _13,
			   float64 _21, float64 _22, float64 _23,
			   float64 _31, float64 _32, float64 _33 );
	Matrix3x3( const float64* _arr );
	Matrix3x3( float32 _11, float32 _12, float32 _13,
			   float32 _21, float32 _22, float32 _23,
			   float32 _31, float32 _32, float32 _33 );
	Matrix3x3( const float32* _arr );
	Matrix3x3( const Vector3* _rows );
	Matrix3x3( const Vector3& r1,
			   const Vector3& r2,
			   const Vector3& r3 );
	Matrix3x3( const Matrix3x3& rhs );
	Matrix3x3( Matrix3x3&& rhs );
	Matrix3x3( const Matrix& rhs );
	Matrix3x3& operator=( const float64* rhs );
	Matrix3x3& operator=( const float32* rhs );
	Matrix3x3& operator=( const Vector3* rhs );
	Matrix3x3& operator=( const Matrix3x3& rhs );
	Matrix3x3& operator=( Matrix3x3&& rhs );
	Matrix3x3& operator=( const Matrix& rhs );
	~Matrix3x3( void );
	Matrix3x3 operator+( void ) const;
	Matrix3x3 operator-( void ) const;
	Matrix3x3 operator+( const Matrix3x3& rhs ) const;
	Matrix3x3 operator-( const Matrix3x3& rhs ) const;
	Matrix3x3 operator*( const Matrix3x3& rhs ) const;
	Matrix3x3 operator*( float64 rhs ) const;
	Matrix3x3 operator/( float64 rhs ) const;
	Matrix3x3& operator+=( const Matrix3x3& rhs );
	Matrix3x3& operator-=( const Matrix3x3& rhs );
	Matrix3x3& operator*=( const Matrix3x3& rhs );
	Matrix3x3& operator*=( float64 rhs );
	Matrix3x3& operator/=( float64 rhs );
	bool operator==( const Matrix3x3& rhs ) const;
	bool operator!=( const Matrix3x3& rhs ) const;
	Matrix3x3 getInverse( void ) const;
	static Matrix3x3 getInverse( const Matrix3x3& );
	Matrix3x3& Invert( void );
	static Matrix3x3& Invert( Matrix3x3& );
	float64 getDeterminant( void ) const;
	static float64 getDeterminant( const Matrix3x3& );
	Matrix3x3 getTranspose( void ) const;
	static Matrix3x3 getTranspose( const Matrix3x3& );
	Matrix3x3& Transpose( void );
	static Matrix3x3& Transpose( Matrix3x3& );
	Matrix3x3 getMinorMatrix( void ) const;
	Matrix3x3 getCofactorMatrix( void ) const;
	Matrix3x3 getAdjugateMatrix( void ) const;
	static Matrix3x3 getMinorMatrix( const Matrix3x3& );
	static Matrix3x3 getCofactorMatrix( const Matrix3x3& );
	static Matrix3x3 getAdjugateMatrix( const Matrix3x3& );
};

Matrix3x3 operator*( float64 lhs, const Matrix3x3& rhs );