﻿#include "stdafx.h"
#include "Console.h"
#include <conio.h>

static HANDLE g_consoleOutput = GetStdHandle( STD_OUTPUT_HANDLE );
static WORD g_originalColors = ( ( ( ( WORD )( Console::GetBackground() ) ) << ( 0x00000004 ) ) | ( ( WORD )( Console::GetForeground() ) ) );
static HWND g_consoleWindow = GetConsoleWindow();

ConsoleColor Console::GetForeground( void )
{
	CONSOLE_SCREEN_BUFFER_INFO info_;
	GetConsoleScreenBufferInfo( g_consoleOutput, &info_ );
	return ( ConsoleColor )( info_.wAttributes & 0x000fui16 );
}

void Console::SetForeground( ConsoleColor _attr )
{
	CONSOLE_SCREEN_BUFFER_INFO info_;
	GetConsoleScreenBufferInfo( g_consoleOutput, &info_ );
	SetConsoleTextAttribute( g_consoleOutput, info_.wAttributes & 0x00f0ui16 | ( ( WORD )_attr ) );
}

ConsoleColor Console::GetBackground( void )
{
	CONSOLE_SCREEN_BUFFER_INFO info_;
	GetConsoleScreenBufferInfo( g_consoleOutput, &info_ );
	return ( ConsoleColor )( ( info_.wAttributes & 0x00f0ui16 ) >> 0x00000004 );
}

void Console::SetBackground( ConsoleColor _attr )
{
	CONSOLE_SCREEN_BUFFER_INFO info_;
	GetConsoleScreenBufferInfo( g_consoleOutput, &info_ );
	SetConsoleTextAttribute( g_consoleOutput, info_.wAttributes & 0x000fui16 | ( ( WORD )_attr ) << 0x00000004 );
}

void Console::SetColors( ConsoleColor _foreground, ConsoleColor _background )
{
	SetConsoleTextAttribute( g_consoleOutput, ( ( ( ( WORD )( _background ) ) << ( 0x00000004 ) ) | ( ( WORD )( _foreground ) ) ) );
}

void Console::GetColors( ConsoleColor& _foreground, ConsoleColor& _background )
{
	CONSOLE_SCREEN_BUFFER_INFO info_;
	GetConsoleScreenBufferInfo( g_consoleOutput, &info_ );
	_foreground = ( ConsoleColor )( info_.wAttributes & 0x000fui16 );
	_background = ( ConsoleColor )( ( info_.wAttributes & 0x00f0ui16 ) >> 0x00000004 );
}

void Console::ResetColor( void )
{
	SetConsoleTextAttribute( g_consoleOutput, g_originalColors );
}

uint32 Console::WindowWidth( void )
{
	CONSOLE_SCREEN_BUFFER_INFO info_;
	GetConsoleScreenBufferInfo( g_consoleOutput, &info_ );
	return ( ( uint32 )( info_.srWindow.Right - info_.srWindow.Left + 0x0001i16 ) );
}

uint32 Console::WindowHeight( void )
{
	CONSOLE_SCREEN_BUFFER_INFO info_;
	GetConsoleScreenBufferInfo( g_consoleOutput, &info_ );
	return ( ( uint32 )( info_.srWindow.Bottom - info_.srWindow.Top + 0x0001i16 ) );
}

void Console::SetWindowSize( uint32 _columns, uint32 _rows )
{
	COORD c_ = GetLargestConsoleWindowSize( g_consoleOutput );
	if ( _columns < 0x00000001u || ( ( uint32 )( c_.X ) ) < _columns )
		_columns = ( uint32 )c_.X;
	if ( _rows < 0x00000001u || ( ( uint32 )( c_.Y ) ) < _rows )
		_rows = ( uint32 )c_.Y;

	SMALL_RECT dim_ = { 0x0000i16, 0x0000i16, ( SHORT )_columns - 0x0001i16, ( SHORT )_rows - 0x0001i16 };
	SetConsoleWindowInfo( g_consoleOutput, TRUE, &dim_ );
}

void Console::SetBufferSize( uint32 _columns, uint32 _rows )
{
	COORD c_ = { ( SHORT )_columns, ( SHORT )_rows };
	SetConsoleScreenBufferSize( g_consoleOutput, c_ );
}

void Console::CursorVisible( BOOL _visible )
{
	CONSOLE_CURSOR_INFO curs_;
	GetConsoleCursorInfo( g_consoleOutput, &curs_ );
	if ( curs_.bVisible != _visible )
	{
		curs_.bVisible = _visible;
		SetConsoleCursorInfo( g_consoleOutput, &curs_ );
	}
}

void Console::Clear( void )
{
	CONSOLE_SCREEN_BUFFER_INFO info_;
	GetConsoleScreenBufferInfo( g_consoleOutput, &info_ );

	DWORD len_ = ( DWORD )info_.dwSize.X, num_;
	COORD c_ = { 0x0000i16, 0x0000i16 };
	for ( SHORT i = 0x0000i16; i < info_.dwSize.Y; ++i )
	{
		c_.Y = i;
		FillConsoleOutputAttribute( g_consoleOutput, info_.wAttributes, len_, c_, &num_ );
		FillConsoleOutputCharacter( g_consoleOutput, L' ', len_, c_, &num_ );
	}

	SetCursorPosition( 0x0000i16, 0x0000i16 );
}

SHORT Console::CursorLeft( void )
{
	CONSOLE_SCREEN_BUFFER_INFO info_;
	GetConsoleScreenBufferInfo( g_consoleOutput, &info_ );
	return info_.dwCursorPosition.X;
}

SHORT Console::CursorTop( void )
{
	CONSOLE_SCREEN_BUFFER_INFO info_;
	GetConsoleScreenBufferInfo( g_consoleOutput, &info_ );
	return info_.dwCursorPosition.Y;
}

void Console::SetCursorPosition( SHORT _left, SHORT _top )
{
	CONSOLE_SCREEN_BUFFER_INFO info_;
	GetConsoleScreenBufferInfo( g_consoleOutput, &info_ );

	if ( _left >= info_.dwSize.X ) _left = info_.dwSize.X - 0x0001i16;
	else if ( _left < 0x0000i16 ) _left = 0x0000i16;
	if ( _top >= info_.dwSize.Y ) _top = info_.dwSize.Y - 0x0001i16;
	else if ( _top < 0x0000i16 ) _top = 0x0000i16;

	COORD cp_ = { _left, _top };
	SetConsoleCursorPosition( g_consoleOutput, cp_ );
}

void Console::Lock( bool _lock )
{
	LockWindowUpdate( _lock ? g_consoleWindow : nullptr );
}

void Console::EOLWrap( bool _on )
{
	DWORD mode_;
	GetConsoleMode( g_consoleOutput, &mode_ );
	if ( _on )
		mode_ |= ENABLE_WRAP_AT_EOL_OUTPUT;
	else
		mode_ &= ~ENABLE_WRAP_AT_EOL_OUTPUT;
	SetConsoleMode( g_consoleOutput, mode_ );
}

void Console::FlushKeys( void )
{
	for ( int i = 0x00000000; i < 0x00000100; ++i )
		( void )GetAsyncKeyState( i );
	while ( _kbhit() )
		( void )_getch();
}

void Console::Show( SHORT _x, SHORT _y, wchar_t _symbol )
{
	static bool imbued_;
	if ( !imbued_ )
	{
		std::wcout.imbue( std::locale( ".OCP" ) );
		imbued_ = true;
	}
	SetCursorPosition( _x, _y );
	std::wcout << _symbol;
}

void Console::DrawBox( SHORT _left, SHORT _top, SHORT _width, SHORT _height, bool _dbl )
{
	static const wchar_t* const SingleLine_ = L"┌─┐│└┘";
	static const wchar_t* const DoubleLine_ = L"╔═╗║╚╝";
	const wchar_t* const Set_ = _dbl ? DoubleLine_ : SingleLine_;
	SHORT i_, x_, y_;

	Show( _left, _top, Set_[ 0x00000000 ] );
	for ( i_ = 0x0000i16; i_ < _width - 0x0002i16; ++i_ )
		std::wcout << Set_[ 0x00000001 ];
	std::wcout << Set_[ 0x00000002 ];

	x_ = _left + _width - 0x0001i16;
	y_ = _top + 0x0001i16;
	for ( i_ = 0x0000i16; i_ < _height - 0x0002i16; ++i_, ++y_ )
	{
		Show( _left, y_, Set_[ 0x00000003 ] );
		Show( x_, y_, Set_[ 0x00000003 ] );
	}

	y_ = _top + _height - 0x0001i16;
	Show( _left, y_, Set_[ 0x00000004 ] );
	for ( i_ = 0x0000i16; i_ < _width - 0x0002i16; ++i_ )
		std::wcout << Set_[ 0x00000001 ];
	std::wcout << Set_[ 0x00000005 ];
}

const char* Console::RandomName( void )
{
	static const char* const Cons_ = "bcdfghjklmnpqrstvwxyz";
	static const char* const Vows_ = "aeiouy";
	static char name_[ 0x00000008 ];
	const uint8 nameLen_ = ( uint8 )( rand() % 0x00000005 ) + 0x03ui8;
	bool putAConsHere_ = ( rand() & 0x00000001 ) != 0x00000000;
	for ( uint8 i = 0x00ui8; i < nameLen_; ++i, putAConsHere_ = !putAConsHere_ )
		name_[ i ] = putAConsHere_ ? Cons_[ rand() % 0x00000015 ] : Vows_[ rand() % 0x00000006 ];
	name_[ nameLen_ ] = 0x00i8;
	name_[ 0x00000000 ] -= 0x20i8;
	return name_;
}

void Console::WordWrap( SHORT _x, SHORT _y, uint32 _width, const char* const _text )
{
	uint32 count_ = ( unsigned int )strlen( _text ), i_, j_, k_;
	for ( i_ = 0x00000000u; i_ < count_; i_ = j_, ++_y )
	{
		j_ = i_ + _width;
		while ( ( j_ < count_ ) && ( 0x20i8 != _text[ j_ ] ) ) --j_;
		if ( j_ > count_ ) j_ = count_;
		SetCursorPosition( _x, _y );
		for ( k_ = i_; k_ < j_; ++k_ )
			std::cout << _text[ k_ ];
		while ( 0x20i8 == _text[ j_ ] ) ++j_;
	}
}