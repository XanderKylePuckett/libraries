#include "stdafx.h"
#include "Uint128Divide.h"

uint128::uint128( void )
{
}
uint128::uint128( const uint128& rhs ) :
	left( rhs.left ), right( rhs.right )
{
}
uint128::uint128( int8 rhs ) :
	left( rhs >= 0i64 ? 0ui64 : 0xffffffffffffffffui64 ),
	right( ( uint64 )( ( int64 )rhs ) )
{
}
uint128::uint128( int16 rhs ) :
	left( rhs >= 0i64 ? 0ui64 : 0xffffffffffffffffui64 ),
	right( ( uint64 )( ( int64 )rhs ) )
{
}
uint128::uint128( int32 rhs ) :
	left( rhs >= 0i64 ? 0ui64 : 0xffffffffffffffffui64 ),
	right( ( uint64 )( ( int64 )rhs ) )
{
}
uint128::uint128( int64 rhs ) :
	left( rhs >= 0i64 ? 0ui64 : 0xffffffffffffffffui64 ),
	right( ( uint64 )rhs )
{
}
uint128::uint128( uint8 rhs ) :
	left( 0ui64 ), right( ( uint64 )rhs )
{
}
uint128::uint128( uint16 rhs ) :
	left( 0ui64 ), right( ( uint64 )rhs )
{
}
uint128::uint128( uint32 rhs ) :
	left( 0ui64 ), right( ( uint64 )rhs )
{
}
uint128::uint128( uint64 rhs ) :
	left( 0ui64 ), right( rhs )
{
}
uint128::uint128( float32 )
{
	// TODO
}
uint128::uint128( float64 )
{
	// TODO
}
uint128::uint128( const float128& )
{
	// TODO
}
uint128::uint128( bool rhs ) :
	left( 0ui64 ), right( rhs ? 1ui64 : 0ui64 )
{
}
uint128::uint128( uint64 l, uint64 r ) :
	left( l ), right( r )
{
}
uint128& uint128::operator=( const uint128& rhs )
{
	if ( this != &rhs )
	{
		left = rhs.left;
		right = rhs.right;
	}
	return *this;
}
uint128 uint128::operator+( void ) const
{
	return *this;
}
uint128 uint128::operator-( void ) const
{
	return ( ~( *this ) ) + ONE;
}
bool overflows( uint64 a, uint64 b )
{
	static const uint64 highBit = 0x8000000000000000ui64;
	if ( a < highBit && b < highBit ) return false;
	if ( a >= highBit && b >= highBit ) return true;
	const uint64& mx = a > b ? a : b;
	const uint64& mn = a < b ? a : b;
	return ( ~mx ) < mn;
}
uint128 uint128::operator+( const uint128& rhs ) const
{
	uint128 x( left + rhs.left, right + rhs.right );
	if ( overflows( right, rhs.right ) )
		++( x.left );
	return x;
}
uint128 uint128::operator-( const uint128& rhs ) const
{
	return ( *this ) + ( -rhs );
}
uint128 uint128::operator*( const uint128& rhs ) const
{
	uint128 ans( 0ui64, 0ui64 );
	const uint128& lhs = *this;
	for ( int32 i = 0; i < 128; ++i )
		if ( ZERO != ( ( ONE << i ) & lhs ) )
			ans += ( rhs << i );
	return ans;
}
uint128 uint128::operator/( const uint128& rhs ) const
{
	uint128 q, r;
	Uint128Divide( *this, rhs, q, r );
	return q;
}
uint128 uint128::operator<<( int32 rhs ) const
{
	if ( rhs < 0 )
		return ( *this ) >> ( -rhs );
	if ( 0 == rhs )
		return *this;
	if ( rhs >= 128 )
		return ZERO;
	uint64 l = left, r = right;
	for ( int32 i = 0; i < rhs; ++i )
	{
		l <<= 1;
		l &= ( r >> 63 );
		r <<= 1;
	}
	return uint128( l, r );
}
uint128 uint128::operator >> ( int32 rhs ) const
{
	if ( rhs < 0 )
		return ( *this ) << ( -rhs );
	if ( 0 == rhs )
		return *this;
	if ( rhs >= 128 )
		return ZERO;
	uint64 l = left, r = right;
	for ( int32 i = 0; i < rhs; ++i )
	{
		r >>= 1;
		r &= ( l << 63 );
		l >>= 1;
	}
	return uint128( l, r );
}
uint128 uint128::operator<<( const uint128& rhs ) const
{
	if ( rhs.left > 0ui64 || rhs.right >= 128ui64 )
		return ZERO;
	return ( *this ) >> ( ( int32 )( rhs.left ) );
}
uint128 uint128::operator >> ( const uint128& rhs ) const
{
	if ( rhs.left > 0ui64 || rhs.right >= 128ui64 )
		return ZERO;
	return ( *this ) >> ( ( int32 )( rhs.right ) );
}
uint128 uint128::operator&( const uint128& rhs ) const
{
	return uint128( left & rhs.left, right & rhs.right );
}
uint128 uint128::operator|( const uint128& rhs ) const
{
	return uint128( left | rhs.left, right | rhs.right );
}
uint128 uint128::operator~( void ) const
{
	return uint128( ~left, ~right );
}
uint128 uint128::operator^( const uint128& rhs ) const
{
	return uint128( left ^ rhs.left, right ^ rhs.right );
}
uint128 uint128::operator%( const uint128& rhs ) const
{
	uint128 q, r;
	Uint128Divide( *this, rhs, q, r );
	return r;
}
uint128& uint128::operator+=( const uint128& rhs )
{
	return *this = ( *this ) + rhs;
}
uint128& uint128::operator-=( const uint128& rhs )
{
	return *this = ( *this ) - rhs;
}
uint128& uint128::operator*=( const uint128& rhs )
{
	return *this = ( *this ) * rhs;
}
uint128& uint128::operator/=( const uint128& rhs )
{
	return *this = ( *this ) / rhs;
}
uint128& uint128::operator<<=( int32 rhs )
{
	return *this = ( *this ) << rhs;
}
uint128& uint128::operator>>=( int32 rhs )
{
	return *this = ( *this ) >> rhs;
}
uint128& uint128::operator<<=( const uint128& rhs )
{
	return *this = ( *this ) << rhs;
}
uint128& uint128::operator>>=( const uint128& rhs )
{
	return *this = ( *this ) >> rhs;
}
uint128& uint128::operator&=( const uint128& rhs )
{
	left &= rhs.left;
	right &= rhs.right;
	return *this;
}
uint128& uint128::operator|=( const uint128& rhs )
{
	left |= rhs.left;
	right |= rhs.right;
	return *this;
}
uint128& uint128::operator^=( const uint128& rhs )
{
	left ^= rhs.left;
	right ^= rhs.right;
	return *this;
}
uint128& uint128::operator%=( const uint128& rhs )
{
	return *this = ( *this ) % rhs;
}
bool uint128::operator==( const uint128& rhs ) const
{
	return left == rhs.left && right == rhs.right;
}
bool uint128::operator!=( const uint128& rhs ) const
{
	return left != rhs.left || right != rhs.right;
}
bool uint128::operator>=( const uint128& rhs ) const
{
	return ( rhs < *this ) || ( *this == rhs );
}
bool uint128::operator<=( const uint128& rhs ) const
{
	return ( *this < rhs ) || ( *this == rhs );
}
bool uint128::operator>( const uint128& rhs ) const
{
	return rhs < *this;
}
bool uint128::operator<( const uint128& rhs ) const
{
	if ( left < rhs.left )
		return true;
	else if ( left > rhs.left )
		return false;
	return right < rhs.right;
}
bool uint128::operator&&( const uint128& rhs ) const
{
	return ( left || right ) && ( rhs.left || rhs.right );
}
bool uint128::operator||( const uint128& rhs ) const
{
	return left || right || rhs.left || rhs.right;
}
bool uint128::operator!( void ) const
{
	return !( left || right );
}
uint128& uint128::operator++( void )
{
	return *this += uint128( 0ui64, 1ui64 );
}
uint128& uint128::operator--( void )
{
	return *this -= uint128( 0ui64, 1ui64 );
}
uint128 uint128::operator++( int )
{
	uint128 tmp = *this;
	++( *this );
	return tmp;
}
uint128 uint128::operator--( int )
{
	uint128 tmp = *this;
	--( *this );
	return tmp;
}
std::ostream& operator<<( std::ostream& os, const uint128& )
{
	// TODO
	return os;
}
bool float128::signBit( void ) const
{
	return ( bits.left & 0x8000000000000000ui64 ) != 0ui64;
}
uint16 float128::exponentBits( void ) const
{
	return ( uint16 )( ( bits.left & 0x7fff000000000000ui64 ) >> 48 );
}
uint128 float128::fractionBits( void ) const
{
	return uint128( bits.left & 0x0000ffffffffffffui64, bits.right );
}
float128::float128( void )
{
}
float128::float128( const float128& rhs ) :
	bits( rhs.bits )
{
}
float128::float128( int8 )
{
	// TODO
}
float128::float128( int16 )
{
	// TODO
}
float128::float128( int32 )
{
	// TODO
}
float128::float128( int64 )
{
	// TODO
}
float128::float128( uint8 )
{
	// TODO
}
float128::float128( uint16 )
{
	// TODO
}
float128::float128( uint32 )
{
	// TODO
}
float128::float128( uint64 )
{
	// TODO
}
float128::float128( const uint128& )
{
	// TODO
}
float128::float128( float32 )
{
	// TODO
}
float128::float128( float64 )
{
	// TODO
}
float128::float128( bool )
{
	// TODO
}
float128& float128::operator=( const float128& rhs )
{
	if ( this != &rhs )
	{
		bits = rhs.bits;
	}
	return *this;
}
float128 float128::operator+( void ) const
{
	// TODO
	return *this;
}
float128 float128::operator-( void ) const
{
	// TODO
	return *this;
}
float128 float128::operator+( const float128& ) const
{
	// TODO
	return *this;
}
float128 float128::operator-( const float128& ) const
{
	// TODO
	return *this;
}
float128 float128::operator*( const float128& ) const
{
	// TODO
	return *this;
}
float128 float128::operator/( const float128& ) const
{
	// TODO
	return *this;
}
float128& float128::operator+=( const float128& rhs )
{
	return *this = ( *this ) + rhs;
}
float128& float128::operator-=( const float128& rhs )
{
	return *this = ( *this ) - rhs;
}
float128& float128::operator*=( const float128& rhs )
{
	return *this = ( *this ) * rhs;
}
float128& float128::operator/=( const float128& rhs )
{
	return *this = ( *this ) / rhs;
}
bool float128::operator==( const float128& rhs ) const
{
	return IsEqual( *this, rhs );
}
bool float128::operator!=( const float128& rhs ) const
{
	return !IsEqual( *this, rhs );
}
bool float128::operator>=( const float128& rhs ) const
{
	return ( rhs < *this ) || ( *this == rhs );
}
bool float128::operator<=( const float128& rhs ) const
{
	return ( *this < rhs ) || ( *this == rhs );
}
bool float128::operator>( const float128& rhs ) const
{
	return rhs < *this;
}
bool float128::operator<( const float128& ) const
{
	// TODO
	return false;
}
bool float128::operator&&( const float128& rhs ) const
{
	return !( IsZero( *this ) || IsZero( rhs ) );
}
bool float128::operator||( const float128& rhs ) const
{
	return !( IsZero( *this ) && IsZero( rhs ) );
}
bool float128::operator!( void ) const
{
	return IsZero( *this );
}
std::ostream& operator<<( std::ostream& os, const float128& )
{
	// TODO
	return os;
}
float64 float128::GetFloat64( void ) const
{
	static const uint64 signMask = 0xc000000000000000ui64;
	static const uint64 roundMask = 0x0800000000000000ui64;
	const uint64 outBits =
		( bits.left & signMask ) |
		( ( ( bits.right >> 60 ) |
		( bits.left << 4 ) ) & ~signMask ) |
		  ( ( roundMask == ( bits.right & roundMask ) ) ? 1ui64 : 0ui64 );
	return *( ( const float64* )( &outBits ) );
}
float32 float128::GetFloat32( void ) const
{
	static const uint32 signMask = 0xc0000000ui32;
	static const uint32 roundMask = 0x01000000ui32;
	const uint32 outBits =
		( ( bits.left >> 25 ) & ~signMask ) |
		( ( bits.left >> 32 ) & signMask ) |
		( ( roundMask == ( bits.left & roundMask ) ) ? 1ui32 : 0ui32 );
	return *( ( const float32* )( &outBits ) );
}
bool float128::IsEqual( const float128&, const float128& )
{
	// TODO
	return false;
}
bool float128::IsZero( const float128& )
{
	// TODO
	return false;
}
bool float128::IsInfinity( const float128& )
{
	// TODO
	return false;
}
bool float128::IsNaN( const float128& )
{
	// TODO
	return false;
}