#include "stdafx.h"
#include "Raster.h"
#include <future>
#pragma warning(disable:6297 6387)

#define MIN_RASTER_SIZE (320u)

static HWND g_window = nullptr;
static HDC g_windowDC = nullptr;
static std::thread g_windowHandler;
static DWORD g_windowHandlerID = ULONG_MAX;
static std::atomic_bool g_windowClosed;
static unsigned int* g_bitmap = nullptr;
static unsigned int g_bitmapWidth = 0u;
static unsigned int g_bitmapHeight = 0u;
static std::mutex g_bitmapMutex;
static std::condition_variable g_bitmapRedraw;
static std::future<unsigned int*> g_bitmapAllocator;
static std::atomic_bool g_bitmapPresent;
static Texture2D g_Window_Texture;
static bool g_isRasterInited = false;
static bool g_isRasterShutdown = false;

LRESULT CALLBACK WndProc( HWND _hWnd, UINT _msg, WPARAM _wParam, LPARAM _lParam )
{
	if ( WM_DESTROY == _msg )
	{
		g_windowClosed = true;
		g_bitmapRedraw.notify_one();
		g_window = nullptr;
		PostQuitMessage( 0 );
	}
	return DefWindowProcW( _hWnd, _msg, _wParam, _lParam );
}
bool PresentFrame( void )
{
	if ( g_bitmap && g_window && g_windowDC )
	{
		std::unique_lock<std::mutex> pixelLock_( g_bitmapMutex );
		BITMAPINFO toDraw_;
		ZEROSTRUCT( toDraw_ );
		toDraw_.bmiHeader.biSize = sizeof( BITMAPINFOHEADER );
		toDraw_.bmiHeader.biWidth = g_bitmapWidth;
		toDraw_.bmiHeader.biHeight = -int( g_bitmapHeight );
		toDraw_.bmiHeader.biPlanes = 1ui16;
		toDraw_.bmiHeader.biBitCount = 32ui16;
		toDraw_.bmiHeader.biCompression = BI_RGB;
		SetDIBitsToDevice( g_windowDC, 0, 0, g_bitmapWidth, g_bitmapHeight, 0, 0, 0, g_bitmapHeight, g_bitmap, &toDraw_, DIB_RGB_COLORS );
		g_bitmapPresent = false;
		g_bitmapRedraw.notify_one();
		return true;
	}
	return false;
}
void ProcessRasterSurface( unsigned int _width, unsigned int _height, std::promise<unsigned int*>&& _bitmapInit )
{
	unsigned int* frontbuffer_ = ( unsigned int* )VirtualAlloc( nullptr, ( SIZE_T )( ( _width * _height ) << 2 ),
																MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE );
	_bitmapInit.set_value( frontbuffer_ );
	WNDCLASSEX wndClass_;
	ZEROSTRUCT( wndClass_ );
	wndClass_.cbSize = sizeof( wndClass_ );
	wndClass_.style = CS_OWNDC;
	wndClass_.lpfnWndProc = WndProc;
	wndClass_.lpszClassName = L"TheWindowOOO";
	wndClass_.hInstance = GetModuleHandleW( 0 );
	wndClass_.hCursor = LoadCursorW( 0, IDC_ARROW );
	wndClass_.hbrBackground = ( HBRUSH )( COLOR_WINDOWFRAME );
	wndClass_.hIcon = LoadIconW( 0, IDI_APPLICATION );
	RegisterClassExW( &wndClass_ );
	RECT windowSize_ = { 0l, 0l, ( LONG )_width, ( LONG )_height };
	AdjustWindowRect( &windowSize_, WS_OVERLAPPEDWINDOW, false );
	g_window = CreateWindowW( L"TheWindowOOO", L"the window :o",
							  WS_OVERLAPPEDWINDOW & ~( WS_THICKFRAME | WS_MAXIMIZEBOX ),
							  CW_USEDEFAULT, CW_USEDEFAULT, windowSize_.right - windowSize_.left,
							  windowSize_.bottom - windowSize_.top,
							  NULL, NULL, GetModuleHandleW( 0 ), 0 );
	if ( g_window )
	{
		ShowWindow( g_window, SW_SHOW );
		g_windowDC = GetDC( g_window );
		MSG msg_;
		ZEROSTRUCT( msg_ );
		while ( msg_.message != WM_QUIT )
		{
			if ( PeekMessageW( &msg_, 0, 0, 0, PM_REMOVE ) )
			{
				if ( nullptr == msg_.hwnd )
					msg_.hwnd = g_window;
				TranslateMessage( &msg_ );
				DispatchMessageW( &msg_ );
			}
			if ( g_bitmapPresent )
				PresentFrame();
		}
	}
	VirtualFree( frontbuffer_, 0, MEM_RELEASE );
	g_bitmap = nullptr;
	UnregisterClassW( L"TheWindowOOO", GetModuleHandleW( nullptr ) );
}
void RS_Shutdown( void )
{
	PostThreadMessageW( g_windowHandlerID, WM_DESTROY, 0ull, 0ll );
	g_windowHandler.join();
	g_window = nullptr;
	g_windowDC = nullptr;
	g_bitmap = nullptr;
}
BOOL WINAPI ConsoleCtrlHandler( DWORD _ctrlCode )
{
	if ( CTRL_BREAK_EVENT == _ctrlCode ||
		 CTRL_CLOSE_EVENT == _ctrlCode ||
		 CTRL_LOGOFF_EVENT == _ctrlCode ||
		 CTRL_SHUTDOWN_EVENT == _ctrlCode )
		RS_Shutdown();
	return FALSE;
}
void RS_Initialize( uint32 _width, uint32 _height )
{
	g_bitmapPresent = false;
	g_windowClosed = false;
	g_bitmapWidth = _width;
	g_bitmapHeight = _height;
	std::promise<unsigned int*>	bitmapGen_;
	g_bitmapAllocator = bitmapGen_.get_future();
	g_windowHandler = std::thread( ProcessRasterSurface, _width, _height, std::move( bitmapGen_ ) );
	g_windowHandlerID = GetThreadId( static_cast< HANDLE >( g_windowHandler.native_handle() ) );
	SetConsoleCtrlHandler( ConsoleCtrlHandler, TRUE );
}
bool RS_Update( const Color* _argbPixels, uint32 _arrSize )
{
	if ( g_bitmapAllocator.valid() )
		g_bitmap = g_bitmapAllocator.get();
	if ( g_bitmap )
	{
		std::unique_lock<std::mutex> pixelLock_( g_bitmapMutex );
		g_bitmapRedraw.wait( pixelLock_, [ & ] ()
		{
			return !g_bitmapPresent || g_windowClosed;
		} );
		if ( g_windowClosed ) return false;
		memcpy_s( g_bitmap, _arrSize, _argbPixels, _arrSize );
		g_bitmapPresent = true;
		return true;
	}
	return false;
}
void Raster::InitWindow( uint32 _width, uint32 _height )
{
	if ( !g_isRasterInited )
	{
		if ( _width < MIN_RASTER_SIZE ) _width = MIN_RASTER_SIZE;
		if ( _height < MIN_RASTER_SIZE ) _height = MIN_RASTER_SIZE;
		g_Window_Texture = Texture2D( _width, _height );
		g_Window_Texture.m_sizeLocked = true;
		RS_Initialize( _width, _height );
		g_isRasterInited = true;
	}
}
bool Raster::UpdateWindow( void )
{
	if ( g_isRasterShutdown )
		return false;
	if ( !g_isRasterInited )
		InitWindow( MIN_RASTER_SIZE, MIN_RASTER_SIZE );
	return RS_Update( g_Window_Texture.m_pixels, g_Window_Texture.m_numPixels << 2 );
}
void Raster::ShutdownWindow( void )
{
	if ( !g_isRasterShutdown )
	{
		g_Window_Texture.m_sizeLocked = false;
		g_isRasterShutdown = true;
		RS_Shutdown();
	}
}
Texture2D& Raster::GetTexture( void )
{
	if ( !g_isRasterInited )
		InitWindow( MIN_RASTER_SIZE, MIN_RASTER_SIZE );
	return g_Window_Texture;
}
HWND Raster::GetWindow( void )
{
	return g_window;
}