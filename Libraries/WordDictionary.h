#pragma once
#include "HashTable.h"
#include "String.h"

class WordDictionary
{
	friend class Scrabble;
private:
	HashTable<String> m_words;
public:
	WordDictionary( void );
	WordDictionary( const WordDictionary& );
	WordDictionary( WordDictionary&& );
	WordDictionary& operator=( const WordDictionary& );
	WordDictionary& operator=( WordDictionary&& );
	WordDictionary& operator+=( const WordDictionary& );
	~WordDictionary( void );
	void AddWord( const String& );
	bool HasWord( const String& ) const;
	static WordDictionary Import( const char* const,
								  bool( *_validator )( const String& ) = [ ] ( const String& ) -> bool
	{
		return true;
	},
								  void( *_conformer )( String& ) = [ ] ( String& )
	{ } );
	static WordDictionary Import( const char* const,
								  void( *_conformer )( String& ) = [ ] ( String& )
	{ },
								  bool( *_validator )( const String& ) = [ ] ( const String& ) -> bool
	{
		return true;
	} );
	static void Export( const WordDictionary&, const char* const, bool _append = false );
	WordDictionary& OptimizeSpace( void );
};