#include "stdafx.h"
#include "AsciiFont.h"
#include "Texture2D.h"
#include "Math.h"
#include "BitFile.h"

std::string AsciiFont::GenerateAscfFileFromBmp( const std::string& _bmp )
{
	static const float64 lightnessThreshold_ = 1.0 - Const_sqrt2Div2;
	const Texture2D texture_ = Texture2D::GetTexture2DFromBmp( _bmp );
	const uint32 letterWidth_ = texture_.GetWidth() >> 4u;
	const uint32 letterHeight_ = texture_.GetHeight() >> 4u;
	std::string ascf_ = _bmp;
	{
		const uint32 x = ( uint32 )ascf_.length();
		ascf_[ x - 1u ] = 'c';
		ascf_[ x - 2u ] = 's';
		ascf_[ x - 3u ] = 'a';
	}
	ascf_ += 'f';
	BitFile file_( ascf_, BitFileMode::BITFILE_OUT );
	if ( file_.IsOpen() )
	{
		file_.WriteData( ( char* )&letterWidth_, 4u );
		file_.WriteData( ( char* )&letterHeight_, 4u );
		uint32 letterY_, letterX_, pixelY_, pixelX_;
		for ( letterY_ = 0u; letterY_ < 16u; ++letterY_ ) for ( letterX_ = 0u; letterX_ < 16u; ++letterX_ ) for ( pixelY_ = 0u; pixelY_ < letterHeight_; ++pixelY_ ) for ( pixelX_ = 0u; pixelX_ < letterWidth_; ++pixelX_ )
			file_.WriteBit( HSLColor( texture_.GetPixel_Ref( letterX_ * letterWidth_ + pixelX_, letterY_ * letterHeight_ + pixelY_ ) ).lightness > lightnessThreshold_ );
		file_.Close();
	}
	return ascf_;
}
AsciiFont& AsciiFont::LoadAscfFile( const std::string& _ascf, const Color& _fore, const Color& _back )
{
	BitFile file_( _ascf, BitFileMode::BITFILE_IN );
	if ( file_.IsOpen() )
	{
		file_.ReadData( ( char* )&m_charWidth, 4u );
		file_.ReadData( ( char* )&m_charHeight, 4u );
		delete[ ] m_characters;
		m_characters = new Texture2D[ 256u ];
		const uint32 numPixels_ = m_charWidth * m_charHeight;
		uint32 i, j;
		Color* pixel_;
		for ( i = 0u; i < 256u; ++i )
		{
			m_characters[ i ].SetSize( m_charWidth, m_charHeight );
			pixel_ = &( m_characters[ i ].GetPixel_Ref( 0u, 0u ) );
			for ( j = 0u; j < numPixels_; ++j )
				pixel_[ j ] = file_.ReadBit() ? _fore : _back;
		}
		file_.Close();
	}
	return *this;
}
AsciiFont::AsciiFont( void ) :
	m_characters( nullptr ),
	m_charWidth( 0u ),
	m_charHeight( 0u )
{
}
AsciiFont::AsciiFont( const std::string& _ascf, const Color& _fore, const Color& _back ) :
	m_characters( nullptr ),
	m_charWidth( 0u ),
	m_charHeight( 0u )
{
	LoadAscfFile( _ascf, _fore, _back );
}
AsciiFont::AsciiFont( const AsciiFont& _rhs ) :
	m_charWidth( _rhs.m_charWidth ),
	m_charHeight( _rhs.m_charHeight )
{
	if ( nullptr != _rhs.m_characters )
	{
		m_characters = new Texture2D[ 256u ];
		for ( uint32 i = 0u; i < 256u; ++i )
			m_characters[ i ] = _rhs.m_characters[ i ];
	}
	else
		m_characters = nullptr;
}
AsciiFont::AsciiFont( AsciiFont&& _rhs ) :
	m_characters( std::move( _rhs.m_characters ) ),
	m_charWidth( std::move( _rhs.m_charWidth ) ),
	m_charHeight( std::move( _rhs.m_charHeight ) )
{
	_rhs.m_characters = nullptr;
}
AsciiFont& AsciiFont::operator=( const AsciiFont& _rhs )
{
	if ( this != &_rhs )
	{
		delete[ ] m_characters;
		if ( nullptr != _rhs.m_characters )
		{
			m_charWidth = _rhs.m_charWidth;
			m_charHeight = _rhs.m_charHeight;
			m_characters = new Texture2D[ 256u ];
			for ( uint32 i = 0u; i < 256u; ++i )
				m_characters[ i ] = _rhs.m_characters[ i ];
		}
		else
		{
			m_charWidth = _rhs.m_charWidth;
			m_charHeight = _rhs.m_charHeight;
			m_characters = nullptr;
		}
	}
	return *this;
}
AsciiFont& AsciiFont::operator=( AsciiFont&& _rhs )
{
	if ( this != &_rhs )
	{
		delete[ ] m_characters;
		m_characters = std::move( _rhs.m_characters );
		m_charWidth = std::move( _rhs.m_charWidth );
		m_charHeight = std::move( _rhs.m_charHeight );
		_rhs.m_characters = nullptr;
	}
	return *this;
}
AsciiFont::~AsciiFont( void )
{
	delete[ ] m_characters;
}
const Texture2D& AsciiFont::GetCharacter( uint8 _char ) const
{
	return m_characters[ _char ];
}
const Texture2D& AsciiFont::GetCharacter( char _char ) const
{
	return m_characters[ ( uint8 )_char ];
}
const uint32 AsciiFont::GetCharacterWidth( void ) const
{
	return m_charWidth;
}
const uint32 AsciiFont::GetCharacterHeight( void ) const
{
	return m_charHeight;
}