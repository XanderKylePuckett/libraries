#pragma once
#include "Texture2D.h"

class AsciiFont
{
private:
	Texture2D* m_characters;
	uint32 m_charWidth, m_charHeight;
public:
	static std::string GenerateAscfFileFromBmp( const std::string& );
	AsciiFont( void );
	AsciiFont( const std::string&, const Color& = Color::WHITE, const Color& = Color::ZERO );
	AsciiFont( const AsciiFont& );
	AsciiFont( AsciiFont&& );
	AsciiFont& operator=( const AsciiFont& );
	AsciiFont& operator=( AsciiFont&& );
	~AsciiFont( void );
	AsciiFont& LoadAscfFile( const std::string&, const Color& = Color::WHITE, const Color& = Color::ZERO );
	const Texture2D& GetCharacter( uint8 ) const;
	const Texture2D& GetCharacter( char ) const;
	const uint32 GetCharacterWidth( void ) const;
	const uint32 GetCharacterHeight( void ) const;
};