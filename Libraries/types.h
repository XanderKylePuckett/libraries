#pragma once

typedef char int8;
typedef short int16;
typedef int int32;
typedef long long int64;
typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int uint32;
typedef unsigned long long uint64;
typedef float float32;
typedef double float64;

struct uint128
{
private:
	uint64 left;
	uint64 right;
public:
	static const uint128 ZERO, ONE;
	friend struct float128;
	uint128( void );
	uint128( const uint128& );
	uint128( int8 );
	uint128( int16 );
	uint128( int32 );
	uint128( int64 );
	uint128( uint8 );
	uint128( uint16 );
	uint128( uint32 );
	uint128( uint64 );
	uint128( float32 );
	uint128( float64 );
	uint128( const float128& );
	uint128( bool );
	uint128( uint64 l, uint64 r );
	uint128& operator=( const uint128& );
	uint128 operator+( void ) const;
	uint128 operator-( void ) const;
	uint128 operator+( const uint128& ) const;
	uint128 operator-( const uint128& ) const;
	uint128 operator*( const uint128& ) const;
	uint128 operator/( const uint128& ) const;
	uint128 operator<<( int32 ) const;
	uint128 operator >> ( int32 ) const;
	uint128 operator<<( const uint128& ) const;
	uint128 operator >> ( const uint128& ) const;
	uint128 operator&( const uint128& ) const;
	uint128 operator|( const uint128& ) const;
	uint128 operator~( void ) const;
	uint128 operator^( const uint128& ) const;
	uint128 operator%( const uint128& ) const;
	uint128& operator+=( const uint128& );
	uint128& operator-=( const uint128& );
	uint128& operator*=( const uint128& );
	uint128& operator/=( const uint128& );
	uint128& operator<<=( int32 );
	uint128& operator>>=( int32 );
	uint128& operator<<=( const uint128& );
	uint128& operator>>=( const uint128& );
	uint128& operator&=( const uint128& );
	uint128& operator|=( const uint128& );
	uint128& operator^=( const uint128& );
	uint128& operator%=( const uint128& );
	bool operator==( const uint128& ) const;
	bool operator!=( const uint128& ) const;
	bool operator>=( const uint128& ) const;
	bool operator<=( const uint128& ) const;
	bool operator>( const uint128& ) const;
	bool operator<( const uint128& ) const;
	bool operator&&( const uint128& ) const;
	bool operator||( const uint128& ) const;
	bool operator!( void ) const;
	uint128& operator++( void );
	uint128& operator--( void );
	uint128 operator++( int );
	uint128 operator--( int );
	friend std::ostream& operator<<( std::ostream&, const uint128& );
};

struct float128
{
private:
	uint128 bits;
	bool signBit( void ) const;
	uint16 exponentBits( void ) const;
	uint128 fractionBits( void ) const;

public:
	float128( void );
	float128( const float128& );
	float128( int8 );
	float128( int16 );
	float128( int32 );
	float128( int64 );
	float128( uint8 );
	float128( uint16 );
	float128( uint32 );
	float128( uint64 );
	float128( const uint128& );
	float128( float32 );
	float128( float64 );
	float128( bool );
	float128& operator=( const float128& );
	float128 operator+( void ) const;
	float128 operator-( void ) const;
	float128 operator+( const float128& ) const;
	float128 operator-( const float128& ) const;
	float128 operator*( const float128& ) const;
	float128 operator/( const float128& ) const;
	float128& operator+=( const float128& );
	float128& operator-=( const float128& );
	float128& operator*=( const float128& );
	float128& operator/=( const float128& );
	bool operator==( const float128& ) const;
	bool operator!=( const float128& ) const;
	bool operator>=( const float128& ) const;
	bool operator<=( const float128& ) const;
	bool operator>( const float128& ) const;
	bool operator<( const float128& ) const;
	bool operator&&( const float128& ) const;
	bool operator||( const float128& ) const;
	bool operator!( void ) const;
	static bool IsEqual( const float128&, const float128& );
	static bool IsZero( const float128& );
	static bool IsInfinity( const float128& );
	static bool IsNaN( const float128& );
	friend std::ostream& operator<<( std::ostream&, const float128& );
	float64 GetFloat64( void ) const;
	float32 GetFloat32( void ) const;
};