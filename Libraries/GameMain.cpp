#include "stdafx.h"
#include "Console.h"
#include "Vector3.h"
#include "DynArray.h"

class GameObject
{
private:
	static uint64 idCounter;
protected:
	Vector3 m_position;
	const uint64 m_id;
	bool m_active;
public:
	GameObject( void );
	GameObject( const GameObject& );
	GameObject( GameObject&& );
	virtual GameObject& operator=( const GameObject& );
	virtual GameObject& operator=( GameObject&& );
	virtual ~GameObject( void );
	bool IsActive( void ) const;
	void SetActive( bool = true );
	uint64 GetID( void ) const;
	virtual void Init( void ) = 0;
	virtual void Input( void ) = 0;
	virtual void Update( uint64 _frame ) = 0;
	virtual void Render( void ) const = 0;
	virtual void Shutdown( void ) = 0;
};

uint64 GameObject::idCounter = 0ull;
GameObject::GameObject( void ) :
	m_id( ++idCounter ), m_active( true ), m_position( Vector3::ZERO )
{
}
GameObject::GameObject( const GameObject& _rhs ) :
	m_id( ++idCounter ),
	m_active( _rhs.m_active ),
	m_position( _rhs.m_position )
{
}
GameObject::GameObject( GameObject&& _rhs ) :
	m_id( ++idCounter ),
	m_active( std::move( _rhs.m_active ) ),
	m_position( std::move( _rhs.m_position ) )
{
}
GameObject& GameObject::operator=( const GameObject& _rhs )
{
	if ( this != &_rhs )
	{
		m_active = _rhs.m_active;
		m_position = _rhs.m_position;
	}
	return *this;
}
GameObject& GameObject::operator=( GameObject&& _rhs )
{
	if ( this != &_rhs )
	{
		m_active = std::move( _rhs.m_active );
		m_position = std::move( _rhs.m_position );
	}
	return *this;
}
GameObject::~GameObject( void )
{
}
bool GameObject::IsActive( void ) const
{
	return m_active;
}
void GameObject::SetActive( bool _active )
{
	m_active = _active;
}
uint64 GameObject::GetID( void ) const
{
	return m_id;
}

class Game
{
private:
	bool m_gameRunning;
	DynArray<GameObject*> m_objects;
public:
	Game( void );
	~Game( void );
	void Init( void );
	void Input( void );
	void Update( uint64 _frame );
	void Render( void ) const;
	bool IsRunning( void );
	void Shutdown( void );
};

class Player : public GameObject
{
private:
	bool m_alive;
public:
	Player( void );
	~Player( void );
	virtual void Init( void );
	virtual void Input( void );
	virtual void Update( uint64 _frame );
	virtual void Render( void ) const;
	virtual void Shutdown( void );
	void Die( void );
	bool IsAlive( void ) const;
};

Player::Player( void ) : GameObject(),
m_alive( true )
{
}
Player::~Player( void )
{
}
void Player::Init( void )
{
}
void Player::Input( void )
{
}
void Player::Update( uint64 _frame )
{
	if ( _frame > 1000ull )
		Die();
}
void Player::Render( void ) const
{
	std::cout << '#';
}
void Player::Shutdown( void )
{
}
void Player::Die( void )
{
	m_alive = false;
}
bool Player::IsAlive( void ) const
{
	return m_alive;
}

class Enemy : public GameObject
{
public:
	Enemy( void );
	~Enemy( void );
	virtual void Init( void );
	virtual void Input( void );
	virtual void Update( uint64 _frame );
	virtual void Render( void ) const;
	virtual void Shutdown( void );
};

Enemy::Enemy( void ) : GameObject()
{
}
Enemy::~Enemy( void )
{
}
void Enemy::Init( void )
{
}
void Enemy::Input( void )
{
}
void Enemy::Update( uint64 )
{
}
void Enemy::Render( void ) const
{
}
void Enemy::Shutdown( void )
{
}

Game::Game( void ) :
	m_gameRunning( false )
{
}
Game::~Game( void )
{
}
void Game::Init( void )
{
	if ( m_objects.GetSize() > 0u )
		Shutdown();
	m_objects.AddItem( new Player );
	for ( uint32 i = 0u; i < 100u; ++i )
		m_objects.AddItem( new Enemy );
	for ( uint32 i = 0u; i < m_objects.GetSize(); ++i )
		m_objects[ i ]->Init();
	m_gameRunning = true;
}
void Game::Input( void )
{
	for ( uint32 i = 0u; i < m_objects.GetSize(); ++i )
		m_objects[ i ]->Input();
}
void Game::Update( uint64 _frame )
{
	for ( uint32 i = 0u; i < m_objects.GetSize(); ++i )
		m_objects[ i ]->Update( _frame );
	if ( !( dynamic_cast< Player* >( m_objects[ 0u ] )->IsAlive() ) )
		m_gameRunning = false;
}
void Game::Render( void ) const
{
	if ( m_gameRunning )
		for ( uint32 i = 0u; i < m_objects.GetSize(); ++i )
			m_objects[ i ]->Render();
}
bool Game::IsRunning( void )
{
	return m_gameRunning;
}
void Game::Shutdown( void )
{
	m_gameRunning = false;
	for ( uint32 i = 0u; i < m_objects.GetSize(); ++i )
	{
		m_objects[ i ]->Shutdown();
		delete m_objects[ i ];
	}
	m_objects.Clear();
}

int GameMain( int, char** )
{
	Console::FlushKeys();
	Console::SetColors( ConsoleColor::Black, ConsoleColor::White );
	Console::Clear();
	Console::EOLWrap( false );
	Console::CursorVisible( false );
	const int32 width_ = Console::WindowWidth();
	const int32 height_ = Console::WindowHeight();
	Console::SetBufferSize( ( uint32 )width_, ( uint32 )height_ );
	Console::SetCursorPosition( ( SHORT )( ( width_ >> 1u ) - 5 ), ( SHORT )( ( height_ >> 1u ) - 1 ) );
	std::cout << "X A N D E R";
	Console::SetCursorPosition( ( SHORT )( ( width_ >> 1 ) - 3 ), ( SHORT )( height_ >> 1 ) );
	std::cout << "K Y L E";
	Console::SetCursorPosition( ( SHORT )( ( width_ >> 1 ) - 6 ), ( SHORT )( ( height_ >> 1 ) + 1 ) );
	std::cout << "P U C K E T T";
	Console::FlushKeys();
	Console::SetCursorPosition( 0i16, 0i16 );
	Console::ResetColor();
	Console::EOLWrap( true );
	Console::CursorVisible( true );
	Console::FlushKeys();
	system( "pause" );
	Console::Clear();
	Console::FlushKeys();
	uint64 frame = 0ull;
	Game game;
	game.Init();
	while ( game.IsRunning() )
	{
		game.Input();
		game.Update( frame );
		game.Render();
		++frame;
	}
	game.Shutdown();
	return EXIT_SUCCESS;
}