#pragma once

template <typename T>
class List
{
private:
	struct Node
	{
		typename List<T>::Node* m_prev;
		typename List<T>::Node* m_curr;
		T m_data;
		Node( void );
		Node( const T&, typename List<T>::Node* = nullptr, typename List<T>::Node* = nullptr );
		Node( T&&, typename List<T>::Node* = nullptr, typename List<T>::Node* = nullptr );
		Node( const typename List<T>::Node& );
		Node( typename List<T>::Node&& );
		typename List<T>::Node& operator=( const typename List<T>::Node& );
		typename List<T>::Node& operator=( typename List<T>::Node&& );
		~Node( void );
	};
	typename List<T>::Node* m_head;
	typename List<T>::Node* m_tail;
	uint64 m_size;
public:
	struct ConstIterator;
	struct Iterator
	{
		friend class List;
	private:
		typename List<T>::Node* m_node;
		Iterator( typename List<T>::Node* );
	public:
		Iterator( void );
		Iterator( const typename List<T>::Iterator& );
		Iterator( typename List<T>::Iterator&& );
		typename List<T>::Iterator& operator=( const typename List<T>::Iterator& );
		typename List<T>::Iterator& operator=( typename List<T>::Iterator&& );
		~Iterator( void );
		typename List<T>::ConstIterator GetConstIterator( void ) const;
		typename List<T>::Iterator& Next( void );
		typename List<T>::Iterator& Prev( void );
		T& Get( void );
		const T& Get( void ) const;
		bool End( void ) const;
		bool operator!( void ) const;
		typename List<T>::Iterator& operator++( void );
		typename List<T>::Iterator operator++( int );
		typename List<T>::Iterator& operator--( void );
		typename List<T>::Iterator operator--( int );
		typename List<T>::Iterator& operator+=( uint64 );
		typename List<T>::Iterator& operator-=( uint64 );
		typename List<T>::Iterator operator+( uint64 ) const;
		typename List<T>::Iterator operator-( uint64 ) const;
		friend typename List<T>::Iterator operator+( uint64, const typename List<T>::Iterator& );
		T& operator*( void );
		const T& operator*( void ) const;
	};
	struct ConstIterator
	{
		friend class List<T>;
	private:
		const typename List<T>::Node* m_node;
		ConstIterator( const typename List<T>::Node* );
	public:
		ConstIterator( void );
		ConstIterator( const typename List<T>::ConstIterator& );
		ConstIterator( typename List<T>::ConstIterator&& );
		typename List<T>::ConstIterator& operator=( const typename List<T>::ConstIterator& );
		typename List<T>::ConstIterator& operator=( typename List<T>::ConstIterator&& );
		ConstIterator( const typename List<T>::Iterator& );
		ConstIterator( typename List<T>::Iterator&& );
		typename List<T>::ConstIterator& operator=( const typename List<T>::Iterator& );
		typename List<T>::ConstIterator& operator=( typename List<T>::Iterator&& );
		~ConstIterator( void );
		typename List<T>::ConstIterator& Next( void );
		typename List<T>::ConstIterator& Prev( void );
		const T& Get( void ) const;
		bool End( void ) const;
		bool operator!( void ) const;
		typename List<T>::ConstIterator& operator++( void );
		typename List<T>::ConstIterator operator++( int );
		typename List<T>::ConstIterator& operator--( void );
		typename List<T>::ConstIterator operator--( int );
		typename List<T>::ConstIterator& operator+=( uint64 );
		typename List<T>::ConstIterator& operator-=( uint64 );
		typename List<T>::ConstIterator operator+( uint64 ) const;
		typename List<T>::ConstIterator operator-( uint64 ) const;
		friend typename List<T>::ConstIterator operator+( uint64, const typename List<T>::ConstIterator& );
		const T& operator*( void ) const;
	};
	List( void );
	List( const List<T>& );
	List( List<T>&& );
	List<T>& operator=( const List<T>& );
	List<T>& operator=( List<T>&& );
	~List( void );
	typename List<T>::Iterator Front( void );
	typename List<T>::ConstIterator Front( void ) const;
	typename List<T>::Iterator Back( void );
	typename List<T>::ConstIterator Back( void ) const;
	List<T>& PushFront( const T& );
	List<T>& PushBack( const T& );
	List<T>& PushFront( T&& );
	List<T>& PushBack( T&& );
	List<T>& PopFront( void );
	List<T>& PopBack( void );
	List<T>& PopFront( uint64 );
	List<T>& PopBack( uint64 );
	T& GetFront( void );
	const T& GetFront( void ) const;
	T& GetBack( void );
	const T& GetBack( void ) const;
	List<T>& Clear( void );
	typename List<T>::Iterator& Insert( typename List<T>::Iterator&, const T& );
	typename List<T>::Iterator& Insert( typename List<T>::Iterator&, T&& );
	typename List<T>::Iterator& Remove( typename List<T>::Iterator& );
	T& operator[]( uint64 );
	const T& operator[]( uint64 ) const;
	List<T>& operator--( void );
	List<T> operator--( int );
	List<T>& operator-=( uint64 );
	List<T>& operator+=( const T& );
	List<T>& operator+=( T&& );
	List<T>& operator+=( const List<T>& );
	List<T>& operator+=( List<T>&& );
	List<T>& operator*=( uint64 );
	List<T> operator-( uint64 ) const;
	List<T> operator+( const List<T>& ) const;
	List<T> operator+( List<T>&& ) const;
	List<T> operator*( uint64 ) const;
	friend List<T> operator*( uint64, const List<T>& );
	typename List<T>::Iterator operator*( void );
	typename List<T>::ConstIterator operator*( void ) const;
};

template <typename T>
List<T>::Node::Node( void ) :
	m_prev( nullptr ),
	m_next( nullptr )
{
}
template <typename T>
List<T>::Node::Node( const T& _data, typename List<T>::Node* _prev, typename List<T>::Node* _next ) :
	m_data( _data ),
	m_prev( _prev ),
	m_next( _next )
{
}
template <typename T>
List<T>::Node::Node( T&& _data, typename List<T>::Node* _prev, typename List<T>::Node* _next ) :
	m_data( std::move( _data ) ),
	m_prev( _prev ),
	m_next( _next )
{
}
template <typename T>
List<T>::Node::Node( const typename List<T>::Node& _rhs ) :
	m_data( _rhs.m_data ),
	m_next( _rhs.m_next ),
	m_prev( _rhs.m_prev )
{
}
template <typename T>
List<T>::Node::Node( typename List<T>::Node&& _rhs ) :
	m_data( std::move( _rhs.m_data ) ),
	m_next( std::move( _rhs.m_next ) ),
	m_prev( std::move( _rhs.m_prev ) )
{
	_rhs.m_next = nullptr;
	_rhs.m_prev = nullptr;
}
template <typename T>
typename List<T>::Node& List<T>::Node::operator=( const typename List<T>::Node& _rhs )
{
	if ( this != &_rhs )
	{
		m_data = _rhs.m_data;
		m_prev = _rhs.m_prev;
		m_next = _rhs.m_next;
	}
	return *this;
}
template <typename T>
typename List<T>::Node& List<T>::Node::operator=( typename List<T>::Node&& _rhs )
{
	if ( this != &_rhs )
	{
		m_data = std::move( _rhs.m_data );
		m_prev = std::move( _rhs.m_prev );
		m_next = std::move( _rhs.m_next );
		_rhs.m_next = nullptr;
		_rhs.m_prev = nullptr;
	}
	return *this;
}
template <typename T>
List<T>::Node::~Node( void )
{
}
template <typename T>
List<T>::Iterator::Iterator( typename List<T>::Node* _node ) :
	m_node( _node )
{
}
template <typename T>
List<T>::Iterator::Iterator( void ) :
	m_node( nullptr )
{
}
template <typename T>
List<T>::Iterator::Iterator( const typename List<T>::Iterator& _rhs ) :
	m_node( _rhs.m_node )
{
}
template <typename T>
List<T>::Iterator::Iterator( typename List<T>::Iterator&& _rhs ) :
	m_node( std::move( _rhs.m_node ) )
{
	_rhs.m_node = nullptr;
}
template <typename T>
typename List<T>::Iterator& List<T>::Iterator::operator=( const typename List<T>::Iterator& _rhs )
{
	if ( this != &_rhs )
	{
		m_node = _rhs.m_node;
	}
	return *this;
}
template <typename T>
typename List<T>::Iterator& List<T>::Iterator::operator=( typename List<T>::Iterator&& _rhs )
{
	if ( this != &_rhs )
	{
		m_node = std::move( _rhs.m_node );
		_rhs.m_node = nullptr;
	}
	return *this;
}
template <typename T>
List<T>::Iterator::~Iterator( void )
{
}
template <typename T>
typename List<T>::ConstIterator List<T>::Iterator::GetConstIterator( void ) const
{
	return ConstIterator( m_node );
}
template <typename T>
typename List<T>::Iterator& List<T>::Iterator::Next( void )
{
	m_node = m_node->m_next;
	return *this;
}
template <typename T>
typename List<T>::Iterator& List<T>::Iterator::Prev( void )
{
	m_node = m_node->m_prev;
	return *this;
}
template <typename T>
T& List<T>::Iterator::Get( void )
{
	return m_node->m_data;
}
template <typename T>
const T& List<T>::Iterator::Get( void ) const
{
	return m_node->m_data;
}
template <typename T>
bool List<T>::Iterator::End( void ) const
{
	return nullptr == m_node;
}
template <typename T>
bool List<T>::Iterator::operator!( void ) const
{
	return End();
}
template <typename T>
typename List<T>::Iterator& List<T>::Iterator::operator++( void )
{
	return Next();
}
template <typename T>
typename List<T>::Iterator List<T>::Iterator::operator++( int )
{
	typename List<T>::Iterator ret_ = *this;
	Next();
	return ret_;
}
template <typename T>
typename List<T>::Iterator& List<T>::Iterator::operator--( void )
{
	return Prev();
}
template <typename T>
typename List<T>::Iterator List<T>::Iterator::operator--( int )
{
	typename List<T>::Iterator ret_ = *this;
	Prev();
	return ret_;
}
template <typename T>
typename List<T>::Iterator& List<T>::Iterator::operator+=( uint64 _rhs )
{
	for ( uint64 i = 0ull; i < _rhs; ++i )
		Next();
	return *this;
}
template <typename T>
typename List<T>::Iterator& List<T>::Iterator::operator-=( uint64 _rhs )
{
	for ( uint64 i = 0ull; i < _rhs; ++i )
		Prev();
	return *this;
}
template <typename T>
typename List<T>::Iterator List<T>::Iterator::operator+( uint64 _rhs ) const
{
	typename List<T>::Iterator ret_ = *this;
	ret_ += _rhs;
	return ret_;
}
template <typename T>
typename List<T>::Iterator List<T>::Iterator::operator-( uint64 _rhs ) const
{
	typename List<T>::Iterator ret_ = *this;
	ret_ -= _rhs;
	return ret_;
}
template <typename T>
typename List<T>::Iterator operator+( uint64 _lhs, const typename List<T>::Iterator& _rhs )
{
	typename List<T>::Iterator ret_ = _rhs;
	ret_ += _lhs;
	return ret_;
}
template <typename T>
T& List<T>::Iterator::operator*( void )
{
	return m_node->m_data;
}
template <typename T>
const T& List<T>::Iterator::operator*( void ) const
{
	return m_node->m_data;
}
template <typename T>
List<T>::ConstIterator::ConstIterator( const typename List<T>::Node* _node ) :
	m_node( _node )
{
}
template <typename T>
List<T>::ConstIterator::ConstIterator( void ) :
	m_node( nullptr )
{
}
template <typename T>
List<T>::ConstIterator::ConstIterator( const typename List<T>::ConstIterator& _rhs ) :
	m_node( _rhs.m_node )
{
}
template <typename T>
List<T>::ConstIterator::ConstIterator( typename List<T>::ConstIterator&& _rhs ) :
	m_node( std::move( _rhs.m_node ) )
{
	_rhs.m_node = nullptr;
}
template <typename T>
typename List<T>::ConstIterator& List<T>::ConstIterator::operator=( const typename List<T>::ConstIterator& _rhs )
{
	if ( this != &_rhs )
	{
		m_node = _rhs.m_node;
	}
	return *this;
}
template <typename T>
typename List<T>::ConstIterator& List<T>::ConstIterator::operator=( typename List<T>::ConstIterator&& _rhs )
{
	if ( this != &_rhs )
	{
		m_node = std::move( _rhs.m_node );
		_rhs.m_node = nullptr;
	}
	return *this;
}
template <typename T>
List<T>::ConstIterator::ConstIterator( const typename List<T>::Iterator& _rhs )
{
	// TODO
}
template <typename T>
List<T>::ConstIterator::ConstIterator( typename List<T>::Iterator&& _rhs )
{
	// TODO
}
template <typename T>
typename List<T>::ConstIterator& List<T>::ConstIterator::operator=( const typename List<T>::Iterator& _rhs )
{
	// TODO
}
template <typename T>
typename List<T>::ConstIterator& List<T>::ConstIterator::operator=( typename List<T>::Iterator&& _rhs )
{
	// TODO
}
template <typename T>
List<T>::ConstIterator::~ConstIterator( void )
{
	// TODO
}
template <typename T>
typename List<T>::ConstIterator& List<T>::ConstIterator::Next( void )
{
	// TODO
}
template <typename T>
typename List<T>::ConstIterator& List<T>::ConstIterator::Prev( void )
{
	// TODO
}
template <typename T>
const T& List<T>::ConstIterator::Get( void ) const
{
	// TODO
}
template <typename T>
bool List<T>::ConstIterator::End( void ) const
{
	// TODO
}
template <typename T>
bool List<T>::ConstIterator::operator!( void ) const
{
	// TODO
}
template <typename T>
typename List<T>::ConstIterator& List<T>::ConstIterator::operator++( void )
{
	// TODO
}
template <typename T>
typename List<T>::ConstIterator List<T>::ConstIterator::operator++( int )
{
	// TODO
}
template <typename T>
typename List<T>::ConstIterator& List<T>::ConstIterator::operator--( void )
{
	// TODO
}
template <typename T>
typename List<T>::ConstIterator List<T>::ConstIterator::operator--( int )
{
	// TODO
}
template <typename T>
typename List<T>::ConstIterator& List<T>::ConstIterator::operator+=( uint64 _rhs )
{
	// TODO
}
template <typename T>
typename List<T>::ConstIterator& List<T>::ConstIterator::operator-=( uint64 _rhs )
{
	// TODO
}
template <typename T>
typename List<T>::ConstIterator List<T>::ConstIterator::operator+( uint64 _rhs ) const
{
	// TODO
}
template <typename T>
typename List<T>::ConstIterator List<T>::ConstIterator::operator-( uint64 _rhs ) const
{
	// TODO
}
template <typename T>
typename List<T>::ConstIterator operator+( uint64 _lhs, const typename List<T>::ConstIterator& _rhs )
{
	// TODO
}
template <typename T>
const T& List<T>::ConstIterator::operator*( void ) const
{
	// TODO
}
template <typename T>
List<T>::List( void )
{
	// TODO
}
template <typename T>
List<T>::List( const List<T>& _rhs )
{
	// TODO
}
template <typename T>
List<T>::List( List<T>&& _rhs )
{
	// TODO
}
template <typename T>
List<T>& List<T>::operator=( const List<T>& _rhs )
{
	// TODO
}
template <typename T>
List<T>& List<T>::operator=( List<T>&& _rhs )
{
	// TODO
}
template <typename T>
List<T>::~List( void )
{
	// TODO
}
template <typename T>
typename List<T>::Iterator List<T>::Front( void )
{
	// TODO
}
template <typename T>
typename List<T>::ConstIterator List<T>::Front( void ) const
{
	// TODO
}
template <typename T>
typename List<T>::Iterator List<T>::Back( void )
{
	// TODO
}
template <typename T>
typename List<T>::ConstIterator List<T>::Back( void ) const
{
	// TODO
}
template <typename T>
List<T>& List<T>::PushFront( const T& _data )
{
	// TODO
}
template <typename T>
List<T>& List<T>::PushBack( const T& _data )
{
	// TODO
}
template <typename T>
List<T>& List<T>::PushFront( T&& _data )
{
	// TODO
}
template <typename T>
List<T>& List<T>::PushBack( T&& _data )
{
	// TODO
}
template <typename T>
List<T>& List<T>::PopFront( void )
{
	// TODO
}
template <typename T>
List<T>& List<T>::PopBack( void )
{
	// TODO
}
template <typename T>
List<T>& List<T>::PopFront( uint64 _count )
{
	// TODO
}
template <typename T>
List<T>& List<T>::PopBack( uint64 _count )
{
	// TODO
}
template <typename T>
T& List<T>::GetFront( void )
{
	// TODO
}
template <typename T>
const T& List<T>::GetFront( void ) const
{
	// TODO
}
template <typename T>
T& List<T>::GetBack( void )
{
	// TODO
}
template <typename T>
const T& List<T>::GetBack( void ) const
{
	// TODO
}
template <typename T>
List<T>& List<T>::Clear( void )
{
	// TODO
}
template <typename T>
typename List<T>::Iterator& List<T>::Insert( typename List<T>::Iterator& _iter, const T& _data )
{
	// TODO
}
template <typename T>
typename List<T>::Iterator& List<T>::Insert( typename List<T>::Iterator& _iter, T&& _data )
{
	// TODO
}
template <typename T>
typename List<T>::Iterator& List<T>::Remove( typename List<T>::Iterator& _iter )
{
	// TODO
}
template <typename T>
T& List<T>::operator[]( uint64 _idx )
{
	// TODO
}
template <typename T>
const T& List<T>::operator[]( uint64 _idx ) const
{
	// TODO
}
template <typename T>
List<T>& List<T>::operator--( void )
{
	// TODO
}
template <typename T>
List<T> List<T>::operator--( int )
{
	// TODO
}
template <typename T>
List<T>& List<T>::operator-=( uint64 _rhs )
{
	// TODO
}
template <typename T>
List<T>& List<T>::operator+=( const T& _rhs )
{
	// TODO
}
template <typename T>
List<T>& List<T>::operator+=( T&& _rhs )
{
	// TODO
}
template <typename T>
List<T>& List<T>::operator+=( const List<T>& _rhs )
{
	// TODO
}
template <typename T>
List<T>& List<T>::operator+=( List<T>&& _rhs )
{
	// TODO
}
template <typename T>
List<T>& List<T>::operator*=( uint64 _rhs )
{
	// TODO
}
template <typename T>
List<T> List<T>::operator-( uint64 _rhs ) const
{
	// TODO
}
template <typename T>
List<T> List<T>::operator+( const List<T>& _rhs ) const
{
	// TODO
}
template <typename T>
List<T> List<T>::operator+( List<T>&& _rhs ) const
{
	// TODO
}
template <typename T>
List<T> List<T>::operator*( uint64 _rhs ) const
{
	// TODO
}
template <typename T>
List<T> operator*( uint64 _lhs, const List<T>& _rhs )
{
	// TODO
}
template <typename T>
typename List<T>::Iterator List<T>::operator*( void )
{
	// TODO
}
template <typename T>
typename List<T>::ConstIterator List<T>::operator*( void ) const
{
	// TODO
}