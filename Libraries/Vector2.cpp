#include "stdafx.h"
#include "Vector2.h"

const Vector2 Vector2::zero = Vector2( 0.0, 0.0 );
const Vector2 Vector2::one = Vector2( 1.0, 1.0 );
const Vector2 Vector2::x_axis = Vector2( 1.0, 0.0 );
const Vector2 Vector2::y_axis = Vector2( 0.0, 1.0 );

Vector2::Vector2( void )
{
}
Vector2::Vector2( float64 _x, float64 _y ) :
	x( _x ), y( _y )
{
}
Vector2::Vector2( const Vector2& _rhs ) :
	x( _rhs.x ), y( _rhs.y )
{
}
Vector2& Vector2::operator=( const Vector2& _rhs )
{
	if ( this != &_rhs )
	{
		x = _rhs.x;
		y = _rhs.y;
	}
	return *this;
}
Vector2::Vector2( Vector2&& _rhs ) :
	x( std::move( _rhs.x ) ),
	y( std::move( _rhs.y ) )
{
}
Vector2& Vector2::operator=( Vector2&& _rhs )
{
	if ( this != &_rhs )
	{
		x = std::move( _rhs.x );
		y = std::move( _rhs.y );
	}
	return *this;
}
Vector2::~Vector2( void )
{
}
float64 Vector2::dot( const Vector2& lhs, const Vector2& rhs )
{
	return lhs.x * rhs.x + lhs.y * rhs.y;
}
float64 Vector2::magnitude( void ) const
{
	return sqrt( x * x + y * y );
}
float64 Vector2::sqmagnitude( void ) const
{
	return x * x + y * y;
}
Vector2& Vector2::normalize( void )
{
	return *this *= 1.0 / sqrt( x * x + y * y );
}
Vector2 Vector2::direction( void ) const
{
	return *this * ( 1.0 / sqrt( x * x + y * y ) );
}
float64 Vector2::dot( const Vector2& rhs ) const
{
	return x * rhs.x + y * rhs.y;
}
Vector2& Vector2::negate( void )
{
	return *this = Vector2( -x, -y );
}
Vector2 Vector2::operator+( void ) const
{
	return *this;
}
Vector2 Vector2::operator-( void ) const
{
	return Vector2( -x, -y );
}
Vector2 Vector2::operator+( const Vector2& rhs ) const
{
	return Vector2( x + rhs.x, y + rhs.y );
}
Vector2 Vector2::operator-( const Vector2& rhs ) const
{
	return Vector2( x - rhs.x, y - rhs.y );
}
float64 Vector2::operator*( const Vector2& rhs ) const
{
	return x * rhs.x + y * rhs.y;
}
Vector2 Vector2::operator*( float64 rhs ) const
{
	return Vector2( x * rhs, y * rhs );
}
Vector2 Vector2::operator/( float64 rhs ) const
{
	rhs = 1.0 / rhs;
	return Vector2( x * rhs, y * rhs );
}
Vector2& Vector2::operator+=( const Vector2& rhs )
{
	x += rhs.x;
	y += rhs.y;
	return *this;
}
Vector2& Vector2::operator-=( const Vector2& rhs )
{
	x -= rhs.x;
	y -= rhs.y;
	return *this;
}
Vector2& Vector2::operator*=( float64 rhs )
{
	x *= rhs;
	y *= rhs;
	return *this;
}
Vector2& Vector2::operator/=( float64 rhs )
{
	rhs = 1.0 / rhs;
	x *= rhs;
	y *= rhs;
	return *this;
}
bool Vector2::operator==( const Vector2& rhs ) const
{
	return x == rhs.x && y == rhs.y;
}
bool Vector2::operator!=( const Vector2& rhs ) const
{
	return x != rhs.x || y != rhs.y;
}
Vector2 operator*( float64 lhs, const Vector2& rhs )
{
	return Vector2( lhs * rhs.x, lhs * rhs.y );
}
std::ostream& operator<<( std::ostream& os, const Vector2& rhs )
{
	return os << '(' << rhs.x << ',' << rhs.y << ')';
}
float64 Vector2::SqDistance( const Vector2& a, const Vector2& b )
{
	const float64 dx = b.x - a.x;
	const float64 dy = b.y - a.y;
	return dx * dx + dy * dy;
}
float64 Vector2::Distance( const Vector2& a, const Vector2& b )
{
	const float64 dx = b.x - a.x;
	const float64 dy = b.y - a.y;
	return sqrt( dx * dx + dy * dy );
}