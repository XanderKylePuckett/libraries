#pragma once

class Input
{
public:
	static void Update( void );
	static bool KeyPress( uint8 );
	static bool KeyRelease( uint8 );
	static bool KeyDown( uint8 );
	static bool KeyUp( uint8 );
};