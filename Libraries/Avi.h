#pragma once
#include <vfw.h>

class Avi
{
private:
	IAVIFile* m_pfile;
	IAVIStream* m_ps;
	IAVIStream* m_psCompressed;
	unsigned long m_nframe;
	bool m_iserr;
public:
	Avi( const std::string& );
	~Avi( void );
	bool AddFrame( const std::string& );
};