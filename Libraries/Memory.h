#pragma once

class Memory
{
public:
	static void Copy( const void* _src, void* _dst, uint32 );
	static void Zeros( void*, uint32 );
	static void Ones( void*, uint32 );
	static bool IsEqual( const void*, const void*, uint32 );
	static bool IsZero( const void*, uint32 );
};