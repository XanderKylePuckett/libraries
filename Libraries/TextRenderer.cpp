#include "stdafx.h"
#include "TextRenderer.h"
#include "Math.h"
#include <vector>

TextRenderer::TextRenderer( void )
{
}
TextRenderer::TextRenderer( const AsciiFont& _font ) :
	m_font( _font )
{
}
TextRenderer::TextRenderer( const TextRenderer& _rhs ) :
	m_font( _rhs.m_font )
{
}
TextRenderer::TextRenderer( TextRenderer&& _rhs ) :
	m_font( std::move( _rhs.m_font ) )
{
}
TextRenderer& TextRenderer::operator=( const TextRenderer& _rhs )
{
	if ( this != &_rhs )
	{
		m_font = _rhs.m_font;
	}
	return *this;
}
TextRenderer& TextRenderer::operator=( TextRenderer&& _rhs )
{
	if ( this != &_rhs )
	{
		m_font = std::move( _rhs.m_font );
	}
	return *this;
}
TextRenderer::~TextRenderer( void )
{
}
const AsciiFont& TextRenderer::GetFont( void ) const
{
	return m_font;
}
void TextRenderer::SetFont( const AsciiFont& _font )
{
	m_font = _font;
}
std::vector<std::string> SeparateString( const std::string& _inStr, uint32 _maxLen )
{
	std::vector<std::string> outStrs_;
	// TODO
#pragma region TheWrongWay
	std::string tmp_ = "";
	uint32 i = 0u;
	while ( i < ( uint32 )_inStr.length() )
	{
		tmp_ += _inStr[ i++ ];
		if ( 0u == i % _maxLen )
		{
			outStrs_.push_back( tmp_ );
			tmp_ = "";
		}
	}
	outStrs_.push_back( tmp_ );
#pragma endregion
	return outStrs_;
}
Texture2D TextRenderer::MakeTextBlock( const std::string& _text, uint32 _textureWidth, uint32 _textureHeight ) const
{
	Texture2D outBlock_( _textureWidth, _textureHeight, Color::ZERO );
	if ( 0u == m_font.GetCharacterWidth() || 0u == outBlock_.GetWidth() )
		return outBlock_;
	std::vector<std::string> strs_ = SeparateString( _text, _textureWidth / m_font.GetCharacterWidth() );
	const uint32 numLines_ = Math::Min( ( uint32 )strs_.size(), _textureHeight / m_font.GetCharacterHeight() + 1u );
	const uint32 charWidth_ = m_font.GetCharacterWidth();
	const uint32 charHeight_ = m_font.GetCharacterHeight();
	uint32 i, strLen_, j;
	for ( i = 0u; i < numLines_; ++i )
	{
		const std::string& str_ = strs_[ i ];
		strLen_ = ( uint32 )str_.length();
		for ( j = 0u; j < strLen_; ++j )
			Texture2D::TextureToTexture( m_font.GetCharacter( str_[ j ] ), outBlock_, j * charWidth_, i * charHeight_ );
	}
	return outBlock_;
}