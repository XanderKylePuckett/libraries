#include "stdafx.h"
#include "Matrix3x3.h"

const Matrix3x3 Matrix3x3::IDENTITY = Matrix3x3( Vector3::X_AXIS,
												 Vector3::Y_AXIS,
												 Vector3::Z_AXIS );
const Matrix3x3 Matrix3x3::ZERO = Matrix3x3( Vector3::ZERO,
											 Vector3::ZERO,
											 Vector3::ZERO );
const Matrix3x3 Matrix3x3::ONE = Matrix3x3( Vector3::ONE,
											Vector3::ONE,
											Vector3::ONE );
Matrix3x3::Matrix3x3( void ) :
	m_row1( Vector3::X_AXIS ),
	m_row2( Vector3::Y_AXIS ),
	m_row3( Vector3::Z_AXIS )
{
}
Matrix3x3::Matrix3x3( float64 _11, float64 _12, float64 _13,
					  float64 _21, float64 _22, float64 _23,
					  float64 _31, float64 _32, float64 _33 ) :
	e11( _11 ), e12( _12 ), e13( _13 ),
	e21( _21 ), e22( _22 ), e23( _23 ),
	e31( _31 ), e32( _32 ), e33( _33 )
{
}
Matrix3x3::Matrix3x3( const float64* _arr )
{
	for ( uint8 i = 0ui8; i < 9ui8; ++i )
		m_arr[ i ] = _arr[ i ];
}
Matrix3x3::Matrix3x3( float32 _11, float32 _12, float32 _13,
					  float32 _21, float32 _22, float32 _23,
					  float32 _31, float32 _32, float32 _33 ) :
	e11( ( float64 )_11 ), e12( ( float64 )_12 ), e13( ( float64 )_13 ),
	e21( ( float64 )_21 ), e22( ( float64 )_22 ), e23( ( float64 )_23 ),
	e31( ( float64 )_31 ), e32( ( float64 )_32 ), e33( ( float64 )_33 )
{
}
Matrix3x3::Matrix3x3( const float32* _arr )
{
	for ( uint8 i = 0ui8; i < 9ui8; ++i )
		m_arr[ i ] = ( float64 )( _arr[ i ] );
}
Matrix3x3::Matrix3x3( const Vector3* _rows ) :
	m_row1( _rows[ 0 ] ),
	m_row2( _rows[ 1 ] ),
	m_row3( _rows[ 2 ] )
{
}
Matrix3x3::Matrix3x3( const Vector3& r1,
					  const Vector3& r2,
					  const Vector3& r3 ) :
	m_row1( r1 ), m_row2( r2 ), m_row3( r3 )
{
}
Matrix3x3::Matrix3x3( const Matrix3x3& rhs ) :
	m_row1( rhs.m_row1 ),
	m_row2( rhs.m_row2 ),
	m_row3( rhs.m_row3 )
{
}
Matrix3x3::Matrix3x3( Matrix3x3&& rhs ) :
	m_row1( std::move( rhs.m_row1 ) ),
	m_row2( std::move( rhs.m_row2 ) ),
	m_row3( std::move( rhs.m_row3 ) )
{
}
Matrix3x3::Matrix3x3( const Matrix& rhs )
{
	e11 = rhs.GetElement( 0u, 0u );
	e12 = rhs.GetElement( 0u, 1u );
	e13 = rhs.GetElement( 0u, 2u );
	e21 = rhs.GetElement( 1u, 0u );
	e22 = rhs.GetElement( 1u, 1u );
	e23 = rhs.GetElement( 1u, 2u );
	e31 = rhs.GetElement( 2u, 0u );
	e32 = rhs.GetElement( 2u, 1u );
	e33 = rhs.GetElement( 2u, 2u );
}
Matrix3x3& Matrix3x3::operator=( const float64* rhs )
{
	for ( uint8 i = 0ui8; i < 9ui8; ++i )
		m_arr[ i ] = rhs[ i ];
	return *this;
}
Matrix3x3& Matrix3x3::operator=( const float32* rhs )
{
	for ( uint8 i = 0ui8; i < 9ui8; ++i )
		m_arr[ i ] = ( float64 )( rhs[ i ] );
	return *this;
}
Matrix3x3& Matrix3x3::operator=( const Vector3* rhs )
{
	m_row1 = rhs[ 0 ];
	m_row2 = rhs[ 1 ];
	m_row3 = rhs[ 2 ];
	return *this;
}
Matrix3x3& Matrix3x3::operator=( const Matrix3x3& rhs )
{
	if ( this != &rhs )
	{
		m_row1 = rhs.m_row1;
		m_row2 = rhs.m_row2;
		m_row3 = rhs.m_row3;
	}
	return *this;
}
Matrix3x3& Matrix3x3::operator=( const Matrix& rhs )
{
	e11 = rhs.GetElement( 0u, 0u );
	e12 = rhs.GetElement( 0u, 1u );
	e13 = rhs.GetElement( 0u, 2u );
	e21 = rhs.GetElement( 1u, 0u );
	e22 = rhs.GetElement( 1u, 1u );
	e23 = rhs.GetElement( 1u, 2u );
	e31 = rhs.GetElement( 2u, 0u );
	e32 = rhs.GetElement( 2u, 1u );
	e33 = rhs.GetElement( 2u, 2u );
	return *this;
}
Matrix3x3& Matrix3x3::operator=( Matrix3x3&& rhs )
{
	if ( this != &rhs )
	{
		m_row1 = std::move( rhs.m_row1 );
		m_row2 = std::move( rhs.m_row2 );
		m_row3 = std::move( rhs.m_row3 );
	}
	return *this;
}
Matrix3x3::~Matrix3x3( void )
{
}
Matrix3x3 Matrix3x3::operator+( void ) const
{
	return *this;
}
Matrix3x3 Matrix3x3::operator-( void ) const
{
	return Matrix3x3( -m_row1, -m_row2, -m_row3 );
}
Matrix3x3 Matrix3x3::operator+( const Matrix3x3& rhs ) const
{
	return Matrix3x3( m_row1 + rhs.m_row1,
					  m_row2 + rhs.m_row2,
					  m_row3 + rhs.m_row3 );
}
Matrix3x3 Matrix3x3::operator-( const Matrix3x3& rhs ) const
{
	return Matrix3x3( m_row1 - rhs.m_row1,
					  m_row2 - rhs.m_row2,
					  m_row3 - rhs.m_row3 );
}
Matrix3x3 Matrix3x3::operator*( const Matrix3x3& rhs ) const
{
	return Matrix3x3
	(
		e11 * rhs.e11 + e12 * rhs.e21 + e13 * rhs.e31,
		e11 * rhs.e12 + e12 * rhs.e22 + e13 * rhs.e32,
		e11 * rhs.e13 + e12 * rhs.e23 + e13 * rhs.e33,
		e21 * rhs.e11 + e22 * rhs.e21 + e23 * rhs.e31,
		e21 * rhs.e12 + e22 * rhs.e22 + e23 * rhs.e32,
		e21 * rhs.e13 + e22 * rhs.e23 + e23 * rhs.e33,
		e31 * rhs.e11 + e32 * rhs.e21 + e33 * rhs.e31,
		e31 * rhs.e12 + e32 * rhs.e22 + e33 * rhs.e32,
		e31 * rhs.e13 + e32 * rhs.e23 + e33 * rhs.e33
	);
}
Matrix3x3 Matrix3x3::operator*( float64 rhs ) const
{
	return Matrix3x3( m_row1 * rhs,
					  m_row2 * rhs,
					  m_row3 * rhs );
}
Matrix3x3 Matrix3x3::operator/( float64 rhs ) const
{
	rhs = 1.0 / rhs;
	return Matrix3x3( m_row1 * rhs, m_row2 * rhs, m_row3 * rhs );
}
Matrix3x3& Matrix3x3::operator+=( const Matrix3x3& rhs )
{
	m_row1 += rhs.m_row1;
	m_row2 += rhs.m_row2;
	m_row3 += rhs.m_row3;
	return *this;
}
Matrix3x3& Matrix3x3::operator-=( const Matrix3x3& rhs )
{
	m_row1 -= rhs.m_row1;
	m_row2 -= rhs.m_row2;
	m_row3 -= rhs.m_row3;
	return *this;
}
Matrix3x3& Matrix3x3::operator*=( const Matrix3x3& rhs )
{
	Matrix3x3& lhs = *this;
	return lhs = lhs * rhs;
}
Matrix3x3& Matrix3x3::operator*=( float64 rhs )
{
	m_row1 *= rhs;
	m_row2 *= rhs;
	m_row3 *= rhs;
	return *this;
}
Matrix3x3& Matrix3x3::operator/=( float64 rhs )
{
	rhs = 1.0 / rhs;
	m_row1 *= rhs;
	m_row2 *= rhs;
	m_row3 *= rhs;
	return *this;
}
bool Matrix3x3::operator==( const Matrix3x3& rhs ) const
{
	return m_row1 == rhs.m_row1 && m_row2 == rhs.m_row2 && m_row3 == rhs.m_row3;
}
bool Matrix3x3::operator!=( const Matrix3x3& rhs ) const
{
	return m_row1 != rhs.m_row1 || m_row2 != rhs.m_row2 || m_row3 != rhs.m_row3;
}
Matrix3x3 operator*( float64 lhs, const Matrix3x3& rhs )
{
	return Matrix3x3( rhs.m_row1 * lhs, rhs.m_row2 * lhs, rhs.m_row3 * lhs );
}
Matrix3x3 Matrix3x3::getInverse( void ) const
{
	return getAdjugateMatrix() * ( 1.0 / getDeterminant() );
}
Matrix3x3 Matrix3x3::getInverse( const Matrix3x3& mat )
{
	return mat.getInverse();
}
Matrix3x3& Matrix3x3::Invert( void )
{
	return *this = getInverse();
}
Matrix3x3& Matrix3x3::Invert( Matrix3x3& mat )
{
	return mat = mat.getInverse();
}
float64 getDeterminant( float64 e11, float64 e12, float64 e21, float64 e22 )
{
	return e11 * e22 - e12 * e21;
}
float64 Matrix3x3::getDeterminant( void ) const
{
	return
		( e11 * ::getDeterminant( e22, e23, e32, e33 ) ) -
		( e12 * ::getDeterminant( e21, e23, e31, e33 ) ) +
		( e13 * ::getDeterminant( e21, e22, e31, e32 ) );
}
float64 Matrix3x3::getDeterminant( const Matrix3x3& mat )
{
	return mat.getDeterminant();
}
Matrix3x3 Matrix3x3::getTranspose( void ) const
{
	return Matrix3x3( e11, e21, e31, e12, e22, e32, e13, e23, e33 );
}
Matrix3x3 Matrix3x3::getTranspose( const Matrix3x3& mat )
{
	return mat.getTranspose();
}
Matrix3x3& Matrix3x3::Transpose( void )
{
	return *this = getTranspose();
}
Matrix3x3& Matrix3x3::Transpose( Matrix3x3& mat )
{
	return mat = mat.getTranspose();
}
Matrix3x3 Matrix3x3::getMinorMatrix( void ) const
{
	return Matrix3x3
	(
		::getDeterminant( e22, e23, e32, e33 ),
		::getDeterminant( e21, e23, e31, e33 ),
		::getDeterminant( e21, e22, e31, e32 ),
		::getDeterminant( e12, e13, e32, e33 ),
		::getDeterminant( e11, e13, e31, e33 ),
		::getDeterminant( e11, e12, e31, e32 ),
		::getDeterminant( e12, e13, e22, e23 ),
		::getDeterminant( e11, e13, e21, e23 ),
		::getDeterminant( e11, e12, e21, e22 )
	);
}
Matrix3x3 Matrix3x3::getCofactorMatrix( void ) const
{
	Matrix3x3 outMat = getMinorMatrix();
	outMat.e12 = -outMat.e12;
	outMat.e21 = -outMat.e21;
	outMat.e23 = -outMat.e23;
	outMat.e32 = -outMat.e32;
	return outMat;
}
Matrix3x3 Matrix3x3::getAdjugateMatrix( void ) const
{
	return getCofactorMatrix().getTranspose();
}
Matrix3x3 Matrix3x3::getMinorMatrix( const Matrix3x3& mat )
{
	return mat.getMinorMatrix();
}
Matrix3x3 Matrix3x3::getCofactorMatrix( const Matrix3x3& mat )
{
	return mat.getCofactorMatrix();
}
Matrix3x3 Matrix3x3::getAdjugateMatrix( const Matrix3x3& mat )
{
	return mat.getAdjugateMatrix();
}