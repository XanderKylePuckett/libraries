#include "stdafx.h"
#include "Vector3.h"

const Vector3 Vector3::ZERO = Vector3( 0.0, 0.0, 0.0 );
const Vector3 Vector3::ONE = Vector3( 1.0, 1.0, 1.0 );
const Vector3 Vector3::X_AXIS = Vector3( 1.0, 0.0, 0.0 );
const Vector3 Vector3::Y_AXIS = Vector3( 0.0, 1.0, 0.0 );
const Vector3 Vector3::Z_AXIS = Vector3( 0.0, 0.0, 1.0 );

Vector3::Vector3( void )
{
}
Vector3::Vector3( float64 _x, float64 _y, float64 _z ) :
	x( _x ), y( _y ), z( _z )
{
}
Vector3::Vector3( const Vector3& _rhs ) :
	x( _rhs.x ), y( _rhs.y ), z( _rhs.z )
{
}
Vector3& Vector3::operator=( const Vector3& _rhs )
{
	if ( this != &_rhs )
	{
		x = _rhs.x;
		y = _rhs.y;
		z = _rhs.z;
	}
	return *this;
}
Vector3::Vector3( Vector3&& _rhs ) :
	x( std::move( _rhs.x ) ),
	y( std::move( _rhs.y ) ),
	z( std::move( _rhs.z ) )
{
}
Vector3& Vector3::operator=( Vector3&& _rhs )
{
	if ( this != &_rhs )
	{
		x = std::move( _rhs.x );
		y = std::move( _rhs.y );
		z = std::move( _rhs.z );
	}
	return *this;
}
Vector3::~Vector3( void )
{
}
float64 Vector3::dot( const Vector3& lhs, const Vector3& rhs )
{
	return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
}
Vector3 Vector3::cross( const Vector3& lhs, const Vector3& rhs )
{
	return Vector3( lhs.y * rhs.z - lhs.z * rhs.y, lhs.z * rhs.x - lhs.x * rhs.z, lhs.x * rhs.y - lhs.y * rhs.x );
}
float64 Vector3::magnitude( void ) const
{
	return sqrt( x * x + y * y + z * z );
}
float64 Vector3::sqmagnitude( void ) const
{
	return x * x + y * y + z * z;
}
Vector3& Vector3::normalize( void )
{
	return *this *= 1.0 / sqrt( x * x + y * y + z * z );
}
Vector3 Vector3::direction( void ) const
{
	return *this * ( 1.0 / sqrt( x * x + y * y + z * z ) );
}
float64 Vector3::dot( const Vector3& rhs ) const
{
	return x * rhs.x + y * rhs.y + z * rhs.z;
}
Vector3 Vector3::cross( const Vector3& rhs ) const
{
	return Vector3( y * rhs.z - z * rhs.y, z * rhs.x - x * rhs.z, x * rhs.y - y * rhs.x );
}
Vector3& Vector3::negate( void )
{
	return *this = Vector3( -x, -y, -z );
}
Vector3 Vector3::operator+( void ) const
{
	return *this;
}
Vector3 Vector3::operator-( void ) const
{
	return Vector3( -x, -y, -z );
}
Vector3 Vector3::operator+( const Vector3& rhs ) const
{
	return Vector3( x + rhs.x, y + rhs.y, z + rhs.z );
}
Vector3 Vector3::operator-( const Vector3& rhs ) const
{
	return Vector3( x - rhs.x, y - rhs.y, z - rhs.z );
}
float64 Vector3::operator*( const Vector3& rhs ) const
{
	return x * rhs.x + y * rhs.y + z * rhs.z;
}
Vector3 Vector3::operator*( float64 rhs ) const
{
	return Vector3( x * rhs, y * rhs, z * rhs );
}
Vector3 Vector3::operator/( float64 rhs ) const
{
	rhs = 1.0 / rhs;
	return Vector3( x * rhs, y * rhs, z * rhs );
}
Vector3& Vector3::operator+=( const Vector3& rhs )
{
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;
	return *this;
}
Vector3& Vector3::operator-=( const Vector3& rhs )
{
	x -= rhs.x;
	y -= rhs.y;
	z -= rhs.z;
	return *this;
}
Vector3& Vector3::operator*=( float64 rhs )
{
	x *= rhs;
	y *= rhs;
	z *= rhs;
	return *this;
}
Vector3& Vector3::operator/=( float64 rhs )
{
	rhs = 1.0 / rhs;
	x *= rhs;
	y *= rhs;
	z *= rhs;
	return *this;
}
bool Vector3::operator==( const Vector3& rhs ) const
{
	return x == rhs.x && y == rhs.y && z == rhs.z;
}
bool Vector3::operator!=( const Vector3& rhs ) const
{
	return x != rhs.x || y != rhs.y || z != rhs.z;
}
Vector3 operator*( float64 lhs, const Vector3& rhs )
{
	return Vector3( lhs * rhs.x, lhs * rhs.y, lhs * rhs.z );
}
std::ostream& operator<<( std::ostream& os, const Vector3& rhs )
{
	return os << '(' << rhs.x << ',' << rhs.y << ',' << rhs.z << ')';
}
float64 Vector3::SqDistance( const Vector3& a, const Vector3& b )
{
	float64 ans = b.x - a.x, tmp = b.y - a.y;
	ans *= ans;
	ans += tmp * tmp;
	tmp = b.z - a.z;
	return ans + tmp * tmp;
}
float64 Vector3::Distance( const Vector3& a, const Vector3& b )
{
	float64 ans = b.x - a.x, tmp = b.y - a.y;
	ans *= ans;
	ans += tmp * tmp;
	tmp = b.z - a.z;
	return sqrt( ans + tmp * tmp );
}