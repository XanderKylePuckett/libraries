#pragma once

enum BitFileMode
{
	BITFILE_CLOSED, BITFILE_IN, BITFILE_OUT
};

class BitFile
{
private:
	std::fstream m_file;
	BitFileMode m_mode;
	uint8 m_byte;
	uint8 m_bit;
public:
	BitFile( void );
	BitFile( const std::string&, BitFileMode );
	BitFile( const BitFile& ) = delete;
	BitFile( BitFile&& );
	BitFile& operator=( const BitFile& ) = delete;
	BitFile& operator=( BitFile&& );
	~BitFile( void );
	BitFile& OpenOut( const std::string& );
	BitFile& OpenIn( const std::string& );
	bool IsOpen( void ) const;
	void Close( void );
	void WriteBit( bool );
	bool ReadBit( void );
	BitFile& operator+=( bool );
	void WriteData( const char*, uint32 = 1u );
	void ReadData( char*, uint32 = 1u );
};