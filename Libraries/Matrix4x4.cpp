#include "stdafx.h"
#include "Matrix4x4.h"
#include "Math.h"

const Matrix4x4 Matrix4x4::IDENTITY = Matrix4x4( Matrix3x3::IDENTITY );
const Matrix4x4 Matrix4x4::ZERO = Matrix4x4( Vector4::ZERO,
											 Vector4::ZERO,
											 Vector4::ZERO,
											 Vector4::ZERO );
const Matrix4x4 Matrix4x4::ONE = Matrix4x4( Vector4::ONE,
											Vector4::ONE,
											Vector4::ONE,
											Vector4::ONE );
Matrix4x4::Matrix4x4( void ) :
	m_row1( Vector4( 1.0, 0.0, 0.0, 0.0 ) ),
	m_row2( Vector4( 0.0, 1.0, 0.0, 0.0 ) ),
	m_row3( Vector4( 0.0, 0.0, 1.0, 0.0 ) ),
	m_row4( Vector4( 0.0, 0.0, 0.0, 1.0 ) )
{
}
Matrix4x4::Matrix4x4( float64 _11, float64 _12, float64 _13, float64 _14,
					  float64 _21, float64 _22, float64 _23, float64 _24,
					  float64 _31, float64 _32, float64 _33, float64 _34,
					  float64 _41, float64 _42, float64 _43, float64 _44 ) :
	e11( _11 ), e12( _12 ), e13( _13 ), e14( _14 ),
	e21( _21 ), e22( _22 ), e23( _23 ), e24( _24 ),
	e31( _31 ), e32( _32 ), e33( _33 ), e34( _34 ),
	e41( _41 ), e42( _42 ), e43( _43 ), e44( _44 )
{
}
Matrix4x4::Matrix4x4( const float64* _arr )
{
	for ( uint8 i = 0ui8; i < 16ui8; ++i )
		m_arr[ i ] = _arr[ i ];
}
Matrix4x4::Matrix4x4( float32 _11, float32 _12, float32 _13, float32 _14,
					  float32 _21, float32 _22, float32 _23, float32 _24,
					  float32 _31, float32 _32, float32 _33, float32 _34,
					  float32 _41, float32 _42, float32 _43, float32 _44 ) :
	e11( ( float64 )_11 ), e12( ( float64 )_12 ), e13( ( float64 )_13 ), e14( ( float64 )_14 ),
	e21( ( float64 )_21 ), e22( ( float64 )_22 ), e23( ( float64 )_23 ), e24( ( float64 )_24 ),
	e31( ( float64 )_31 ), e32( ( float64 )_32 ), e33( ( float64 )_33 ), e34( ( float64 )_34 ),
	e41( ( float64 )_41 ), e42( ( float64 )_42 ), e43( ( float64 )_43 ), e44( ( float64 )_44 )
{
}
Matrix4x4::Matrix4x4( const float32* _arr )
{
	for ( uint8 i = 0ui8; i < 16ui8; ++i )
		m_arr[ i ] = ( float64 )( _arr[ i ] );
}
Matrix4x4::Matrix4x4( const Vector4* _rows ) :
	m_row1( _rows[ 0 ] ),
	m_row2( _rows[ 1 ] ),
	m_row3( _rows[ 2 ] ),
	m_row4( _rows[ 3 ] )
{
}
Matrix4x4::Matrix4x4( const Vector4& r1,
					  const Vector4& r2,
					  const Vector4& r3,
					  const Vector4& r4 ) :
	m_row1( r1 ), m_row2( r2 ), m_row3( r3 ), m_row4( r4 )
{
}
Matrix4x4::Matrix4x4( const Matrix4x4& rhs ) :
	m_row1( rhs.m_row1 ),
	m_row2( rhs.m_row2 ),
	m_row3( rhs.m_row3 ),
	m_row4( rhs.m_row4 )
{
}
Matrix4x4::Matrix4x4( const Matrix3x3& rhs ) :
	m_xAxis( rhs.m_row1 ),
	m_yAxis( rhs.m_row2 ),
	m_zAxis( rhs.m_row3 ),
	m_wAxis( Vector3::ZERO ),
	m_paddingX( 0.0 ), m_paddingY( 0.0 ), m_paddingZ( 0.0 ), m_paddingW( 1.0 )
{
}
Matrix4x4::Matrix4x4( const Matrix& rhs )
{
	e11 = rhs.GetElement( 0u, 0u );
	e12 = rhs.GetElement( 0u, 1u );
	e13 = rhs.GetElement( 0u, 2u );
	e14 = rhs.GetElement( 0u, 3u );
	e21 = rhs.GetElement( 1u, 0u );
	e22 = rhs.GetElement( 1u, 1u );
	e23 = rhs.GetElement( 1u, 2u );
	e24 = rhs.GetElement( 1u, 3u );
	e31 = rhs.GetElement( 2u, 0u );
	e32 = rhs.GetElement( 2u, 1u );
	e33 = rhs.GetElement( 2u, 2u );
	e34 = rhs.GetElement( 2u, 3u );
	e41 = rhs.GetElement( 3u, 0u );
	e42 = rhs.GetElement( 3u, 1u );
	e43 = rhs.GetElement( 3u, 2u );
	e44 = rhs.GetElement( 3u, 3u );
}
Matrix4x4::Matrix4x4( Matrix4x4&& rhs ) :
	m_row1( std::move( rhs.m_row1 ) ),
	m_row2( std::move( rhs.m_row2 ) ),
	m_row3( std::move( rhs.m_row3 ) ),
	m_row4( std::move( rhs.m_row4 ) )
{
}
Matrix4x4& Matrix4x4::operator=( const float64* rhs )
{
	for ( uint8 i = 0ui8; i < 16ui8; ++i )
		m_arr[ i ] = rhs[ i ];
	return *this;
}
Matrix4x4& Matrix4x4::operator=( const float32* rhs )
{
	for ( uint8 i = 0ui8; i < 16ui8; ++i )
		m_arr[ i ] = ( float64 )( rhs[ i ] );
	return *this;
}
Matrix4x4& Matrix4x4::operator=( const Vector4* rhs )
{
	m_row1 = rhs[ 0 ];
	m_row2 = rhs[ 1 ];
	m_row3 = rhs[ 2 ];
	m_row4 = rhs[ 3 ];
	return *this;
}
Matrix4x4& Matrix4x4::operator=( const Matrix4x4& rhs )
{
	if ( this != &rhs )
	{
		m_row1 = rhs.m_row1;
		m_row2 = rhs.m_row2;
		m_row3 = rhs.m_row3;
		m_row4 = rhs.m_row4;
	}
	return *this;
}
Matrix4x4& Matrix4x4::operator=( const Matrix3x3& rhs )
{
	m_xAxis = rhs.m_row1;
	m_yAxis = rhs.m_row2;
	m_zAxis = rhs.m_row3;
	m_wAxis = Vector3::ZERO;
	m_paddingX = m_paddingY = m_paddingZ = 0.0;
	m_paddingW = 1.0;
	return *this;
}
Matrix4x4& Matrix4x4::operator=( const Matrix& rhs )
{
	e11 = rhs.GetElement( 0u, 0u );
	e12 = rhs.GetElement( 0u, 1u );
	e13 = rhs.GetElement( 0u, 2u );
	e14 = rhs.GetElement( 0u, 3u );
	e21 = rhs.GetElement( 1u, 0u );
	e22 = rhs.GetElement( 1u, 1u );
	e23 = rhs.GetElement( 1u, 2u );
	e24 = rhs.GetElement( 1u, 3u );
	e31 = rhs.GetElement( 2u, 0u );
	e32 = rhs.GetElement( 2u, 1u );
	e33 = rhs.GetElement( 2u, 2u );
	e34 = rhs.GetElement( 2u, 3u );
	e41 = rhs.GetElement( 3u, 0u );
	e42 = rhs.GetElement( 3u, 1u );
	e43 = rhs.GetElement( 3u, 2u );
	e44 = rhs.GetElement( 3u, 3u );
	return *this;
}
Matrix4x4& Matrix4x4::operator=( Matrix4x4&& rhs )
{
	if ( this != &rhs )
	{
		m_row1 = std::move( rhs.m_row1 );
		m_row2 = std::move( rhs.m_row2 );
		m_row3 = std::move( rhs.m_row3 );
		m_row4 = std::move( rhs.m_row4 );
	}
	return *this;
}
Matrix4x4::~Matrix4x4( void )
{
}
Matrix4x4 Matrix4x4::operator+( void ) const
{
	return *this;
}
Matrix4x4 Matrix4x4::operator-( void ) const
{
	return Matrix4x4( -m_row1, -m_row2, -m_row3, -m_row4 );
}
Matrix4x4 Matrix4x4::operator+( const Matrix4x4& rhs ) const
{
	return Matrix4x4( m_row1 + rhs.m_row1,
					  m_row2 + rhs.m_row2,
					  m_row3 + rhs.m_row3,
					  m_row4 + rhs.m_row4 );
}
Matrix4x4 Matrix4x4::operator-( const Matrix4x4& rhs ) const
{
	return Matrix4x4( m_row1 - rhs.m_row1,
					  m_row2 - rhs.m_row2,
					  m_row3 - rhs.m_row3,
					  m_row4 - rhs.m_row4 );
}
Matrix4x4 Matrix4x4::operator*( const Matrix4x4& rhs ) const
{
	return Matrix4x4
	(
		e11 * rhs.e11 + e12 * rhs.e21 + e13 * rhs.e31 + e14 * rhs.e41,
		e11 * rhs.e12 + e12 * rhs.e22 + e13 * rhs.e32 + e14 * rhs.e42,
		e11 * rhs.e13 + e12 * rhs.e23 + e13 * rhs.e33 + e14 * rhs.e43,
		e11 * rhs.e14 + e12 * rhs.e24 + e13 * rhs.e34 + e14 * rhs.e44,
		e21 * rhs.e11 + e22 * rhs.e21 + e23 * rhs.e31 + e24 * rhs.e41,
		e21 * rhs.e12 + e22 * rhs.e22 + e23 * rhs.e32 + e24 * rhs.e42,
		e21 * rhs.e13 + e22 * rhs.e23 + e23 * rhs.e33 + e24 * rhs.e43,
		e21 * rhs.e14 + e22 * rhs.e24 + e23 * rhs.e34 + e24 * rhs.e44,
		e31 * rhs.e11 + e32 * rhs.e21 + e33 * rhs.e31 + e34 * rhs.e41,
		e31 * rhs.e12 + e32 * rhs.e22 + e33 * rhs.e32 + e34 * rhs.e42,
		e31 * rhs.e13 + e32 * rhs.e23 + e33 * rhs.e33 + e34 * rhs.e43,
		e31 * rhs.e14 + e32 * rhs.e24 + e33 * rhs.e34 + e34 * rhs.e44,
		e41 * rhs.e11 + e42 * rhs.e21 + e43 * rhs.e31 + e44 * rhs.e41,
		e41 * rhs.e12 + e42 * rhs.e22 + e43 * rhs.e32 + e44 * rhs.e42,
		e41 * rhs.e13 + e42 * rhs.e23 + e43 * rhs.e33 + e44 * rhs.e43,
		e41 * rhs.e14 + e42 * rhs.e24 + e43 * rhs.e34 + e44 * rhs.e44
	);
}
Matrix4x4 Matrix4x4::operator*( float64 rhs ) const
{
	return Matrix4x4( m_row1 * rhs, m_row2 * rhs, m_row3 * rhs, m_row4 * rhs );
}
Matrix4x4 Matrix4x4::operator/( float64 rhs ) const
{
	rhs = 1.0 / rhs;
	return Matrix4x4( m_row1 * rhs, m_row2 * rhs, m_row3 * rhs, m_row4 * rhs );
}
Matrix4x4& Matrix4x4::operator+=( const Matrix4x4& rhs )
{
	m_row1 += rhs.m_row1;
	m_row2 += rhs.m_row2;
	m_row3 += rhs.m_row3;
	m_row4 += rhs.m_row4;
	return *this;
}
Matrix4x4& Matrix4x4::operator-=( const Matrix4x4& rhs )
{
	m_row1 -= rhs.m_row1;
	m_row2 -= rhs.m_row2;
	m_row3 -= rhs.m_row3;
	m_row4 -= rhs.m_row4;
	return *this;
}
Matrix4x4& Matrix4x4::operator*=( const Matrix4x4& rhs )
{
	Matrix4x4& lhs = *this;
	return lhs = lhs * rhs;
}
Matrix4x4& Matrix4x4::operator*=( float64 rhs )
{
	m_row1 *= rhs;
	m_row2 *= rhs;
	m_row3 *= rhs;
	m_row4 *= rhs;
	return *this;
}
Matrix4x4& Matrix4x4::operator/=( float64 rhs )
{
	rhs = 1.0 / rhs;
	m_row1 *= rhs;
	m_row2 *= rhs;
	m_row3 *= rhs;
	m_row4 *= rhs;
	return *this;
}
bool Matrix4x4::operator==( const Matrix4x4& rhs ) const
{
	return m_row1 == rhs.m_row1 && m_row2 == rhs.m_row2 && m_row3 == rhs.m_row3 && m_row4 == rhs.m_row4;
}
bool Matrix4x4::operator!=( const Matrix4x4& rhs ) const
{
	return m_row1 != rhs.m_row1 || m_row2 != rhs.m_row2 || m_row3 != rhs.m_row3 || m_row4 != rhs.m_row4;
}
Matrix3x3 Matrix4x4::get3x3( void ) const
{
	return Matrix3x3( e11, e12, e13, e21, e22, e23, e31, e32, e33 );
}
Matrix4x4 operator*( float64 lhs, const Matrix4x4& rhs )
{
	return Matrix4x4( rhs.m_row1 * lhs, rhs.m_row2 * lhs, rhs.m_row3 * lhs, rhs.m_row4 * lhs );
}
Matrix4x4 Matrix4x4::getInverse( void ) const
{
	return getAdjugateMatrix() * ( 1.0 / getDeterminant() );
}
Matrix4x4 Matrix4x4::getInverse( const Matrix4x4& mat )
{
	return mat.getInverse();
}
Matrix4x4& Matrix4x4::Invert( void )
{
	return *this = getInverse();
}
Matrix4x4& Matrix4x4::Invert( Matrix4x4& mat )
{
	return mat = mat.getInverse();
}
float64 Matrix4x4::getDeterminant( void ) const
{
	return
		( e11 * Matrix3x3( e22, e23, e24, e32, e33, e34, e42, e43, e44 ).getDeterminant() ) -
		( e12 * Matrix3x3( e21, e23, e24, e31, e33, e34, e41, e43, e44 ).getDeterminant() ) +
		( e13 * Matrix3x3( e21, e22, e24, e31, e32, e34, e41, e42, e44 ).getDeterminant() ) -
		( e14 * Matrix3x3( e21, e22, e23, e31, e32, e33, e41, e42, e43 ).getDeterminant() );
}
float64 Matrix4x4::getDeterminant( const Matrix4x4& mat )
{
	return mat.getDeterminant();
}
Matrix4x4 Matrix4x4::getTranspose( void ) const
{
	return Matrix4x4( e11, e21, e31, e41,
					  e12, e22, e32, e42,
					  e13, e23, e33, e43,
					  e14, e24, e34, e44 );
}
Matrix4x4 Matrix4x4::getTranspose( const Matrix4x4& mat )
{
	return mat.getTranspose();
}
Matrix4x4& Matrix4x4::Transpose( void )
{
	return *this = getTranspose();
}
Matrix4x4& Matrix4x4::Transpose( Matrix4x4& mat )
{
	return mat = mat.getTranspose();
}
Matrix4x4 Matrix4x4::getMinorMatrix( void ) const
{
	return Matrix4x4
	(
		Matrix3x3( e22, e23, e24, e32, e33, e34, e42, e43, e44 ).getDeterminant(),
		Matrix3x3( e21, e23, e24, e31, e33, e34, e41, e43, e44 ).getDeterminant(),
		Matrix3x3( e21, e22, e24, e31, e32, e34, e41, e42, e44 ).getDeterminant(),
		Matrix3x3( e21, e22, e23, e31, e32, e33, e41, e42, e43 ).getDeterminant(),
		Matrix3x3( e12, e13, e14, e32, e33, e34, e42, e43, e44 ).getDeterminant(),
		Matrix3x3( e11, e13, e14, e31, e33, e34, e41, e43, e44 ).getDeterminant(),
		Matrix3x3( e11, e12, e14, e31, e32, e34, e41, e42, e44 ).getDeterminant(),
		Matrix3x3( e11, e12, e13, e31, e32, e33, e41, e42, e43 ).getDeterminant(),
		Matrix3x3( e12, e13, e14, e22, e23, e24, e42, e43, e44 ).getDeterminant(),
		Matrix3x3( e11, e13, e14, e21, e23, e24, e41, e43, e44 ).getDeterminant(),
		Matrix3x3( e11, e12, e14, e21, e22, e24, e41, e42, e44 ).getDeterminant(),
		Matrix3x3( e11, e12, e13, e21, e22, e23, e41, e42, e43 ).getDeterminant(),
		Matrix3x3( e12, e13, e14, e22, e23, e24, e32, e33, e34 ).getDeterminant(),
		Matrix3x3( e11, e13, e14, e21, e23, e24, e31, e33, e34 ).getDeterminant(),
		Matrix3x3( e11, e12, e14, e21, e22, e24, e31, e32, e34 ).getDeterminant(),
		Matrix3x3( e11, e12, e13, e21, e22, e23, e31, e32, e33 ).getDeterminant()
	);
}
Matrix4x4 Matrix4x4::getCofactorMatrix( void ) const
{
	Matrix4x4 outMat = getMinorMatrix();
	outMat.e12 = -outMat.e12;
	outMat.e14 = -outMat.e14;
	outMat.e21 = -outMat.e21;
	outMat.e23 = -outMat.e23;
	outMat.e32 = -outMat.e32;
	outMat.e34 = -outMat.e34;
	outMat.e41 = -outMat.e41;
	outMat.e43 = -outMat.e43;
	return outMat;
}
Matrix4x4 Matrix4x4::getAdjugateMatrix( void ) const
{
	return getCofactorMatrix().getTranspose();
}
Matrix4x4 Matrix4x4::getMinorMatrix( const Matrix4x4& mat )
{
	return mat.getMinorMatrix();
}
Matrix4x4 Matrix4x4::getCofactorMatrix( const Matrix4x4& mat )
{
	return mat.getCofactorMatrix();
}
Matrix4x4 Matrix4x4::getAdjugateMatrix( const Matrix4x4& mat )
{
	return mat.getAdjugateMatrix();
}
Matrix4x4 Matrix4x4::TranslationMatrix( const Vector3& translationVector )
{
	return Matrix4x4( 1.0, 0.0, 0.0, 0.0,
					  0.0, 1.0, 0.0, 0.0,
					  0.0, 0.0, 1.0, 0.0,
					  translationVector.x,
					  translationVector.y,
					  translationVector.z, 1.0 );
}
Matrix4x4 Matrix4x4::ScaleMatrix( const Vector3& scaleVector )
{
	return Matrix4x4( scaleVector.x, 0.0, 0.0, 0.0,
					  0.0, scaleVector.y, 0.0, 0.0,
					  0.0, 0.0, scaleVector.z, 0.0,
					  0.0, 0.0, 0.0, 1.0 );
}
Matrix4x4 Matrix4x4::ScaleMatrix( float64 scaleValue )
{
	return Matrix4x4( scaleValue, 0.0, 0.0, 0.0,
					  0.0, scaleValue, 0.0, 0.0,
					  0.0, 0.0, scaleValue, 0.0,
					  0.0, 0.0, 0.0, 1.0 );
}
Matrix4x4 Matrix4x4::RotationXMatrix( float64 angle )
{
	const float64 sinAngle = Math::Sin( angle );
	const float64 cosAngle = Math::Cos( angle );
	return Matrix4x4( 1.0, 0.0, 0.0, 0.0,
					  0.0, cosAngle, sinAngle, 0.0,
					  0.0, -sinAngle, cosAngle, 0.0,
					  0.0, 0.0, 0.0, 1.0 );
}
Matrix4x4 Matrix4x4::RotationYMatrix( float64 angle )
{
	const float64 sinAngle = Math::Sin( angle );
	const float64 cosAngle = Math::Cos( angle );
	return Matrix4x4( cosAngle, 0.0, -sinAngle, 0.0,
					  0.0, 1.0, 0.0, 0.0,
					  sinAngle, 0.0, cosAngle, 0.0,
					  0.0, 0.0, 0.0, 1.0 );
}
Matrix4x4 Matrix4x4::RotationZMatrix( float64 angle )
{
	const float64 sinAngle = Math::Sin( angle );
	const float64 cosAngle = Math::Cos( angle );
	return Matrix4x4( cosAngle, sinAngle, 0.0, 0.0,
					  -sinAngle, cosAngle, 0.0, 0.0,
					  0.0, 0.0, 1.0, 0.0,
					  0.0, 0.0, 0.0, 1.0 );
}
Matrix4x4 Matrix4x4::AxisAngleRotation( const Vector3& axis, float64 angle )
{
	const float64& x = axis.x;
	const float64& y = axis.y;
	const float64& z = axis.z;
	const float64 cs = Math::Cos( angle );
	const float64 sn = Math::Sin( angle );
	const float64 _1cs = 1.0 - cs;
	return Matrix4x4( cs + x * x * _1cs, x * y * _1cs + z * sn, x * z * _1cs - y * sn, 0.0,
					  x * y * _1cs - z * sn, cs + y * y * _1cs, y * z * _1cs + x * sn, 0.0,
					  x * z * _1cs + y * sn, y * z * _1cs - x * sn, cs + z * z * _1cs, 0.0,
					  0.0, 0.0, 0.0, 1.0 );
}
Vector4 Matrix4x4::operator*( const Vector4& v ) const
{
	const Matrix4x4 m = getTranspose();
	return Vector4
	(
		v.x * m.e11 + v.y * m.e12 + v.z * m.e13 + v.w * m.e14,
		v.x * m.e21 + v.y * m.e22 + v.z * m.e23 + v.w * m.e24,
		v.x * m.e31 + v.y * m.e32 + v.z * m.e33 + v.w * m.e34,
		v.x * m.e41 + v.y * m.e42 + v.z * m.e43 + v.w * m.e44
	);
}
Vector3 Matrix4x4::operator*( const Vector3& v ) const
{
	const Matrix4x4 m = getTranspose();
	return Vector3
	(
		v.x * m.e11 + v.y * m.e12 + v.z * m.e13 + m.e14,
		v.x * m.e21 + v.y * m.e22 + v.z * m.e23 + m.e24,
		v.x * m.e31 + v.y * m.e32 + v.z * m.e33 + m.e34
	) / ( v.x * m.e41 + v.y * m.e42 + v.z * m.e43 + m.e44 );
}
Vector4 operator*( const Vector4& v, const Matrix4x4& m )
{
	return Vector4
	(
		v.x * m.e11 + v.y * m.e21 + v.z * m.e31 + v.w * m.e41,
		v.x * m.e12 + v.y * m.e22 + v.z * m.e32 + v.w * m.e42,
		v.x * m.e13 + v.y * m.e23 + v.z * m.e33 + v.w * m.e43,
		v.x * m.e14 + v.y * m.e24 + v.z * m.e34 + v.w * m.e44
	);
}
Vector3 operator*( const Vector3& v, const Matrix4x4& m )
{
	return Vector3
	(
		v.x * m.e11 + v.y * m.e21 + v.z * m.e31 + m.e41,
		v.x * m.e12 + v.y * m.e22 + v.z * m.e32 + m.e42,
		v.x * m.e13 + v.y * m.e23 + v.z * m.e33 + m.e43
	) / ( v.x * m.e14 + v.y * m.e24 + v.z * m.e34 + m.e44 );
}