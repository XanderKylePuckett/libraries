#include "stdafx.h"
#include "Input.h"

enum GameStates
{
	DefaultState,
	NumStates
};

static const uint64 NUM_STATES = ( uint64 )GameStates::NumStates;

class Minesweeper;

class GameState
{
protected:
	Minesweeper* m_game;
public:
	virtual void Init( Minesweeper* ) = 0;
	virtual void Enter( void ) = 0;
	virtual void Input( void ) = 0;
	virtual void Update( uint64 ) = 0;
	virtual void Render( void ) const = 0;
	virtual void Exit( void ) = 0;
	virtual void Cleanup( void ) = 0;
};

class DefaultGameState : public GameState
{
public:
	virtual void Init( Minesweeper* );
	virtual void Enter( void );
	virtual void Input( void );
	virtual void Update( uint64 );
	virtual void Render( void ) const;
	virtual void Exit( void );
	virtual void Cleanup( void );
};

class Minesweeper
{
private:
	bool m_isRunning;
	GameState* m_currState;
	GameState** m_gameStates;
	void SwitchGameState( GameStates );
	void Input( void );
	void Update( uint64 );
	void Render( void ) const;
	void Init( void );
	void Cleanup( void );
	Minesweeper( const Minesweeper& );
	Minesweeper( Minesweeper&& );
	Minesweeper& operator=( const Minesweeper& );
	Minesweeper& operator=( Minesweeper&& );
public:
	Minesweeper( void );
	~Minesweeper( void );
	void Run( void );
	void Stop( void );
};

void Minesweeper::Run( void )
{
	Init();
	for ( uint64 frame = 0ull; m_isRunning; ++frame )
	{
		Input();
		Update( frame );
		Render();
	}
	Cleanup();
}

int MinesweeperMain( int, char** )
{
	Minesweeper().Run();
	return EXIT_SUCCESS;
}

Minesweeper::Minesweeper( void ) :
	m_isRunning( false ),
	m_currState( nullptr ),
	m_gameStates( nullptr )
{
}
Minesweeper::~Minesweeper( void )
{
}
void Minesweeper::Init( void )
{
	m_gameStates = new GameState*[ NUM_STATES ];
	m_gameStates[ GameStates::DefaultState ] = new DefaultGameState();
	for ( uint64 i = 0ull; i < NUM_STATES; ++i )
		m_gameStates[ i ]->Init( this );
	m_currState = m_gameStates[ GameStates::DefaultState ];
	m_currState->Enter();
	m_isRunning = true;
}
void Minesweeper::Cleanup( void )
{
	m_currState->Exit();
	m_currState = nullptr;
	for ( uint64 i = 0ull; i < NUM_STATES; ++i )
	{
		m_gameStates[ i ]->Cleanup();
		delete m_gameStates[ i ];
	}
	delete[ ] m_gameStates;
	m_gameStates = nullptr;
}
void Minesweeper::SwitchGameState( GameStates _state )
{
	m_currState->Exit();
	m_currState = m_gameStates[ ( uint64 )_state ];
	m_currState->Enter();
}
void Minesweeper::Input( void )
{
	Input::Update();
	m_currState->Input();
}
void Minesweeper::Update( uint64 _frame )
{
	m_currState->Update( _frame );
}
void Minesweeper::Render( void ) const
{
	m_currState->Render();
}
void Minesweeper::Stop( void )
{
	m_isRunning = false;
}
void DefaultGameState::Init( Minesweeper* _game )
{
	m_game = _game;
}
void DefaultGameState::Enter( void )
{
}
void DefaultGameState::Input( void )
{
}
void DefaultGameState::Update( uint64 _frame )
{
	if ( _frame > 9000ull )
		m_game->Stop();
}
void DefaultGameState::Render( void ) const
{
}
void DefaultGameState::Exit( void )
{
}
void DefaultGameState::Cleanup( void )
{
}