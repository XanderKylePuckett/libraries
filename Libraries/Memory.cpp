#include "stdafx.h"
#include "Memory.h"

void Memory::Copy( const void* _source, void* _destination, uint32 _size )
{
	const uint8* const source_ = ( const uint8* )_source;
	uint8* const destination_ = ( uint8* )_destination;
	for ( uint32 i = 0u; i < _size; ++i )
		destination_[ i ] = source_[ i ];
}
void Memory::Zeros( void* _data, uint32 _size )
{
	uint8* const data_ = ( uint8* )_data;
	for ( uint32 i = 0u; i < _size; ++i )
		data_[ i ] = 0ui8;
}
void Memory::Ones( void* _data, uint32 _size )
{
	uint8* const data_ = ( uint8* )_data;
	for ( uint32 i = 0u; i < _size; ++i )
		data_[ i ] = 0xffui8;
}
bool Memory::IsEqual( const void* _lhs, const void* _rhs, uint32 _size )
{
	const uint8* const lhs_ = ( const uint8* )_lhs;
	const uint8* const rhs_ = ( const uint8* )_rhs;
	for ( uint32 i = 0u; i < _size; ++i )
		if ( lhs_[ i ] != rhs_[ i ] )
			return false;
	return true;
}
bool Memory::IsZero( const void* _data, uint32 _size )
{
	const uint8* const data_ = ( const uint8* )_data;
	for ( uint32 i = 0u; i < _size; ++i )
		if ( 0ui8 != data_[ i ] )
			return false;
	return true;
}