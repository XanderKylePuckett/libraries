#include "stdafx.h"
#include "Raster.h"
#include "Rasterizer.h"
#include "BitmapImage.h"

struct XKPLine
{
	uint16 startX, startY, endX, endY;
};
int DrawXKPMain( int, char** )
{
	std::ifstream xkpFile;
	xkpFile.open( "XKP_Lines_Short.bin", std::ios_base::in | std::ios_base::binary );
	if ( xkpFile.is_open() )
	{
		uint16 size, i;
		XKPLine xkpline;
		xkpFile.read( ( char* )( &size ), sizeof( uint16 ) );
		RasterLine* rasterlines = new RasterLine[ size ];
		for ( i = 0ui16; i < size; ++i )
		{
			xkpFile.read( ( char* )( &( xkpline ) ), sizeof( XKPLine ) );
			rasterlines[ i ].m_start.m_posX = ( ( ( ( int32 )( xkpline.startX ) ) >> 2 ) + 5 );
			rasterlines[ i ].m_start.m_posY = ( ( ( ( int32 )( xkpline.startY ) ) >> 2 ) + 5 );
			rasterlines[ i ].m_end.m_posX = ( ( ( ( int32 )( xkpline.endX ) ) >> 2 ) + 5 );
			rasterlines[ i ].m_end.m_posY = ( ( ( ( int32 )( xkpline.endY ) ) >> 2 ) + 5 );
		}
		xkpFile.close();
		Raster::InitWindow( 970u, 682u );
		do
		{
			Raster::GetTexture() = Color::BLACK;
			for ( i = 0ui16; i < size; ++i )
				DrawRasterLine( rasterlines[ i ] );
		} while ( Raster::UpdateWindow() );
		Raster::ShutdownWindow();
		delete[ ] rasterlines;
	}
	else
		return EXIT_FAILURE;
	return EXIT_SUCCESS;
}