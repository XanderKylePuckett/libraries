#include "stdafx.h"
#include "MpegWriter.h"
#pragma warning(disable:6297)

static const uint8 g_HuffmanTblDCY[ 18u ] =
{
	4ui8, 3ui8, 0ui8, 2ui8, 1ui8, 2ui8,
	5ui8, 3ui8, 6ui8, 3ui8, 14ui8, 4ui8,
	30ui8, 5ui8, 62ui8, 6ui8, 126ui8, 7ui8
};
static const uint8 g_HuffmanTblDCC[ 18u ] =
{
	0ui8, 2ui8, 1ui8, 2ui8, 2ui8, 2ui8,
	6ui8, 3ui8, 14ui8, 4ui8, 30ui8, 5ui8,
	62ui8, 6ui8, 126ui8, 7ui8, 254ui8, 8ui8
};
static const uint8 g_HuffmanTblAC[ 32u ][ 40u ][ 2u ] =
{
	{
		{ 6ui8, 3ui8 }, { 8ui8, 5ui8 }, { 10ui8, 6ui8 }, { 12ui8, 8ui8 },
		{ 76ui8, 9ui8 }, { 66ui8, 9ui8 }, { 20ui8, 11ui8 }, { 58ui8, 13ui8 },
		{ 48ui8, 13ui8 }, { 38ui8, 13ui8 }, { 32ui8, 13ui8 }, { 52ui8, 14ui8 },
		{ 50ui8, 14ui8 }, { 48ui8, 14ui8 }, { 46ui8, 14ui8 }, { 62ui8, 15ui8 },
		{ 62ui8, 15ui8 }, { 58ui8, 15ui8 }, { 56ui8, 15ui8 }, { 54ui8, 15ui8 },
		{ 52ui8, 15ui8 }, { 50ui8, 15ui8 }, { 48ui8, 15ui8 }, { 46ui8, 15ui8 },
		{ 44ui8, 15ui8 }, { 42ui8, 15ui8 }, { 40ui8, 15ui8 }, { 38ui8, 15ui8 },
		{ 36ui8, 15ui8 }, { 34ui8, 15ui8 }, { 32ui8, 15ui8 }, { 48ui8, 16ui8 },
		{ 46ui8, 16ui8 }, { 44ui8, 16ui8 }, { 42ui8, 16ui8 }, { 40ui8, 16ui8 },
		{ 38ui8, 16ui8 }, { 36ui8, 16ui8 }, { 34ui8, 16ui8 }, { 32ui8, 16ui8 }
	},
	{
		{ 6ui8, 4ui8 }, { 12ui8, 7ui8 }, { 74ui8, 9ui8 }, { 24ui8, 11ui8 },
		{ 54ui8, 13ui8 }, { 44ui8, 14ui8 }, { 42ui8, 14ui8 }, { 62ui8, 16ui8 },
		{ 60ui8, 16ui8 }, { 58ui8, 16ui8 }, { 56ui8, 16ui8 }, { 54ui8, 16ui8 },
		{ 52ui8, 16ui8 }, { 50ui8, 16ui8 }, { 38ui8, 17ui8 }, { 36ui8, 17ui8 },
		{ 34ui8, 17ui8 }, { 32ui8, 17ui8 }
	},
	{ { 10ui8, 5ui8 }, { 8ui8, 8ui8 }, { 22ui8, 11ui8 }, { 40ui8, 13ui8 }, { 40ui8, 14ui8 } },
	{ { 14ui8, 6ui8 }, { 72ui8, 9ui8 }, { 56ui8, 13ui8 }, { 38ui8, 14ui8 } },
	{ { 12ui8, 6ui8 }, { 30ui8, 11ui8 }, { 36ui8, 13ui8 } },
	{ { 14ui8, 7ui8 }, { 18ui8, 11ui8 }, { 36ui8, 14ui8 } },
	{ { 10ui8, 7ui8 }, { 60ui8, 13ui8 }, { 40ui8, 17ui8 } },
	{ { 8ui8, 7ui8 }, { 42ui8, 13ui8 } }, { { 14ui8, 8ui8 }, { 34ui8, 13ui8 } },
	{ { 10ui8, 8ui8 }, { 34ui8, 14ui8 } }, { { 78ui8, 9ui8 }, { 32ui8, 14ui8 } },
	{ { 70ui8, 9ui8 }, { 52ui8, 17ui8 } }, { { 68ui8, 9ui8 }, { 50ui8, 17ui8 } },
	{ { 64ui8, 9ui8 }, { 48ui8, 17ui8 } }, { { 28ui8, 11ui8 }, { 46ui8, 17ui8 } },
	{ { 26ui8, 11ui8 }, { 44ui8, 17ui8 } }, { { 16ui8, 11ui8 }, { 42ui8, 17ui8 } },
	{ { 62ui8, 13ui8 } }, { { 52ui8, 13ui8 } }, { { 50ui8, 13ui8 } }, { { 46ui8, 13ui8 } },
	{ { 44ui8, 13ui8 } }, { { 62ui8, 14ui8 } }, { { 60ui8, 14ui8 } }, { { 58ui8, 14ui8 } },
	{ { 56ui8, 14ui8 } }, { { 54ui8, 14ui8 } }, { { 62ui8, 17ui8 } }, { { 60ui8, 17ui8 } },
	{ { 58ui8, 17ui8 } }, { { 56ui8, 17ui8 } }, { { 54ui8, 17ui8 } }
};
static const float64 g_quantTbl[ 64u ] =
{
	0.015625, 0.005632, 0.005035, 0.004832, 0.004808, 0.005892, 0.007964, 0.013325,
	0.005632, 0.004061, 0.003135, 0.003193, 0.003338, 0.003955, 0.004898, 0.008828,
	0.005035, 0.003135, 0.002816, 0.003013, 0.003299, 0.003581, 0.005199, 0.009125,
	0.004832, 0.003484, 0.003129, 0.003348, 0.003666, 0.003979, 0.005309, 0.009632,
	0.005682, 0.003466, 0.003543, 0.003666, 0.003906, 0.004546, 0.005774, 0.009439,
	0.006119, 0.004248, 0.004199, 0.004228, 0.004546, 0.005062, 0.006124, 0.009942,
	0.008883, 0.006167, 0.006096, 0.005777, 0.006078, 0.006391, 0.007621, 0.012133,
	0.016780, 0.011263, 0.009907, 0.010139, 0.009849, 0.010297, 0.012133, 0.019785
};
static const uint8 g_zigzag[ 64u ] =
{
	0ui8, 1ui8, 5ui8, 6ui8, 14ui8, 15ui8, 27ui8, 28ui8,
	2ui8, 4ui8, 7ui8, 13ui8, 16ui8, 26ui8, 29ui8, 42ui8,
	3ui8, 8ui8, 12ui8, 17ui8, 25ui8, 30ui8, 41ui8, 43ui8,
	9ui8, 11ui8, 18ui8, 24ui8, 31ui8, 40ui8, 44ui8, 53ui8,
	10ui8, 19ui8, 23ui8, 32ui8, 39ui8, 45ui8, 52ui8, 54ui8,
	20ui8, 22ui8, 33ui8, 38ui8, 46ui8, 51ui8, 55ui8, 60ui8,
	21ui8, 34ui8, 37ui8, 47ui8, 50ui8, 56ui8, 59ui8, 61ui8,
	35ui8, 36ui8, 48ui8, 49ui8, 57ui8, 58ui8, 62ui8, 63ui8
};
static void DiscreteCosineTransform( float64& _d0, float64& _d1, float64& _d2, float64& _d3,
									 float64& _d4, float64& _d5, float64& _d6, float64& _d7 )
{
	static const uint64 u1_ = 0x3fe6a09e667f3bccull; // sqrt(2)/2
	static const uint64 u2_ = 0x3fd87de2a6aea962ull; // sqrt(2-sqrt(2))/2
	static const uint64 u3_ = 0x3fe1517a7bdb3895ull; // sqrt(1-sqrt(2)/2)
	static const uint64 u4_ = 0x3ff4e7ae9144f0fcull; // sqrt(1+sqrt(2)/2)
	static const float64& f1_ = *( ( float64* )( &u1_ ) );
	static const float64& f2_ = *( ( float64* )( &u2_ ) );
	static const float64& f3_ = *( ( float64* )( &u3_ ) );
	static const float64& f4_ = *( ( float64* )( &u4_ ) );
	const float64 t1_ = _d0;
	const float64 t2_ = _d0 - _d1 - _d2 + _d3 - _d5 - _d6 + _d7;
	const float64 t3_ = ( _d1 + _d2 - _d5 - _d6 ) * f1_;
	const float64 t4_ = ( _d0 + _d1 - _d2 - _d3 - _d4 - _d5 + _d6 + _d7 ) * f1_;
	const float64 t5_ = ( _d7 - _d0 - _d1 + _d2 + _d3 - _d4 - _d5 + _d6 ) * f2_;
	const float64 t6_ = _d0 - _d3 - _d4 + _d7 - t4_;
	const float64 t7_ = ( _d2 + _d3 - _d4 - _d5 ) * f3_ + t5_;
	const float64 t8_ = ( _d0 + _d1 - _d6 - _d7 ) * f4_ + t5_;
	_d0 += _d1 + _d2 + _d3 + _d4 + _d5 + _d6 + _d7;
	_d2 = t1_ - _d3 - _d4 + _d7 + t4_;
	_d1 = t1_ - _d7 + t3_ + t8_;
	_d3 = t1_ - _d7 - t7_ - t3_;
	_d5 = t1_ - _d7 + t7_ - t3_;
	_d7 = t1_ - _d7 + t3_ - t8_;
	_d4 += t2_;
	_d6 = t6_;
}
void MpegWriter::WriteBits( uint32 _v, uint32 _c )
{
	uint8 t_;
	m_cnt += _c;
	m_buf |= _v << ( 24u - m_cnt );
	while ( m_cnt >= 8u )
	{
		m_file.write( ( char* )( &( t_ = ( ( uint8 )( ( m_buf ) >> 16u ) ) ) ), 1u );
		m_buf <<= 8u;
		m_cnt -= 8u;
	}
}
int32 MpegWriter::ProcessDU( float64* _A, const uint8* _htdc, int32 _DC )
{
	int32* Q_ = ( int32* )( m_tempDataBuffer + 896u );
	float64 v_;
	uint32 size_ = 0u, endpos_ = 63u, i_, run_, absAC_, code_, absDC_, i0_;
	int32 AC_;
	for ( i0_ = 0u; i0_ < 64u; i0_ += 8u )
		DiscreteCosineTransform( _A[ i0_ ], _A[ i0_ + 1u ], _A[ i0_ + 2u ], _A[ i0_ + 3u ],
								 _A[ i0_ + 4u ], _A[ i0_ + 5u ], _A[ i0_ + 6u ], _A[ i0_ + 7u ] );
	for ( i0_ = 0u; i0_ < 8u; ++i0_ )
		DiscreteCosineTransform( _A[ i0_ ], _A[ i0_ + 8u ], _A[ i0_ + 16u ], _A[ i0_ + 24u ],
								 _A[ i0_ + 32u ], _A[ i0_ + 40u ], _A[ i0_ + 48u ], _A[ i0_ + 56u ] );
	for ( i0_ = 0u; i0_ < 64u; ++i0_ )
	{
		v_ = _A[ i0_ ] * g_quantTbl[ i0_ ];
		Q_[ g_zigzag[ i0_ ] ] = ( int32 )( v_ < 0.0 ? ceil( v_ - 0.5 ) : floor( v_ + 0.5 ) );
	}
	_DC = Q_[ 0u ] - _DC;
	absDC_ = ( uint32 )( _DC < 0 ? -_DC : _DC );
	for ( i0_ = absDC_; 0u != i0_; i0_ >>= 1u )
		++size_;
	WriteBits( ( uint32 )( _htdc[ size_ << 1u ] ), ( uint32 )( _htdc[ ( size_ << 1u ) + 1u ] ) );
	if ( _DC < 0 )
		absDC_ ^= ( ( 1u << size_ ) - 1u );
	WriteBits( absDC_, size_ );
	while ( 0u < endpos_ && 0 == Q_[ endpos_ ] )
		--endpos_;
	for ( i_ = 1u; endpos_ >= i_; ++i_ )
	{
		for ( run_ = 0u; 0 == Q_[ i_ ] && i_ < endpos_; ++run_, ++i_ );
		AC_ = Q_[ i_ ];
		absAC_ = ( uint32 )( AC_ < 0 ? -AC_ : AC_ );
		code_ = size_ = 0u;
		if ( run_ < 32u && 40u >= absAC_ )
		{
			code_ = g_HuffmanTblAC[ run_ ][ absAC_ - 1u ][ 0u ];
			size_ = g_HuffmanTblAC[ run_ ][ absAC_ - 1u ][ 1u ];
			if ( AC_ < 0 )
				++code_;
		}
		if ( 0u == size_ )
		{
			WriteBits( 1u, 6u );
			WriteBits( run_, 6u );
			if ( AC_ < -127 )
				WriteBits( 128u, 8u );
			else if ( 127 < AC_ )
				WriteBits( 0u, 8u );
			code_ = ( uint32 )( AC_ & 255 );
			size_ = 8u;
		}
		WriteBits( code_, size_ );
	}
	WriteBits( 2u, 2u );
	return Q_[ 0u ];
}
MpegWriter::MpegWriter( void ) :
	m_width( 0u ),
	m_height( 0u ),
	m_fps( MPEG_FPS::FRAMERATE_60_FPS ),
	m_tempDataBuffer( nullptr )
{
}
MpegWriter::MpegWriter( const char* const _filename, uint32 _width, uint32 _height, MPEG_FPS _fps ) :
	m_width( _width ),
	m_height( _height ),
	m_fps( _fps ),
	m_tempDataBuffer( nullptr )
{
	if ( 0u != m_width && 0u != m_height )
		m_file.open( _filename, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary );
	else
		m_width = m_height = 0u;
	if ( m_file.is_open() )
		m_tempDataBuffer = new float64[ 928u ];
}
MpegWriter::~MpegWriter( void )
{
	if ( m_file.is_open() )
		m_file.close();
	delete[ ] m_tempDataBuffer;
}
void MpegWriter::AddFrame( const uint32* _argb )
{
	if ( !m_file.is_open() )
		return;
	static const char* const tmpStr1_ = "\x00\x00\x01\xB3";
	static const char* const tmpStr2_ = "\xFF\xFF\xE0\xA0\x00\x00\x01\xB8\x80\x08\x00\x40\x00\x00\x01\x00\x00\x0C\x00\x00\x00\x00\x01\x01";
	static const char* const tmpStr3_ = "\x00\x00\x01\xB7";
	static const float64 inv255_ = 1.0 / 255.0;
	float64* const CBx_ = m_tempDataBuffer + 256u;
	float64* const CRx_ = m_tempDataBuffer + 512u;
	float64* const CB_ = m_tempDataBuffer + 768u;
	float64* const CR_ = m_tempDataBuffer + 832u;
	const uint8* c_;
	float64 b_, g_, r_;
	uint32 vblock_, hblock_, x_, y_, i_, j_, k_;
	const uint32 size_ = m_width * m_height;
	const uint32 width4_ = ( m_width << 2u );
	const uint32 hblockmax_ = ( ( m_width + 15u ) >> 4u );
	const uint32 vblockmax_ = ( ( m_height + 15u ) >> 4u );
	int32 lastDCY_ = 128, lastDCCR_ = 128, lastDCCB_ = 128;
	uint8 tmpC_;
	m_cnt = m_buf = 0u;
	m_file.write( tmpStr1_, 4u );
	m_file.write( ( char* )&( tmpC_ = ( uint8 )( m_width >> 4u ) ), 1u );
	m_file.write( ( char* )&( tmpC_ = ( uint8 )( ( ( m_width & 0x0fu ) << 4u ) | ( ( m_height >> 8u ) & 0x0fu ) ) ), 1u );
	m_file.write( ( char* )&( tmpC_ = ( uint8 )m_height ), 1u );
	m_file.write( ( char* )&( tmpC_ = ( uint8 )m_fps ), 1u );
	m_file.write( tmpStr2_, 24u );
	WriteBits( 16u, 6u );
	for ( vblock_ = 0u; vblock_ < vblockmax_; ++vblock_ ) for ( hblock_ = 0u; hblock_ < hblockmax_; ++hblock_ )
	{
		WriteBits( 3u, 2u );
		for ( i_ = 0u; i_ < 256u; ++i_ )
		{
			x_ = ( hblock_ << 4u ) + ( i_ & 15u );
			y_ = ( vblock_ << 4u ) + ( i_ >> 4u );
			if ( x_ >= m_width ) x_ = m_width - 1u;
			if ( y_ >= m_height ) y_ = m_height - 1u;
			c_ = ( ( const uint8* )_argb ) + y_ * width4_ + ( x_ << 2u );
			b_ = ( float64 )( c_[ 0u ] );
			g_ = ( float64 )( c_[ 1u ] );
			r_ = ( float64 )( c_[ 2u ] );
			m_tempDataBuffer[ i_ ] = 16.0 + ( 24.966 * b_ + 128.553 * g_ + 65.481 * r_ ) * inv255_;
			CBx_[ i_ ] = ( 198.464 * b_ - 131.488 * g_ - 66.976 * r_ ) * inv255_;
			CRx_[ i_ ] = ( -25.536 * b_ - 131.488 * g_ + 157.024 * r_ ) * inv255_;
		}
		for ( i_ = 0u; i_ < 64u; ++i_ )
		{
			j_ = ( ( i_ & 7u ) << 1u ) + ( ( i_ & 56u ) << 2u );
			CB_[ i_ ] = ( CBx_[ j_ ] + CBx_[ j_ + 1u ] + CBx_[ j_ + 16u ] + CBx_[ j_ + 17u ] + 512.0 ) * 0.25;
			CR_[ i_ ] = ( CRx_[ j_ ] + CRx_[ j_ + 1u ] + CRx_[ j_ + 16u ] + CRx_[ j_ + 17u ] + 512.0 ) * 0.25;
		}
		for ( i_ = 0u; i_ < 2u; ++i_ ) for ( j_ = 0u; j_ < 2u; ++j_ )
		{
			for ( k_ = 0u; k_ < 64u; k_ += 8u )
				memcpy( CBx_ + k_, m_tempDataBuffer + ( ( i_ << 7u ) + ( j_ << 3u ) + ( ( k_ & 56u ) << 1u ) ), 64u );
			lastDCY_ = ProcessDU( CBx_, g_HuffmanTblDCY, lastDCY_ );
		}
		lastDCCB_ = ProcessDU( CB_, g_HuffmanTblDCC, lastDCCB_ );
		lastDCCR_ = ProcessDU( CR_, g_HuffmanTblDCC, lastDCCR_ );
	}
	WriteBits( 0u, 7u );
	m_file.write( tmpStr3_, 4u );
}