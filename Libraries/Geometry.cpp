#include "stdafx.h"
#include "Geometry.h"

PositionColorVertex::PositionColorVertex( void )
{
}
PositionColorVertex::PositionColorVertex( const Vector3& _position, const Color& _color ) :
	m_position( _position ), m_color( _color )
{
}
PositionColorVertex::PositionColorVertex( const PositionColorVertex& _rhs ) :
	m_position( _rhs.m_position ), m_color( _rhs.m_color )
{
}
Line::Line( void )
{
}
Line::Line( const PositionColorVertex& _start, const PositionColorVertex& _end ) :
	m_start( _start ), m_end( _end )
{
}
Line::Line( const Line& _rhs ) :
	m_start( _rhs.m_start ), m_end( _rhs.m_end )
{
}
RasterPoint::RasterPoint( void )
{
}
RasterPoint::RasterPoint( int32 _posX, int32 _posY ) :
	m_posX( _posX ), m_posY( _posY )
{
}
RasterPoint::RasterPoint( const RasterPoint& _rhs ) :
	m_posX( _rhs.m_posX ), m_posY( _rhs.m_posY )
{
}
RasterLine::RasterLine( void )
{
}
RasterLine::RasterLine( const RasterPoint& _start, const RasterPoint& _end ) :
	m_start( _start ), m_end( _end )
{
}
RasterLine::RasterLine( const RasterLine& _rhs ) :
	m_start( _rhs.m_start ), m_end( _rhs.m_end )
{
}
PositionColorTriangle::PositionColorTriangle( void )
{
}
PositionColorTriangle::PositionColorTriangle(
	const PositionColorVertex& _vertA,
	const PositionColorVertex& _vertB,
	const PositionColorVertex& _vertC ) :
	m_vertA( _vertA ),
	m_vertB( _vertB ),
	m_vertC( _vertC )
{
}
PositionColorTriangle::PositionColorTriangle( const PositionColorTriangle& _rhs ) :
	m_vertA( _rhs.m_vertA ),
	m_vertB( _rhs.m_vertB ),
	m_vertC( _rhs.m_vertC )
{
}
bool PositionColorVertex::operator==( const PositionColorVertex& _rhs ) const
{
	return m_position == _rhs.m_position && m_color == _rhs.m_color;
}
bool PositionColorVertex::operator!=( const PositionColorVertex& _rhs ) const
{
	return m_position != _rhs.m_position || m_color != _rhs.m_color;
}
bool Line::operator==( const Line& _rhs ) const
{
	return ( m_start == _rhs.m_start && m_end == _rhs.m_end ) ||
		( m_start == _rhs.m_end && m_end == _rhs.m_start );
}
bool Line::operator!=( const Line& _rhs ) const
{
	return ( m_start != _rhs.m_start || m_end != _rhs.m_end ) &&
		( m_start != _rhs.m_end || m_end != _rhs.m_start );
}
bool RasterPoint::operator==( const RasterPoint& _rhs ) const
{
	return m_posX == _rhs.m_posX && m_posY == _rhs.m_posY;
}
bool RasterPoint::operator!=( const RasterPoint& _rhs ) const
{
	return m_posX != _rhs.m_posX || m_posY != _rhs.m_posY;
}
bool RasterLine::operator==( const RasterLine& _rhs ) const
{
	return ( m_start == _rhs.m_start && m_end == _rhs.m_end ) ||
		( m_start == _rhs.m_end && m_end == _rhs.m_start );
}
bool RasterLine::operator!=( const RasterLine& _rhs ) const
{
	return ( m_start != _rhs.m_start || m_end != _rhs.m_end ) &&
		( m_start != _rhs.m_end || m_end != _rhs.m_start );
}
bool PositionColorTriangle::operator==( const PositionColorTriangle& _rhs ) const
{
	return ( m_vertA == _rhs.m_vertA && m_vertB == _rhs.m_vertB && m_vertC == _rhs.m_vertC ) ||
		( m_vertA == _rhs.m_vertB && m_vertB == _rhs.m_vertC && m_vertC == _rhs.m_vertA ) ||
		( m_vertA == _rhs.m_vertC && m_vertB == _rhs.m_vertA && m_vertC == _rhs.m_vertB ) ||
		( m_vertA == _rhs.m_vertC && m_vertB == _rhs.m_vertB && m_vertC == _rhs.m_vertA ) ||
		( m_vertA == _rhs.m_vertB && m_vertB == _rhs.m_vertA && m_vertC == _rhs.m_vertC ) ||
		( m_vertA == _rhs.m_vertA && m_vertB == _rhs.m_vertC && m_vertC == _rhs.m_vertB );
}
bool PositionColorTriangle::operator!=( const PositionColorTriangle& _rhs ) const
{
	return ( m_vertA != _rhs.m_vertA || m_vertB != _rhs.m_vertB || m_vertC != _rhs.m_vertC ) &&
		( m_vertA != _rhs.m_vertB || m_vertB != _rhs.m_vertC || m_vertC != _rhs.m_vertA ) &&
		( m_vertA != _rhs.m_vertC || m_vertB != _rhs.m_vertA || m_vertC != _rhs.m_vertB ) &&
		( m_vertA != _rhs.m_vertC || m_vertB != _rhs.m_vertB || m_vertC != _rhs.m_vertA ) &&
		( m_vertA != _rhs.m_vertB || m_vertB != _rhs.m_vertA || m_vertC != _rhs.m_vertC ) &&
		( m_vertA != _rhs.m_vertA || m_vertB != _rhs.m_vertC || m_vertC != _rhs.m_vertB );
}
