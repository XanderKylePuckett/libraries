#pragma once

struct Vector2
{
	float64 x, y;
	static const Vector2 zero;
	static const Vector2 one;
	static const Vector2 x_axis;
	static const Vector2 y_axis;
	Vector2( void );
	Vector2( float64, float64 );
	Vector2( const Vector2& );
	Vector2& operator=( const Vector2& );
	Vector2( Vector2&& );
	Vector2& operator=( Vector2&& );
	~Vector2( void );
	static float64 dot( const Vector2&, const Vector2& );
	float64 magnitude( void ) const;
	float64 sqmagnitude( void ) const;
	Vector2& normalize( void );
	Vector2 direction( void ) const;
	float64 dot( const Vector2& ) const;
	Vector2& negate( void );
	Vector2 operator+( void ) const;
	Vector2 operator-( void ) const;
	Vector2 operator+( const Vector2& ) const;
	Vector2 operator-( const Vector2& ) const;
	float64 operator*( const Vector2& ) const;
	Vector2 operator*( float64 ) const;
	Vector2 operator/( float64 ) const;
	Vector2& operator+=( const Vector2& );
	Vector2& operator-=( const Vector2& );
	Vector2& operator*=( float64 );
	Vector2& operator/=( float64 );
	bool operator==( const Vector2& ) const;
	bool operator!=( const Vector2& ) const;
	static float64 SqDistance( const Vector2&, const Vector2& );
	static float64 Distance( const Vector2&, const Vector2& );
};

Vector2 operator*( float64, const Vector2& );
std::ostream& operator<<( std::ostream&, const Vector2& );