#include "stdafx.h"
#include "Quaternion.h"
#include "Math.h"

const Quaternion Quaternion::ZERO = Quaternion( 0.0, 0.0, 0.0, 0.0 );

Quaternion::Quaternion( void )
{
}
Quaternion::Quaternion( float64 _s ) :
	a( _s ), b( 0.0 ), c( 0.0 ), d( 0.0 )
{
}
Quaternion::Quaternion( const Vector3& _v ) :
	m_scalar( 0.0 ), m_vector( _v )
{
}
Quaternion::Quaternion( float64 _s, const Vector3& _v ) :
	m_scalar( _s ), m_vector( _v )
{
}
Quaternion::Quaternion( float64 _a, float64 _b, float64 _c, float64 _d ) :
	a( _a ), b( _b ), c( _c ), d( _d )
{
}
Quaternion::Quaternion( const Quaternion& _rhs ) :
	a( _rhs.a ), b( _rhs.b ), c( _rhs.c ), d( _rhs.d )
{
}
Quaternion::~Quaternion( void )
{

}
Quaternion& Quaternion::operator=( const Quaternion& _rhs )
{
	if ( this != &_rhs )
	{
		a = _rhs.a;
		b = _rhs.b;
		c = _rhs.c;
		d = _rhs.d;
	}
	return *this;
}
Quaternion Quaternion::Slerp( const Quaternion&, const Quaternion&, float64 )
{
	// TODO
	return Quaternion();
}
Quaternion Quaternion::operator+( void ) const
{
	return *this;
}
Quaternion Quaternion::operator-( void ) const
{
	return Quaternion( -a, -b, -c, -d );
}
Quaternion Quaternion::operator+( const Quaternion& _rhs ) const
{
	return Quaternion( a + _rhs.a, b + _rhs.b, c + _rhs.c, d + _rhs.d );
}
Quaternion Quaternion::operator-( const Quaternion& _rhs ) const
{
	return Quaternion( a - _rhs.a, b - _rhs.b, c - _rhs.c, d - _rhs.d );
}
Quaternion Quaternion::operator+( const Vector3& _v ) const
{
	return Quaternion( m_scalar, m_vector + _v );
}
Quaternion Quaternion::operator-( const Vector3& _v ) const
{
	return Quaternion( m_scalar, m_vector - _v );
}
Quaternion Quaternion::operator+( float64 _s ) const
{
	return Quaternion( m_scalar + _s, m_vector );
}
Quaternion Quaternion::operator-( float64 _s ) const
{
	return Quaternion( m_scalar - _s, m_vector );
}
Quaternion Quaternion::operator*( const Quaternion& _rhs ) const
{
	return Quaternion
	(
		a * _rhs.a - b * _rhs.b - c * _rhs.c - d * _rhs.d,
		a * _rhs.b + b * _rhs.a + c * _rhs.d - d * _rhs.c,
		a * _rhs.c - b * _rhs.d + c * _rhs.a + d * _rhs.b,
		a * _rhs.d + b * _rhs.c - c * _rhs.b + d * _rhs.a
	);
}
Quaternion Quaternion::operator/( const Quaternion& _rhs ) const
{
	return ( *this ) * _rhs.GetInverse();
}
Quaternion Quaternion::operator*( float64 _rhs ) const
{
	return Quaternion( a * _rhs, b * _rhs, c * _rhs, d * _rhs );
}
Quaternion Quaternion::operator/( float64 _rhs ) const
{
	_rhs = 1.0 / _rhs;
	return Quaternion( a * _rhs, b * _rhs, c * _rhs, d * _rhs );
}
Quaternion& Quaternion::operator+=( const Quaternion& _rhs )
{
	a += _rhs.a;
	b += _rhs.b;
	c += _rhs.c;
	d += _rhs.d;
	return *this;
}
Quaternion& Quaternion::operator-=( const Quaternion& _rhs )
{
	a -= _rhs.a;
	b -= _rhs.b;
	c -= _rhs.c;
	d -= _rhs.d;
	return *this;
}
Quaternion& Quaternion::operator+=( float64 _s )
{
	m_scalar += _s;
	return *this;
}
Quaternion& Quaternion::operator-=( float64 _s )
{
	m_scalar -= _s;
	return *this;
}
Quaternion& Quaternion::operator+=( const Vector3& _v )
{
	m_vector += _v;
	return *this;
}
Quaternion& Quaternion::operator-=( const Vector3& _v )
{
	m_vector -= _v;
	return *this;
}
Quaternion& Quaternion::operator*=( const Quaternion& _rhs )
{
	return *this = ( *this ) * _rhs;
}
Quaternion& Quaternion::operator/=( const Quaternion& _rhs )
{
	return *this = ( *this ) * _rhs.GetInverse();
}
Quaternion& Quaternion::operator*=( float64 _rhs )
{
	a *= _rhs;
	b *= _rhs;
	c *= _rhs;
	d *= _rhs;
	return *this;
}
Quaternion& Quaternion::operator/=( float64 _rhs )
{
	_rhs = 1.0 / _rhs;
	a *= _rhs;
	b *= _rhs;
	c *= _rhs;
	d *= _rhs;
	return *this;
}
Quaternion Quaternion::GetConjugate( void ) const
{
	return Quaternion( a, -b, -c, -d );
}
float64 Quaternion::GetNorm( void ) const
{
	return Math::Sqrt( a * a + b * b + c * c + d * d );
}
float64 Quaternion::GetNormSq( void ) const
{
	return a * a + b * b + c * c + d * d;
}
Quaternion Quaternion::GetInverse( void ) const
{
	return GetConjugate() / GetNormSq();
}
Quaternion Quaternion::GetConjugate( const Quaternion& _q )
{
	return Quaternion( _q.m_scalar, -_q.m_vector );
}
float64 Quaternion::GetNorm( const Quaternion& _q )
{
	return _q.GetNorm();
}
float64 Quaternion::GetNormSq( const Quaternion& _q )
{
	return _q.GetNormSq();
}
Quaternion Quaternion::GetInverse( const Quaternion& _q )
{
	return _q.GetInverse();
}
Quaternion& Quaternion::SetToConjugate( void )
{
	m_vector = -m_vector;
	return *this;
}
Quaternion& Quaternion::SetToInverse( void )
{
	return *this = GetInverse();
}
Quaternion& Quaternion::SetToConjugate( Quaternion& _q )
{
	return _q.SetToConjugate();
}
Quaternion& Quaternion::SetToInverse( Quaternion& _q )
{
	return _q.SetToInverse();
}
bool Quaternion::operator==( const Quaternion& _rhs ) const
{
	return a == _rhs.a && b == _rhs.b && c == _rhs.c && d == _rhs.d;
}
bool Quaternion::operator!=( const Quaternion& _rhs ) const
{
	return a != _rhs.a || b != _rhs.b || c != _rhs.c || d != _rhs.d;
}
Matrix3x3 Quaternion::GetRotationMatrix3x3( void ) const
{
	// TODO
	return Matrix3x3();
}
Matrix4x4 Quaternion::GetRotationMatrix4x4( void ) const
{
	// TODO
	return Matrix4x4();
}
Matrix3x3 Quaternion::GetRotationMatrix3x3( const Quaternion& )
{
	// TODO
	return Matrix3x3();
}
Matrix4x4 Quaternion::GetRotationMatrix4x4( const Quaternion& )
{
	// TODO
	return Matrix4x4();
}
Quaternion Quaternion::GetQuaternionFromMatrix( const Matrix3x3& )
{
	// TODO
	return Quaternion();
}
Quaternion Quaternion::GetQuaternionFromMatrix( const Matrix4x4& )
{
	// TODO
	return Quaternion();
}
Quaternion Quaternion::AxisAngleRotation( const Vector3&, float64 )
{
	// TODO
	return Quaternion();
}
Quaternion operator+( float64 _s, const Quaternion& _q )
{
	return Quaternion( _s + _q.m_scalar, _q.m_vector );
}
Quaternion operator-( float64 _s, const Quaternion& _q )
{
	return Quaternion( _s - _q.m_scalar, -_q.m_vector );
}
Quaternion operator+( const Vector3& _v, const Quaternion& _q )
{
	return Quaternion( _q.m_scalar, _v + _q.m_vector );
}
Quaternion operator-( const Vector3& _v, const Quaternion& _q )
{
	return Quaternion( -_q.m_scalar, _v - _q.m_vector );
}
Quaternion operator+( const Vector3& _v, float64 _s )
{
	return Quaternion( _s, _v );
}
Quaternion operator-( const Vector3& _v, float64 _s )
{
	return Quaternion( -_s, _v );
}
Quaternion operator+( float64 _s, const Vector3& _v )
{
	return Quaternion( _s, _v );
}
Quaternion operator-( float64 _s, const Vector3& _v )
{
	return Quaternion( _s, -_v );
}
Quaternion operator*( float64 _lhs, const Quaternion& _rhs )
{
	return Quaternion( _lhs * _rhs.a, _lhs * _rhs.b, _lhs * _rhs.c, _lhs * _rhs.d );
}
Quaternion operator/( float64 _lhs, const Quaternion& _rhs )
{
	return _lhs * _rhs.GetInverse();
}
std::ostream& operator<<( std::ostream& _os, const Quaternion& )
{
	// TODO

	return _os;
}