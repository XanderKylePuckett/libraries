#include "stdafx.h"

#define PLEB for(j=0u;j<0x0c1dcf5eu;++j){t=primeArr64[j];if(t*t>q)break;if(0ull==q%t){isPrime=false;break;}}if(isPrime)outFile.write(pQ,8u);else isPrime=true

void PrimeNumberTableMain( void )
{
	std::ifstream inFile;
	inFile.open( "PrimeNumbers\\PrimeNumbers64.bin", std::ios_base::in | std::ios_base::binary );
	if ( !inFile.is_open() )
		return;
	uint64* primeArr64 = new uint64[ 0x0c1dcf5eu ];
	inFile.read( ( char* )primeArr64, 0x60ee7ae8u );
	inFile.close();
	primeArr64[ 0x0c1dcf5du ] = 0x000000010000000full;
	std::ofstream outFile;
	outFile.open( "PrimeNumbers\\PrimeNumbers64out.bin",
				  std::ios_base::out | std::ios_base::trunc | std::ios_base::binary );
	if ( !outFile.is_open() )
	{
		delete[ ] primeArr64;
		return;
	}
	outFile.write( ( char* )primeArr64, 0x60ee7af0u );
	outFile.close();
	outFile.open( "PrimeNumbers\\PrimeNumbers64out.bin",
				  std::ios_base::out | std::ios_base::app | std::ios_base::binary );
	if ( !outFile.is_open() )
	{
		delete[ ] primeArr64;
		return;
	}
	uint64 q, t;
	bool isPrime = true;
	uint32 j;
	const char* const pQ = ( const char* )( &q );
	for ( uint64 i = 0x000000000888888aull; i < 0x0888888888888888u; ++i )
	{
		q = i * 30ull + 1ull; PLEB;
		q += 6ull; PLEB;
		q += 4ull; PLEB;
		q += 2ull; PLEB;
		q += 4ull; PLEB;
		q += 2ull; PLEB;
		q += 4ull; PLEB;
		q += 6ull; PLEB;
	}
	q = 0xfffffffffffffff1ull; PLEB;
	q = 0xfffffffffffffff7ull; PLEB;
	q = 0xfffffffffffffffbull; PLEB;
	q = 0xfffffffffffffffdull; PLEB;
	outFile.close();
	delete[ ] primeArr64;
}