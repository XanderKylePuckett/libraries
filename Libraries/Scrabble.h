#pragma once
#include "WordDictionary.h"

class Scrabble
{
private:
	WordDictionary m_dict;
	int32* m_tileBag;
	char* m_board;
public:
	Scrabble( void );
	Scrabble( const Scrabble& );
	Scrabble( Scrabble&& );
	Scrabble& operator=( const Scrabble& );
	Scrabble& operator=( Scrabble&& );
	~Scrabble( void );
	void ImportDictionary( const char* const );
	void ExportDictionary( const char* const ) const;
	void PrintWordsFromLetters( const String& letters ) const;
};