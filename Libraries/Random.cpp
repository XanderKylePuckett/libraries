#include "stdafx.h"
#include "Random.h"
#include <chrono>

Random g_random = Random();

Random::Random( void ) :
	mt_rand( ( uint64 )( std::chrono::high_resolution_clock::now().time_since_epoch().count() ) )
{
}
Random::Random( uint64 _seed ) :
	mt_rand( _seed )
{
}
Random::Random( const Random& _rhs ) :
	mt_rand( _rhs.mt_rand )
{
}
Random::Random( Random&& _rhs ) :
	mt_rand( std::move( _rhs.mt_rand ) )
{
}
Random& Random::operator=( const Random& _rhs )
{
	if ( this != &_rhs )
	{
		mt_rand = _rhs.mt_rand;
	}
	return *this;
}
Random& Random::operator=( Random&& _rhs )
{
	if ( this != &_rhs )
	{
		mt_rand = std::move( _rhs.mt_rand );
	}
	return *this;
}
Random::~Random( void )
{
}
uint64 Random::GetRandom( uint64 _min, uint64 _max )
{
	return std::uniform_int_distribution<uint64>( _min, _max )( mt_rand );
}
uint32 Random::GetRandom( uint32 _min, uint32 _max )
{
	return std::uniform_int_distribution<uint32>( _min, _max )( mt_rand );
}
uint16 Random::GetRandom( uint16 _min, uint16 _max )
{
	return std::uniform_int_distribution<uint16>( _min, _max )( mt_rand );
}
uint8 Random::GetRandom( uint8 _min, uint8 _max )
{
	return ( uint8 )std::uniform_int_distribution<int16>( ( uint16 )_min, ( uint16 )_max )( mt_rand );
}
int64 Random::GetRandom( int64 _min, int64 _max )
{
	return std::uniform_int_distribution<int64>( _min, _max )( mt_rand );
}
int32 Random::GetRandom( int32 _min, int32 _max )
{
	return std::uniform_int_distribution<int32>( _min, _max )( mt_rand );
}
int16 Random::GetRandom( int16 _min, int16 _max )
{
	return std::uniform_int_distribution<int16>( _min, _max )( mt_rand );
}
int8 Random::GetRandom( int8 _min, int8 _max )
{
	return ( int8 )std::uniform_int_distribution<int16>( ( int16 )_min, ( int16 )_max )( mt_rand );
}
float32 Random::GetRandom( float32 _min, float32 _max )
{
	return std::uniform_real_distribution<float32>( _min, _max )( mt_rand );
}
float64 Random::GetRandom( float64 _min, float64 _max )
{
	return std::uniform_real_distribution<float64>( _min, _max )( mt_rand );
}
uint64 Random::GetRandomUint64( void )
{
	return GetRandom( 0x0000000000000000ui64, 0xffffffffffffffffui64 );
}
uint32 Random::GetRandomUint32( void )
{
	return GetRandom( 0x00000000ui32, 0xffffffffui32 );
}
uint16 Random::GetRandomUint16( void )
{
	return GetRandom( 0x0000ui16, 0xffffui16 );
}
uint8 Random::GetRandomUint8( void )
{
	return GetRandom( 0x00ui8, 0xffui8 );
}
int64 Random::GetRandomInt64( void )
{
	return GetRandom( 0x8000000000000000i64, 0x7fffffffffffffffi64 );
}
int32 Random::GetRandomInt32( void )
{
	return GetRandom( 0x80000000i32, 0x7fffffffi32 );
}
int16 Random::GetRandomInt16( void )
{
	return GetRandom( 0x8000i16, 0x7fffi16 );
}
int8 Random::GetRandomInt8( void )
{
	return GetRandom( 0x80i8, 0x7fi8 );
}
float32 Random::GetRandomFloat32( void )
{
	return GetRandom( -FLT_MAX, FLT_MAX );
}
float64 Random::GetRandomFloat64( void )
{
	return GetRandom( -DBL_MAX, DBL_MAX );
}
float32 Random::GetRandomFloat32_01( void )
{
	return GetRandom( 0.0f, 1.0f );
}
float64 Random::GetRandomFloat64_01( void )
{
	return GetRandom( 0.0, 1.0 );
}