#pragma once
#include "AsciiFont.h"

class TextRenderer
{
private:
	AsciiFont m_font;
public:
	TextRenderer( void );
	TextRenderer( const AsciiFont& );
	TextRenderer( const TextRenderer& );
	TextRenderer( TextRenderer&& );
	TextRenderer& operator=( const TextRenderer& );
	TextRenderer& operator=( TextRenderer&& );
	~TextRenderer( void );
	const AsciiFont& GetFont( void ) const;
	void SetFont( const AsciiFont& );
	Texture2D MakeTextBlock( const std::string& _text, uint32 _texWid, uint32 _texHei ) const;
};
