#include "stdafx.h"
#include "BitFile.h"

static constexpr inline bool is_open( const BitFileMode& _bfm )
{
	return ( BITFILE_CLOSED != _bfm );
}
static constexpr inline bool is_not_out( const BitFileMode& _bfm )
{
	return ( BITFILE_OUT != _bfm );
}
static constexpr inline bool is_not_in( const BitFileMode& _bfm )
{
	return ( BITFILE_IN != _bfm );
}

BitFile::BitFile( void ) :
	m_mode( BITFILE_CLOSED )
{
}
BitFile::BitFile( const std::string& _filename, BitFileMode _mode ) :
	m_mode( _mode )
{
	switch ( m_mode )
	{
		case BITFILE_IN:
			m_file.open( _filename, std::ios_base::in | std::ios_base::binary );
			if ( m_file.is_open() )
				m_bit = 0ui8;
			else
				m_mode = BITFILE_CLOSED;
			break;
		case BITFILE_OUT:
			m_file.open( _filename, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary );
			if ( m_file.is_open() )
			{
				m_bit = 1ui8;
				m_byte = 0ui8;
			}
			else
				m_mode = BITFILE_CLOSED;
			break;
		case BITFILE_CLOSED:
		default:
			break;
	}
}
BitFile::BitFile( BitFile&& _rhs ) :
	m_file( std::move( _rhs.m_file ) ),
	m_mode( std::move( _rhs.m_mode ) ),
	m_byte( std::move( _rhs.m_byte ) ),
	m_bit( std::move( _rhs.m_bit ) )
{
	_rhs.m_mode = BITFILE_CLOSED;
}
BitFile& BitFile::operator=( BitFile&& _rhs )
{
	if ( this != &_rhs )
	{
		Close();
		m_file = std::move( _rhs.m_file );
		m_mode = std::move( _rhs.m_mode );
		m_byte = std::move( _rhs.m_byte );
		m_bit = std::move( _rhs.m_bit );
		_rhs.m_mode = BITFILE_CLOSED;
	}
	return *this;
}
BitFile::~BitFile( void )
{
	Close();
}
BitFile& BitFile::OpenOut( const std::string& _filename )
{
	Close();
	m_file.open( _filename, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary );
	if ( m_file.is_open() )
	{
		m_mode = BITFILE_OUT;
		m_bit = 1ui8;
		m_byte = 0ui8;
	}
	else
		m_mode = BITFILE_CLOSED;
	return *this;
}
BitFile& BitFile::OpenIn( const std::string& _filename )
{
	Close();
	m_file.open( _filename, std::ios_base::in | std::ios_base::binary );
	m_mode = m_file.is_open() ? BITFILE_IN : BITFILE_CLOSED;
	m_bit = 0ui8;
	return *this;
}
bool BitFile::IsOpen( void ) const
{
	return is_open( m_mode );
}
void BitFile::Close( void )
{
	if ( is_open( m_mode ) )
	{
		if ( is_not_in( m_mode ) )
			if ( 1ui8 != m_bit )
				m_file.write( ( const char* )( &m_byte ), 1u );
		m_file.close();
		m_mode = BITFILE_CLOSED;
	}
}
void BitFile::WriteBit( bool _bit )
{
	if ( is_not_out( m_mode ) )
		return;
	if ( _bit )
		m_byte |= m_bit;
	m_bit <<= 1ui8;
	if ( 0ui8 == m_bit )
	{
		m_file.write( ( const char* )( &m_byte ), 1u );
		m_bit = 1ui8;
		m_byte = 0ui8;
	}
}
bool BitFile::ReadBit( void )
{
	if ( is_not_in( m_mode ) )
		return false;
	if ( 0ui8 == m_bit )
	{
		m_file.read( ( char* )( &m_byte ), 1u );
		m_bit = 1ui8;
	}
	const bool out_ = 0ui8 != ( m_byte & m_bit );
	m_bit <<= 1u;
	return out_;
}
BitFile& BitFile::operator+=( bool _bit )
{
	WriteBit( _bit );
	return *this;
}
void BitFile::WriteData( const char* _data, uint32 _size )
{
	if ( is_not_out( m_mode ) )
		return;
	m_file.write( _data, _size );
}
void BitFile::ReadData( char* _data, uint32 _size )
{
	if ( is_not_in( m_mode ) )
		return;
	m_file.read( _data, _size );
}