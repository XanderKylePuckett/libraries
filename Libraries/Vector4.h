#pragma once

struct Vector4
{
	float64 x, y, z, w;
	static const Vector4 ZERO;
	static const Vector4 ONE;
	Vector4( void );
	Vector4( float64, float64, float64, float64 = 1.0 );
	Vector4( const Vector4& );
	Vector4& operator=( const Vector4& );
	Vector4( Vector4&& );
	Vector4& operator=( Vector4&& );
	~Vector4( void );
	static float64 dot( const Vector4&, const Vector4& );
	float64 dot( const Vector4& ) const;
	Vector4 operator+( void ) const;
	Vector4 operator-( void ) const;
	Vector4 operator+( const Vector4& ) const;
	Vector4 operator-( const Vector4& ) const;
	float64 operator*( const Vector4& ) const;
	Vector4 operator*( float64 ) const;
	Vector4 operator/( float64 ) const;
	Vector4& operator+=( const Vector4& );
	Vector4& operator-=( const Vector4& );
	Vector4& operator*=( float64 );
	Vector4& operator/=( float64 );
	bool operator==( const Vector4& ) const;
	bool operator!=( const Vector4& ) const;
};

Vector4 operator*( float64, const Vector4& );
std::ostream& operator<<( std::ostream&, const Vector4& );