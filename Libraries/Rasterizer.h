#pragma once
#include "Texture2D.h"
#include "Geometry.h"

void DrawLine( Texture2D&, const Line& );
void DrawRasterPoint( Texture2D&, const RasterPoint&, const Color& = Color::WHITE );
void DrawRasterLine( Texture2D&, const RasterLine&, const Color& = Color::WHITE );
void DrawRasterLine( Texture2D&, const RasterLine&, const Color&, const Color& );

void DrawLine( const Line& );
void DrawRasterPoint( const RasterPoint&, const Color& = Color::WHITE );
void DrawRasterLine( const RasterLine&, const Color& = Color::WHITE );
void DrawRasterLine( const RasterLine&, const Color&, const Color& );