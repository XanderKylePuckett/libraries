#pragma once
#include "DynArray.h"

template <typename T>
class HashTable
{
private:
	DynArray<T>* m_table;
	uint32 m_tableSize;
	uint32( *m_hashFunction )( const T& );
	HashTable( void );
public:
	HashTable( uint32( *_hashFunc )( const T& ), uint32 _size );
	HashTable( uint32 _size, uint32( *_hashFunc )( const T& ) );
	HashTable( const HashTable<T>& _rhs );
	HashTable( HashTable<T>&& _rhs );
	HashTable<T>& operator=( const HashTable<T>& _rhs );
	HashTable<T>& operator=( HashTable<T>&& _rhs );
	~HashTable( void );
	void Add( const T& _obj );
	bool Has( const T& _obj ) const;
	void Clear( void );
	HashTable<T>& OptimizeSpace( void );
	const DynArray<T>* GetTable( uint32* = nullptr ) const;
	DynArray<T>* GetTable_M( uint32* = nullptr );
};

template <typename T>
HashTable<T>::HashTable( uint32( *_hashFunc )( const T& ), uint32 _size ) :
	m_hashFunction( _hashFunc ), m_tableSize( _size )
{
	m_table = new DynArray<T>[ m_tableSize ];
}
template <typename T>
HashTable<T>::HashTable( uint32 _size, uint32( *_hashFunc )( const T& ) ) :
	m_hashFunction( _hashFunc ), m_tableSize( _size )
{
	m_table = new DynArray<T>[ m_tableSize ];
}
template <typename T>
HashTable<T>::HashTable( const HashTable<T>& _rhs ) :
	m_hashFunction( _rhs.m_hashFunction ), m_tableSize( _rhs.m_tableSize )
{
	m_table = new DynArray<T>[ m_tableSize ];
	for ( uint32 i = 0u; i < m_tableSize; ++i )
		m_table[ i ] = _rhs.m_table[ i ];
}
template <typename T>
HashTable<T>::HashTable( HashTable<T>&& _rhs ) :
	m_hashFunction( std::move( _rhs.m_hashFunction ) ),
	m_tableSize( std::move( _rhs.m_tableSize ) ),
	m_table( std::move( _rhs.m_table ) )
{
	_rhs.m_table = nullptr;
}
template <typename T>
HashTable<T>& HashTable<T>::operator=( const HashTable<T>& _rhs )
{
	if ( this != &_rhs )
	{
		delete[ ] m_table;
		m_hashFunction = _rhs.m_hashFunction;
		m_tableSize = _rhs.m_tableSize;
		m_table = new DynArray<T>[ m_tableSize ];
		for ( uint32 i = 0u; i < m_tableSize; ++i )
			m_table[ i ] = _rhs.m_table[ i ];
	}
	return *this;
}
template <typename T>
HashTable<T>& HashTable<T>::operator=( HashTable<T>&& _rhs )
{
	if ( this != &_rhs )
	{
		delete[ ] m_table;
		m_hashFunction = std::move( _rhs.m_hashFunction );
		m_tableSize = std::move( _rhs.m_tableSize );
		m_table = std::move( _rhs.m_table );
		_rhs.m_table = nullptr;
	}
	return *this;
}
template <typename T>
HashTable<T>::~HashTable( void )
{
	delete[ ] m_table;
}
template <typename T>
void HashTable<T>::Add( const T& _obj )
{
	m_table[ m_hashFunction( _obj ) ].AddItem( _obj );
}
template <typename T>
bool HashTable<T>::Has( const T& _obj ) const
{
	return m_table[ m_hashFunction( _obj ) ].Has( _obj );
}
template <typename T>
void HashTable<T>::Clear( void )
{
	for ( uint32 i = 0u; i < m_tableSize; ++i )
		m_table[ i ].Clear();
}
template <typename T>
HashTable<T>& HashTable<T>::OptimizeSpace( void )
{
	for ( uint32 i = 0u; i < m_tableSize; ++i )
		m_table[ i ].OptimizeSpace();
	return *this;
}
template <typename T>
const DynArray<T>* HashTable<T>::GetTable( uint32* _size ) const
{
	if ( nullptr != _size )
		*_size = m_tableSize;
	return m_table;
}
template <typename T>
DynArray<T>* HashTable<T>::GetTable_M( uint32* _size )
{
	if ( nullptr != _size )
		*_size = m_tableSize;
	return m_table;
}