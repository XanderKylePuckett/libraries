#include "stdafx.h"
#include "Math.h"
#include <math.h>

#define CONST_DEFINE(X,Y,Z)static const uint64 bits##Z=0##X##ui64;static const uint32 bitsf##Z=0##Y##ui32;const float64&Const##Z=*((float64*)&bits##Z);const float32&ConstF##Z=*((float32*)&bitsf##Z)
CONST_DEFINE( x400921fb54442d18, x40490fdb, _pi );
CONST_DEFINE( x3ff921fb54442d18, x3fc90fdb, _piDiv2 );
CONST_DEFINE( x3fe921fb54442d18, x3f490fdb, _piDiv4 );
CONST_DEFINE( x3f91df46a2529d39, x3c8efa35, _piDiv180 );
CONST_DEFINE( x404ca5dc1a63c1f8, x42652ee1, _180DivPi );
CONST_DEFINE( x3fd45f306dc9c883, x3ea2f983, _1DivPi );
CONST_DEFINE( x401921fb54442d18, x40c90fdb, _2pi );
CONST_DEFINE( x4005bf0a8b145769, x402df854, _e );
CONST_DEFINE( x3ff6a09e667f3bcd, x3fb504f3, _sqrt2 );
CONST_DEFINE( x3fe6a09e667f3bcc, x3f3504f3, _sqrt2Div2 );
CONST_DEFINE( x3ffbb67ae8584caa, x3fddb3d7, _sqrt3 );
CONST_DEFINE( x3febb67ae8584caa, x3f5db3d7, _sqrt3Div2 );
CONST_DEFINE( x3fe279a74590331d, x3f13cd3a, _sqrt3Div3 );
CONST_DEFINE( x3fc5555555555555, x3e2aaaab, _1Div6 );
CONST_DEFINE( x3fd5555555555555, x3eaaaaab, _1Div3 );
CONST_DEFINE( x3fe5555555555555, x3f2aaaab, _2Div3 );
CONST_DEFINE( x3ff9e3779b97f4a8, x3fcf1bbd, _goldenRatio );
CONST_DEFINE( x3fefffffffffffff, x3f7fffff, _wau );
CONST_DEFINE( x3ff0f38f92d97963, x3f879c7d, _12thRoot2 );
CONST_DEFINE( x4200bd99415af206, x5005ecca, _coulombs );
CONST_DEFINE( x3dd25877ed2e511d, x2e92c3bf, _gravitational );
CONST_DEFINE( x3da37876f14ded31, x2d1bc3b8, _permittivity );
CONST_DEFINE( x3eb515370f99f6cb, x35a8a9b8, _permeability );
CONST_DEFINE( x41b1de784a000000, x4d8ef3c2, _lightSpeed );
CONST_DEFINE( x4373f4d4eacdd756, x5b9fa6a7, _lightSpeedSquared );
CONST_DEFINE( x3c07a4da25cda639, x203d26d1, _elementaryCharge );
CONST_DEFINE( x44dfe185d3061ec5, x66ff0c2f, _avogadro );
#undef CONST_DEFINE

int8 Math::Abs( int8 _x )
{
	return _x < 0i8 ? -_x : _x;
}
int16 Math::Abs( int16 _x )
{
	return _x < 0i16 ? -_x : _x;
}
int32 Math::Abs( int32 _x )
{
	return _x < 0 ? -_x : _x;
}
int64 Math::Abs( int64 _x )
{
	return _x < 0ll ? -_x : _x;
}
float32 Math::Abs( float32 _x )
{
	return _x < 0.0f ? -_x : _x;
}
float64 Math::Abs( float64 _x )
{
	return _x < 0.0 ? -_x : _x;
}
int8 Math::Min( int8 _a, int8 _b )
{
	return _b < _a ? _b : _a;
}
int16 Math::Min( int16 _a, int16 _b )
{
	return _b < _a ? _b : _a;
}
int32 Math::Min( int32 _a, int32 _b )
{
	return _b < _a ? _b : _a;
}
int64 Math::Min( int64 _a, int64 _b )
{
	return _b < _a ? _b : _a;
}
uint8 Math::Min( uint8 _a, uint8 _b )
{
	return _b < _a ? _b : _a;
}
uint16 Math::Min( uint16 _a, uint16 _b )
{
	return _b < _a ? _b : _a;
}
uint32 Math::Min( uint32 _a, uint32 _b )
{
	return _b < _a ? _b : _a;
}
uint64 Math::Min( uint64 _a, uint64 _b )
{
	return _b < _a ? _b : _a;
}
float32 Math::Min( float32 _a, float32 _b )
{
	return _b < _a ? _b : _a;
}
float64 Math::Min( float64 _a, float64 _b )
{
	return _b < _a ? _b : _a;
}
int8 Math::Max( int8 _a, int8 _b )
{
	return _a < _b ? _b : _a;
}
int16 Math::Max( int16 _a, int16 _b )
{
	return _a < _b ? _b : _a;
}
int32 Math::Max( int32 _a, int32 _b )
{
	return _a < _b ? _b : _a;
}
int64 Math::Max( int64 _a, int64 _b )
{
	return _a < _b ? _b : _a;
}
uint8 Math::Max( uint8 _a, uint8 _b )
{
	return _a < _b ? _b : _a;
}
uint16 Math::Max( uint16 _a, uint16 _b )
{
	return _a < _b ? _b : _a;
}
uint32 Math::Max( uint32 _a, uint32 _b )
{
	return _a < _b ? _b : _a;
}
uint64 Math::Max( uint64 _a, uint64 _b )
{
	return _a < _b ? _b : _a;
}
float32 Math::Max( float32 _a, float32 _b )
{
	return _a < _b ? _b : _a;
}
float64 Math::Max( float64 _a, float64 _b )
{
	return _a < _b ? _b : _a;
}
int8 Math::Min( int8 _a, int8 _b, int8 _c )
{
	return ( _a < _b ) ? ( ( _a < _c ) ? _a : _c ) : ( ( _b < _c ) ? _b : _c );
}
int16 Math::Min( int16 _a, int16 _b, int16 _c )
{
	return ( _a < _b ) ? ( ( _a < _c ) ? _a : _c ) : ( ( _b < _c ) ? _b : _c );
}
int32 Math::Min( int32 _a, int32 _b, int32 _c )
{
	return ( _a < _b ) ? ( ( _a < _c ) ? _a : _c ) : ( ( _b < _c ) ? _b : _c );
}
int64 Math::Min( int64 _a, int64 _b, int64 _c )
{
	return ( _a < _b ) ? ( ( _a < _c ) ? _a : _c ) : ( ( _b < _c ) ? _b : _c );
}
uint8 Math::Min( uint8 _a, uint8 _b, uint8 _c )
{
	return ( _a < _b ) ? ( ( _a < _c ) ? _a : _c ) : ( ( _b < _c ) ? _b : _c );
}
uint16 Math::Min( uint16 _a, uint16 _b, uint16 _c )
{
	return ( _a < _b ) ? ( ( _a < _c ) ? _a : _c ) : ( ( _b < _c ) ? _b : _c );
}
uint32 Math::Min( uint32 _a, uint32 _b, uint32 _c )
{
	return ( _a < _b ) ? ( ( _a < _c ) ? _a : _c ) : ( ( _b < _c ) ? _b : _c );
}
uint64 Math::Min( uint64 _a, uint64 _b, uint64 _c )
{
	return ( _a < _b ) ? ( ( _a < _c ) ? _a : _c ) : ( ( _b < _c ) ? _b : _c );
}
float32 Math::Min( float32 _a, float32 _b, float32 _c )
{
	return ( _a < _b ) ? ( ( _a < _c ) ? _a : _c ) : ( ( _b < _c ) ? _b : _c );
}
float64 Math::Min( float64 _a, float64 _b, float64 _c )
{
	return ( _a < _b ) ? ( ( _a < _c ) ? _a : _c ) : ( ( _b < _c ) ? _b : _c );
}
int8 Math::Max( int8 _a, int8 _b, int8 _c )
{
	return ( _b < _a ) ? ( ( _c < _a ) ? _a : _c ) : ( ( _c < _b ) ? _b : _c );
}
int16 Math::Max( int16 _a, int16 _b, int16 _c )
{
	return ( _b < _a ) ? ( ( _c < _a ) ? _a : _c ) : ( ( _c < _b ) ? _b : _c );
}
int32 Math::Max( int32 _a, int32 _b, int32 _c )
{
	return ( _b < _a ) ? ( ( _c < _a ) ? _a : _c ) : ( ( _c < _b ) ? _b : _c );
}
int64 Math::Max( int64 _a, int64 _b, int64 _c )
{
	return ( _b < _a ) ? ( ( _c < _a ) ? _a : _c ) : ( ( _c < _b ) ? _b : _c );
}
uint8 Math::Max( uint8 _a, uint8 _b, uint8 _c )
{
	return ( _b < _a ) ? ( ( _c < _a ) ? _a : _c ) : ( ( _c < _b ) ? _b : _c );
}
uint16 Math::Max( uint16 _a, uint16 _b, uint16 _c )
{
	return ( _b < _a ) ? ( ( _c < _a ) ? _a : _c ) : ( ( _c < _b ) ? _b : _c );
}
uint32 Math::Max( uint32 _a, uint32 _b, uint32 _c )
{
	return ( _b < _a ) ? ( ( _c < _a ) ? _a : _c ) : ( ( _c < _b ) ? _b : _c );
}
uint64 Math::Max( uint64 _a, uint64 _b, uint64 _c )
{
	return ( _b < _a ) ? ( ( _c < _a ) ? _a : _c ) : ( ( _c < _b ) ? _b : _c );
}
float32 Math::Max( float32 _a, float32 _b, float32 _c )
{
	return ( _b < _a ) ? ( ( _c < _a ) ? _a : _c ) : ( ( _c < _b ) ? _b : _c );
}
float64 Math::Max( float64 _a, float64 _b, float64 _c )
{
	return ( _b < _a ) ? ( ( _c < _a ) ? _a : _c ) : ( ( _c < _b ) ? _b : _c );
}
int8 Math::Min( const int8* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0i8;
	int8 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
int16 Math::Min( const int16* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0i16;
	int16 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
int32 Math::Min( const int32* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0;
	int32 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
int64 Math::Min( const int64* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0ll;
	int64 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
uint8 Math::Min( const uint8* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0ui8;
	uint8 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
uint16 Math::Min( const uint16* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0ui16;
	uint16 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
uint32 Math::Min( const uint32* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0u;
	uint32 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
uint64 Math::Min( const uint64* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0ull;
	uint64 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
float32 Math::Min( const float32* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0.0f;
	float32 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
float64 Math::Min( const float64* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0.0;
	float64 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
int8 Math::Max( const int8* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0i8;
	int8 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
int16 Math::Max( const int16* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0i16;
	int16 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
int32 Math::Max( const int32* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0;
	int32 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
int64 Math::Max( const int64* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0ll;
	int64 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
uint8 Math::Max( const uint8* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0ui8;
	uint8 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
uint16 Math::Max( const uint16* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0ui16;
	uint16 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
uint32 Math::Max( const uint32* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0u;
	uint32 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
uint64 Math::Max( const uint64* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0ull;
	uint64 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
float32 Math::Max( const float32* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0.0f;
	float32 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
float64 Math::Max( const float64* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0.0;
	float64 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		if ( _arr[ i ] < ret_ )
			ret_ = _arr[ i ];
	return ret_;
}
int8 Math::Avg( const int8* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0i8;
	int64 ret_ = ( int64 )_arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		ret_ += ( int64 )_arr[ i ];
	return ( int8 )( ret_ / ( ( int64 )_siz ) );
}
int16 Math::Avg( const int16* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0i16;
	int64 ret_ = ( int64 )_arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		ret_ += ( int64 )_arr[ i ];
	return ( int16 )( ret_ / ( ( int64 )_siz ) );
}
int32 Math::Avg( const int32* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0;
	int64 ret_ = ( int64 )_arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		ret_ += ( int64 )_arr[ i ];
	return ( int32 )( ret_ / ( ( int64 )_siz ) );
}
int64 Math::Avg( const int64* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0ll;
	int64 ret_ = ( int64 )_arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		ret_ += ( int64 )_arr[ i ];
	return ( int64 )( ret_ / ( ( int64 )_siz ) );
}
uint8 Math::Avg( const uint8* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0ui8;
	int64 ret_ = ( int64 )_arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		ret_ += ( int64 )_arr[ i ];
	return ( uint8 )( ret_ / ( ( int64 )_siz ) );
}
uint16 Math::Avg( const uint16* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0ui16;
	int64 ret_ = ( int64 )_arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		ret_ += ( int64 )_arr[ i ];
	return ( uint16 )( ret_ / ( ( int64 )_siz ) );
}
uint32 Math::Avg( const uint32* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0u;
	int64 ret_ = ( int64 )_arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		ret_ += ( int64 )_arr[ i ];
	return ( uint32 )( ret_ / ( ( int64 )_siz ) );
}
uint64 Math::Avg( const uint64* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0ull;
	int64 ret_ = ( int64 )_arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		ret_ += ( int64 )_arr[ i ];
	return ( uint64 )( ret_ / ( ( int64 )_siz ) );
}
float32 Math::Avg( const float32* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0.0f;
	float64 ret_ = ( float64 )_arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		ret_ += ( float64 )_arr[ i ];
	return ( float32 )( ret_ / ( ( float64 )_siz ) );
}
float64 Math::Avg( const float64* _arr, uint32 _siz )
{
	if ( nullptr == _arr || 0u == _siz )
		return 0.0f;
	float64 ret_ = _arr[ 0u ];
	for ( uint32 i = 1u; i < _siz; ++i )
		ret_ += _arr[ i ];
	return ret_ / ( ( float64 )_siz );
}
int8 Math::Lerp( int8 _a, int8 _b, float32 _rat )
{
	return ( int8 )( ( float32 )_a + ( ( float32 )_b - ( float32 )_a ) * _rat );
}
int16 Math::Lerp( int16 _a, int16 _b, float32 _rat )
{
	return ( int16 )( ( float32 )_a + ( ( float32 )_b - ( float32 )_a ) * _rat );
}
int32 Math::Lerp( int32 _a, int32 _b, float32 _rat )
{
	return ( int32 )( ( float32 )_a + ( ( float32 )_b - ( float32 )_a ) * _rat );
}
int64 Math::Lerp( int64 _a, int64 _b, float32 _rat )
{
	return ( int64 )( ( float32 )_a + ( ( float32 )_b - ( float32 )_a ) * _rat );
}
uint8 Math::Lerp( uint8 _a, uint8 _b, float32 _rat )
{
	return ( uint8 )( ( float32 )_a + ( ( float32 )_b - ( float32 )_a ) * _rat );
}
uint16 Math::Lerp( uint16 _a, uint16 _b, float32 _rat )
{
	return ( uint16 )( ( float32 )_a + ( ( float32 )_b - ( float32 )_a ) * _rat );
}
uint32 Math::Lerp( uint32 _a, uint32 _b, float32 _rat )
{
	return ( uint32 )( ( float32 )_a + ( ( float32 )_b - ( float32 )_a ) * _rat );
}
uint64 Math::Lerp( uint64 _a, uint64 _b, float32 _rat )
{
	return ( uint64 )( ( float32 )_a + ( ( float32 )_b - ( float32 )_a ) * _rat );
}
float32 Math::Lerp( float32 _a, float32 _b, float32 _rat )
{
	return _a + ( _b - _a ) * _rat;
}
float64 Math::Lerp( float64 _a, float64 _b, float32 _rat )
{
	return _a + ( _b - _a ) * ( ( float64 )_rat );
}
int8 Math::Lerp( int8 _a, int8 _b, float64 _rat )
{
	return ( int8 )( ( float64 )_a + ( ( float64 )_b - ( float64 )_a ) * _rat );
}
int16 Math::Lerp( int16 _a, int16 _b, float64 _rat )
{
	return ( int16 )( ( float64 )_a + ( ( float64 )_b - ( float64 )_a ) * _rat );
}
int32 Math::Lerp( int32 _a, int32 _b, float64 _rat )
{
	return ( int32 )( ( float64 )_a + ( ( float64 )_b - ( float64 )_a ) * _rat );
}
int64 Math::Lerp( int64 _a, int64 _b, float64 _rat )
{
	return ( int64 )( ( float64 )_a + ( ( float64 )_b - ( float64 )_a ) * _rat );
}
uint8 Math::Lerp( uint8 _a, uint8 _b, float64 _rat )
{
	return ( uint8 )( ( float64 )_a + ( ( float64 )_b - ( float64 )_a ) * _rat );
}
uint16 Math::Lerp( uint16 _a, uint16 _b, float64 _rat )
{
	return ( uint16 )( ( float64 )_a + ( ( float64 )_b - ( float64 )_a ) * _rat );
}
uint32 Math::Lerp( uint32 _a, uint32 _b, float64 _rat )
{
	return ( uint32 )( ( float64 )_a + ( ( float64 )_b - ( float64 )_a ) * _rat );
}
uint64 Math::Lerp( uint64 _a, uint64 _b, float64 _rat )
{
	return ( uint64 )( ( float64 )_a + ( ( float64 )_b - ( float64 )_a ) * _rat );
}
float32 Math::Lerp( float32 _a, float32 _b, float64 _rat )
{
	return ( float32 )( ( float64 )_a + ( ( float64 )_b - ( float64 )_a ) * _rat );
}
float64 Math::Lerp( float64 _a, float64 _b, float64 _rat )
{
	return _a + ( _b - _a ) * _rat;
}
float32 Math::Sqrt( float32 _x )
{
	return ( float32 )sqrt( ( double )_x );
}
float64 Math::Sqrt( float64 _x )
{
	return sqrt( _x );
}
float32 Math::DegreesToRadians( float32 _deg )
{
	return _deg * ConstF_piDiv180;
}
float64 Math::DegreesToRadians( float64 _deg )
{
	return _deg * Const_piDiv180;
}
float32 Math::RadiansToDegrees( float32 _rad )
{
	return _rad * ConstF_180DivPi;
}
float64 Math::RadiansToDegrees( float64 _rad )
{
	return _rad * Const_180DivPi;
}
Color Math::Lerp( const Color& _a, const Color& _b, float32 _rat )
{
	return Lerp( _a, _b, ( float64 )_rat );
}
Color Math::Lerp( const Color& _a, const Color& _b, float64 _rat )
{
	const RGBAColor a_( _a );
	const RGBAColor b_( _b );
	return Color
	( RGBAColor(
		Lerp( a_.red, b_.red, _rat ),
		Lerp( a_.green, b_.green, _rat ),
		Lerp( a_.blue, b_.blue, _rat ),
		Lerp( a_.alpha, b_.alpha, _rat )
	) );
}
Color Math::Lerp_SqrtFormula( const Color& _a, const Color& _b, float32 _rat )
{
	return Lerp_SqrtFormula( _a, _b, ( float64 )_rat );
}
Color Math::Lerp_SqrtFormula( const Color& _a, const Color& _b, float64 _rat )
{
	const RGBAColor a_( _a );
	const RGBAColor b_( _b );
	return Color
	( RGBAColor(
		Sqrt( Lerp( a_.red * a_.red, b_.red * b_.red, _rat ) ),
		Sqrt( Lerp( a_.green * a_.green, b_.green * b_.green, _rat ) ),
		Sqrt( Lerp( a_.blue * a_.blue, b_.blue * b_.blue, _rat ) ),
		Sqrt( Lerp( a_.alpha * a_.alpha, b_.alpha * b_.alpha, _rat ) )
	) );
}
float32 Math::Clamp01( float32 _x )
{
	if ( _x <= 0.0f ) return 0.0f;
	if ( _x >= 1.0f ) return 1.0f;
	return _x;
}
float64 Math::Clamp01( float64 _x )
{
	if ( _x <= 0.0 ) return 0.0;
	if ( _x >= 1.0 ) return 1.0;
	return _x;
}
float32 Math::Wrap01( float32 _x )
{
	if ( 1.0f == _x ) return 1.0f;
	return Wrap01ex( _x );
}
float64 Math::Wrap01( float64 _x )
{
	if ( 1.0 == _x ) return 1.0;
	return Wrap01ex( _x );
}
float32 Math::Wrap01ex( float32 _x )
{
	_x = fmodf( _x, 1.0f );
	if ( -0.0f == _x )
		return 0.0f;
	if ( _x < 0.0f )
		_x += 1.0f;
	if ( _x >= 1.0f )
		_x -= 1.0f;
	return _x;
}
float64 Math::Wrap01ex( float64 _x )
{
	_x = fmod( _x, 1.0 );
	if ( -0.0 == _x )
		return 0.0;
	if ( _x < 0.0 )
		_x += 1.0;
	if ( _x >= 1.0 )
		_x -= 1.0;
	return _x;
}
float64 Math::Sin( float64 _deg )
{
	return sin( DegreesToRadians( _deg ) );
}
float64 Math::Cos( float64 _deg )
{
	return cos( DegreesToRadians( _deg ) );
}
float32 Math::Sin( float32 _deg )
{
	return ( float32 )Sin( ( float64 )_deg );
}
float32 Math::Cos( float32 _deg )
{
	return ( float32 )Cos( ( float64 )_deg );
}
float64 Math::Sin_Rad( float64 _rad )
{
	return sin( _rad );
}
float64 Math::Cos_Rad( float64 _rad )
{
	return cos( _rad );
}
float32 Math::Sin_Rad( float32 _rad )
{
	return ( float32 )Sin_Rad( ( float64 )_rad );
}
float32 Math::Cos_Rad( float32 _rad )
{
	return ( float32 )Cos_Rad( ( float64 )_rad );
}
uint16 Math::ReverseByteOrder( uint16 _x )
{
	return
		(
		( ( _x & 0x00ffui16 ) << 010 ) |
		( ( _x & 0xff00ui16 ) >> 010 )
		);
}
uint32 Math::ReverseByteOrder( uint32 _x )
{
	return
		(
		( ( _x & 0x000000ffui32 ) << 030 ) |
		( ( _x & 0x0000ff00ui32 ) << 010 ) |
		( ( _x & 0x00ff0000ui32 ) >> 010 ) |
		( ( _x & 0xff000000ui32 ) >> 030 )
		);
}
uint64 Math::ReverseByteOrder( uint64 _x )
{
	return
		(
		( ( _x & 0x00000000000000ffui64 ) << 070 ) |
		( ( _x & 0x000000000000ff00ui64 ) << 050 ) |
		( ( _x & 0x0000000000ff0000ui64 ) << 030 ) |
		( ( _x & 0x00000000ff000000ui64 ) << 010 ) |
		( ( _x & 0x000000ff00000000ui64 ) >> 010 ) |
		( ( _x & 0x0000ff0000000000ui64 ) >> 030 ) |
		( ( _x & 0x00ff000000000000ui64 ) >> 050 ) |
		( ( _x & 0xff00000000000000ui64 ) >> 070 )
		);
}
uint128 Math::HexStringToUint128( const String& _str )
{
	const uint32 len_ = _str.GetLength();
	uint128 num_ = uint128::ZERO;
	for ( uint32 i = 0u; i < len_; ++i )
	{
		const char& char_ = _str[ i ];
		if ( char_ >= '0' && char_ <= '9' )
			( num_ <<= 4 ) |= ( ( uint128 )( char_ - '0' ) );
		else if ( char_ >= 'A' && char_ <= 'F' )
			( num_ <<= 4 ) |= ( ( uint128 )( char_ - 'A' + 10i8 ) );
		else if ( char_ >= 'a' && char_ <= 'f' )
			( num_ <<= 4 ) |= ( ( uint128 )( char_ - 'a' + 10i8 ) );
	}
	return num_;
}
uint128 Math::DecStringToUint128( const String& _str )
{
	static const uint128 ten_ = uint128( 0ui64, 10ui64 );
	const uint32 len_ = _str.GetLength();
	uint128 num_ = uint128::ZERO;
	for ( uint32 i = 0u; i < len_; ++i )
	{
		const char& char_ = _str[ i ];
		if ( char_ >= '0' && char_ <= '9' )
			( num_ *= ten_ ) += ( ( uint128 )( char_ - '0' ) );
	}
	return num_;
}
uint128 Math::BinStringToUint128( const String& _str )
{
	const uint32 len_ = _str.GetLength();
	uint128 num_ = uint128::ZERO;
	for ( uint32 i = 0u; i < len_; ++i )
	{
		const char& char_ = _str[ i ];
		if ( '0' == char_ )
			num_ <<= 1;
		else if ( '1' == char_ )
			( num_ <<= 1 ) |= uint128::ONE;
	}
	return num_;
}
uint128 Math::OctStringToUint128( const String& _str )
{
	const uint32 len_ = _str.GetLength();
	uint128 num_ = uint128::ZERO;
	for ( uint32 i = 0u; i < len_; ++i )
	{
		const char& char_ = _str[ i ];
		if ( char_ >= '0' && char_ <= '7' )
			( num_ <<= 3 ) |= ( ( uint128 )( char_ - '0' ) );
	}
	return num_;
}