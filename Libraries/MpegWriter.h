#pragma once

enum MPEG_FPS
{
	FRAMERATE_24_FPS = 0x12,
	FRAMERATE_25_FPS = 0x13,
	FRAMERATE_30_FPS = 0x14,
	FRAMERATE_50_FPS = 0x15,
	FRAMERATE_60_FPS = 0x16
};

class MpegWriter
{
private:
	std::ofstream m_file;
	float64* m_tempDataBuffer;
	uint32 m_width, m_height, m_buf, m_cnt;
	MPEG_FPS m_fps;
	MpegWriter( void );
	int32 ProcessDU( float64*, const uint8*, int32 );
	void WriteBits( uint32, uint32 );
public:
	MpegWriter( const char* const, uint32, uint32, MPEG_FPS = MPEG_FPS::FRAMERATE_60_FPS );
	~MpegWriter( void );
	void AddFrame( const uint32* );
};