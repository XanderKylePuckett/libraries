#pragma once

struct Vector3
{
	float64 x, y, z;
	static const Vector3 ZERO;
	static const Vector3 ONE;
	static const Vector3 X_AXIS;
	static const Vector3 Y_AXIS;
	static const Vector3 Z_AXIS;
	Vector3( void );
	Vector3( float64, float64, float64 );
	Vector3( const Vector3& );
	Vector3& operator=( const Vector3& );
	Vector3( Vector3&& );
	Vector3& operator=( Vector3&& );
	~Vector3( void );
	static float64 dot( const Vector3&, const Vector3& );
	static Vector3 cross( const Vector3&, const Vector3& );
	float64 magnitude( void ) const;
	float64 sqmagnitude( void ) const;
	Vector3& normalize( void );
	Vector3 direction( void ) const;
	float64 dot( const Vector3& ) const;
	Vector3 cross( const Vector3& ) const;
	Vector3& negate( void );
	Vector3 operator+( void ) const;
	Vector3 operator-( void ) const;
	Vector3 operator+( const Vector3& ) const;
	Vector3 operator-( const Vector3& ) const;
	float64 operator*( const Vector3& ) const;
	Vector3 operator*( float64 ) const;
	Vector3 operator/( float64 ) const;
	Vector3& operator+=( const Vector3& );
	Vector3& operator-=( const Vector3& );
	Vector3& operator*=( float64 );
	Vector3& operator/=( float64 );
	bool operator==( const Vector3& ) const;
	bool operator!=( const Vector3& ) const;
	static float64 SqDistance( const Vector3&, const Vector3& );
	static float64 Distance( const Vector3&, const Vector3& );
};

Vector3 operator*( float64, const Vector3& );
std::ostream& operator<<( std::ostream&, const Vector3& );