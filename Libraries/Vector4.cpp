#include "stdafx.h"
#include "Vector4.h"

const Vector4 Vector4::ZERO = Vector4( 0.0, 0.0, 0.0, 0.0 );
const Vector4 Vector4::ONE = Vector4( 1.0, 1.0, 1.0, 1.0 );
Vector4::Vector4( void )
{
}
Vector4::Vector4( float64 _x, float64 _y, float64 _z, float64 _w ) :
	x( _x ), y( _y ), z( _z ), w( _w )
{
}
Vector4::Vector4( const Vector4& rhs ) :
	x( rhs.x ), y( rhs.y ), z( rhs.z ), w( rhs.w )
{
}
Vector4& Vector4::operator=( const Vector4& rhs )
{
	if ( this != &rhs )
	{
		x = rhs.x;
		y = rhs.y;
		z = rhs.z;
		w = rhs.w;
	}
	return *this;
}
Vector4::Vector4( Vector4&& rhs ) :
	x( std::move( rhs.x ) ),
	y( std::move( rhs.y ) ),
	z( std::move( rhs.z ) ),
	w( std::move( rhs.w ) )
{
}
Vector4& Vector4::operator=( Vector4&& rhs )
{
	if ( this != &rhs )
	{
		x = std::move( rhs.x );
		y = std::move( rhs.y );
		z = std::move( rhs.z );
		w = std::move( rhs.w );
	}
	return *this;
}
Vector4::~Vector4( void )
{
}
float64 Vector4::dot( const Vector4& lhs, const Vector4& rhs )
{
	return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z + lhs.w * rhs.w;
}
float64 Vector4::dot( const Vector4& rhs ) const
{
	return x * rhs.x + y * rhs.y + z * rhs.z + w * rhs.w;
}
Vector4 Vector4::operator+( void ) const
{
	return *this;
}
Vector4 Vector4::operator-( void ) const
{
	return Vector4( -x, -y, -z, -w );
}
Vector4 Vector4::operator+( const Vector4& rhs ) const
{
	return Vector4( x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w );
}
Vector4 Vector4::operator-( const Vector4& rhs ) const
{
	return Vector4( x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w );
}
float64 Vector4::operator*( const Vector4& rhs ) const
{
	return x * rhs.x + y * rhs.y + z * rhs.z + w * rhs.w;
}
Vector4 Vector4::operator*( float64 rhs ) const
{
	return Vector4( x * rhs, y * rhs, z * rhs, w * rhs );
}
Vector4 Vector4::operator/( float64 rhs ) const
{
	rhs = 1.0 / rhs;
	return Vector4( x * rhs, y * rhs, z * rhs, w * rhs );
}
Vector4& Vector4::operator+=( const Vector4& rhs )
{
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;
	w += rhs.w;
	return *this;
}
Vector4& Vector4::operator-=( const Vector4& rhs )
{
	x -= rhs.x;
	y -= rhs.y;
	z -= rhs.z;
	w -= rhs.w;
	return *this;
}
Vector4& Vector4::operator*=( float64 rhs )
{
	x *= rhs;
	y *= rhs;
	z *= rhs;
	w *= rhs;
	return *this;
}
Vector4& Vector4::operator/=( float64 rhs )
{
	rhs = 1.0 / rhs;
	x *= rhs;
	y *= rhs;
	z *= rhs;
	w *= rhs;
	return *this;
}
bool Vector4::operator==( const Vector4& rhs ) const
{
	return x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w;
}
bool Vector4::operator!=( const Vector4& rhs ) const
{
	return x != rhs.x || y != rhs.y || z != rhs.z || w != rhs.w;
}
Vector4 operator*( float64 lhs, const Vector4& rhs )
{
	return Vector4( lhs * rhs.x, lhs * rhs.y, lhs * rhs.z, lhs * rhs.w );
}
std::ostream& operator<<( std::ostream& os, const Vector4& rhs )
{
	return os << '(' << rhs.x << ',' << rhs.y << ',' << rhs.z << ',' << rhs.w << ')';
}