#pragma once
#include "Texture2D.h"

class Raster
{
public:
	static void InitWindow( uint32 width, uint32 height );
	static bool UpdateWindow( void );
	static void ShutdownWindow( void );
	static Texture2D& GetTexture( void );
	static HWND GetWindow( void );
};