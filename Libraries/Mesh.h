#pragma once
#include "DynArray.h"
#include "Geometry.h"

class Mesh
{
private:
	DynArray<PositionColorTriangle> m_triangles;
public:
	Mesh( void );
	Mesh( const Mesh& );
	Mesh( Mesh&& );
	Mesh& operator=( const Mesh& );
	Mesh& operator=( Mesh&& );
	~Mesh( void );
	void AddTriangle( const PositionColorTriangle& );
	Mesh& operator+=( const PositionColorTriangle& );
	Mesh operator+( const Mesh& ) const;
	Mesh& operator+=( const Mesh& );
	const DynArray<PositionColorTriangle>& GetTriangles( void ) const;
	void ClearTriangles( void );
};