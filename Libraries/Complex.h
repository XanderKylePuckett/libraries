#pragma once

struct Complex
{
	float64 real, imaginary;
	Complex( void );
	Complex( float64, float64 = 0.0 );
	Complex Conjugate( void ) const;
	float64 Argument( void ) const;
	Complex Pow( const Complex& ) const;
	Complex Pow( float64 ) const;
	float64 Norm( void ) const;
	float64 NormSq( void ) const;
	Complex Square( void ) const;
	static Complex Conjugate( const Complex& );
	static float64 Argument( const Complex& );
	static Complex Pow( const Complex&, const Complex& );
	static Complex Pow( const Complex&, float64 );
	static Complex Pow( float64, const Complex& );
	static float64 Norm( const Complex& );
	static float64 NormSq( const Complex& );
	static Complex Square( const Complex& );
	Complex operator+( const Complex& ) const;
	Complex operator-( const Complex& ) const;
	Complex operator*( const Complex& ) const;
	Complex operator/( const Complex& ) const;
	Complex operator+( void ) const;
	Complex operator-( void ) const;
	Complex& operator+=( const Complex& );
	Complex& operator-=( const Complex& );
	Complex& operator*=( const Complex& );
	Complex& operator/=( const Complex& );
	Complex operator+( float64 ) const;
	Complex operator-( float64 ) const;
	Complex operator*( float64 ) const;
	Complex operator/( float64 ) const;
	Complex& operator+=( float64 );
	Complex& operator-=( float64 );
	Complex& operator*=( float64 );
	Complex& operator/=( float64 );
	bool operator==( const Complex& ) const;
	bool operator!=( const Complex& ) const;
	bool operator<( const Complex& ) const;
	bool operator>( const Complex& ) const;
	bool operator<=( const Complex& ) const;
	bool operator>=( const Complex& ) const;
	bool operator==( float64 ) const;
	bool operator!=( float64 ) const;
	bool operator<( float64 ) const;
	bool operator>( float64 ) const;
	bool operator<=( float64 ) const;
	bool operator>=( float64 ) const;
};

Complex operator+( float64, const Complex& );
Complex operator-( float64, const Complex& );
Complex operator*( float64, const Complex& );
Complex operator/( float64, const Complex& );
bool operator==( float64, const Complex& );
bool operator!=( float64, const Complex& );
bool operator<( float64, const Complex& );
bool operator>( float64, const Complex& );
bool operator<=( float64, const Complex& );
bool operator>=( float64, const Complex& );
std::ostream& operator<<( std::ostream&, const Complex& );