#include "stdafx.h"
#include "Input.h"

static uint64 g_keyDown[ 4u ] = { 0ull, 0ull, 0ull, 0ull };
static uint64 g_keyPress[ 4u ] = { 0ull, 0ull, 0ull, 0ull };
static uint64 g_keyRelease[ 4u ] = { 0ull, 0ull, 0ull, 0ull };

void Input::Update( void )
{
	static uint16 j, idx, bit;
	for ( j = 0ui16; j < 256ui16; ++j )
	{
		idx = j & 3ui16;
		bit = 1ull << ( j >> 2u );
		if ( GetAsyncKeyState( j ) )
		{
			if ( g_keyDown[ idx ] & bit )
				g_keyPress[ idx ] &= ~bit;
			else g_keyPress[ idx ] |= bit;
			g_keyDown[ idx ] |= bit;
			g_keyRelease[ idx ] &= ~bit;
		}
		else
		{
			if ( g_keyDown[ idx ] & bit )
				g_keyRelease[ idx ] |= bit;
			else g_keyRelease[ idx ] &= ~bit;
			g_keyDown[ idx ] &= ~bit;
			g_keyPress[ idx ] &= ~bit;
		}
	}
}
bool Input::KeyPress( uint8 _key )
{
	return 0u != ( g_keyPress[ _key & 3ui8 ] & ( 1ull << ( _key >> 2u ) ) );
}
bool Input::KeyRelease( uint8 _key )
{
	return 0u != ( g_keyRelease[ _key & 3ui8 ] & ( 1ull << ( _key >> 2u ) ) );
}
bool Input::KeyDown( uint8 _key )
{
	return 0u != ( g_keyDown[ _key & 3ui8 ] & ( 1ull << ( _key >> 2u ) ) );
}
bool Input::KeyUp( uint8 _key )
{
	return 0u == ( g_keyDown[ _key & 3ui8 ] & ( 1ull << ( _key >> 2u ) ) );
}