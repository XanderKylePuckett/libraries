#include "stdafx.h"
#include "Fuzzy.h"
#include "Math.h"

Fuzzy::Fuzzy( void ) :
	m_fuzzy( 0.0 )
{
}
Fuzzy::Fuzzy( bool _fuzzy ) :
	m_fuzzy( _fuzzy ? 1.0 : 0.0 )
{
}
Fuzzy::Fuzzy( float64 _fuzzy ) :
	m_fuzzy( Math::Clamp01( _fuzzy ) )
{
}
Fuzzy::Fuzzy( const Fuzzy& _rhs ) :
	m_fuzzy( _rhs.m_fuzzy )
{
}
Fuzzy::Fuzzy( Fuzzy&& _rhs ) :
	m_fuzzy( std::move( _rhs.m_fuzzy ) )
{
	_rhs.m_fuzzy = false;
}
Fuzzy& Fuzzy::operator=( const Fuzzy& _rhs )
{
	if ( this != &_rhs )
	{
		m_fuzzy = _rhs.m_fuzzy;
	}
	return *this;
}
Fuzzy& Fuzzy::operator=( Fuzzy&& _rhs )
{
	if ( this != &_rhs )
	{
		m_fuzzy = std::move( _rhs.m_fuzzy );
		_rhs.m_fuzzy = 0.0;
	}
	return *this;
}
Fuzzy::~Fuzzy( void )
{
}
Fuzzy Fuzzy::operator!( void ) const
{
	return Fuzzy( 1.0 - m_fuzzy );
}
Fuzzy Fuzzy::operator&&( const Fuzzy& _rhs ) const
{
	return Fuzzy( Math::Min( m_fuzzy, _rhs.m_fuzzy ) );
}
Fuzzy Fuzzy::operator||( const Fuzzy& _rhs ) const
{
	return Fuzzy( Math::Min( m_fuzzy, _rhs.m_fuzzy ) );
}
bool Fuzzy::GetBool( float64 _threshold ) const
{
	return m_fuzzy >= _threshold;
}
float64 Fuzzy::GetValue( void ) const
{
	return m_fuzzy;
}
Fuzzy& Fuzzy::SetValue( float64 _fuzzy )
{
	m_fuzzy = Math::Clamp01( _fuzzy );
	return *this;
}
Fuzzy::operator bool( void ) const
{
	return m_fuzzy >= 0.5;
}
Fuzzy::operator float64( void ) const
{
	return m_fuzzy;
}
Fuzzy Fuzzy::operator==( const Fuzzy& _rhs ) const
{
	return Fuzzy( 1.0 - Math::Abs( m_fuzzy - _rhs.m_fuzzy ) );
}
Fuzzy Fuzzy::operator!=( const Fuzzy& _rhs ) const
{
	return Fuzzy( Math::Abs( m_fuzzy - _rhs.m_fuzzy ) );
}