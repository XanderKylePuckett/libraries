#include "stdafx.h"
#include "Avi.h"
#pragma comment( lib, "vfw32.lib" )

class Bitmap
{
private:
	Bitmap( HBITMAP _hbmp ) :
		m_bitmap( _hbmp )
	{
	}
	HBITMAP m_bitmap;
public:
	Bitmap( void ) :
		m_bitmap( nullptr )
	{
	}
	Bitmap( const std::string& _filename ) :
		m_bitmap( ( HBITMAP )LoadImageA( nullptr, _filename.c_str(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION ) )
	{
	}
	operator HBITMAP( void ) const
	{
		return m_bitmap;
	}
};

Avi::Avi( const std::string& _filename ) :
	m_pfile( nullptr ),
	m_ps( nullptr ),
	m_psCompressed( nullptr ),
	m_nframe( 0ul ),
	m_iserr( true )
{
	AVIFileInit();
	if ( AVIERR_OK == AVIFileOpenA( &m_pfile, _filename.c_str(), OF_WRITE | OF_CREATE, nullptr ) )
		m_iserr = false;
	else
		AVIFileExit();
}

Avi::~Avi( void )
{
	if ( nullptr != m_psCompressed )
		AVIStreamRelease( m_psCompressed );
	if ( nullptr != m_ps )
		AVIStreamRelease( m_ps );
	if ( nullptr != m_pfile )
		AVIFileRelease( m_pfile );
	if ( !m_iserr )
		AVIFileExit();
}

bool Avi::AddFrame( const std::string& _bmpfile )
{
	if ( m_iserr )
		return false;
	DIBSECTION dibs;
	if ( sizeof( DIBSECTION ) != GetObject( Bitmap( _bmpfile ), sizeof( dibs ), &dibs ) )
		return false;
	if ( nullptr == m_ps )
	{
		AVISTREAMINFO strhdr;
		ZeroMemory( &strhdr, sizeof( strhdr ) );
		strhdr.fccType = streamtypeVIDEO;
		strhdr.fccHandler = 0;
		strhdr.dwScale = 16;
		strhdr.dwRate = 1000;
		strhdr.dwSuggestedBufferSize = dibs.dsBmih.biSizeImage;
		SetRect( &strhdr.rcFrame, 0, 0, dibs.dsBmih.biWidth, dibs.dsBmih.biHeight );
		if ( AVIERR_OK != AVIFileCreateStream( m_pfile, &m_ps, &strhdr ) )
		{
			m_iserr = true;
			AVIFileExit();
			return false;
		}
	}
	if ( nullptr == m_psCompressed )
	{
		AVICOMPRESSOPTIONS opts;
		ZeroMemory( &opts, sizeof( opts ) );
		opts.fccHandler = mmioFOURCC( 'D', 'I', 'B', ' ' );
		if ( AVIERR_OK != AVIMakeCompressedStream( &m_psCompressed, m_ps, &opts, nullptr ) )
		{
			m_iserr = true;
			AVIFileExit();
			return false;
		}
		if ( AVIERR_OK != AVIStreamSetFormat( m_psCompressed, 0, &dibs.dsBmih, dibs.dsBmih.biSize + dibs.dsBmih.biClrUsed * sizeof( RGBQUAD ) ) )
		{
			m_iserr = true;
			AVIFileExit();
			return false;
		}
	}
	if ( AVIERR_OK != AVIStreamWrite( m_psCompressed, m_nframe, 1, dibs.dsBm.bmBits, dibs.dsBmih.biSizeImage, AVIIF_KEYFRAME, 0, 0 ) )
	{
		m_iserr = true;
		AVIFileExit();
		return false;
	}
	++m_nframe;
	return true;
}