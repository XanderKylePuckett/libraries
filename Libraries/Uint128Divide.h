#pragma once

void Uint128Divide( uint128 _dividend, const uint128& _divisor, uint128& _quotient, uint128& _remainder )
{
	_remainder = uint128::ZERO;
	_quotient = uint128::ZERO;
	if ( _divisor == uint128::ZERO )
		return;
	if ( _divisor > _dividend )
		_remainder = _dividend;
	else if ( _divisor == _dividend )
		_quotient = uint128::ONE;
	else
	{
		static const uint128 highBit_ = uint128( 0x8000000000000000ui64, 0ui64 );
		uint16 numBits_ = 128ui16;
		uint128 t0_, t1_, t2_, t3_;
		while ( _remainder < _divisor )
		{
			t0_ = ( _dividend & highBit_ ) >> 127;
			_remainder = ( _remainder << 1 ) | t0_;
			t1_ = _dividend;
			_dividend <<= 1;
			--numBits_;
		}
		_dividend = t1_;
		_remainder >>= 1;
		++numBits_;
		for ( uint16 i = 0ui16; i < numBits_; ++i )
		{
			t0_ = ( _dividend & highBit_ ) >> 127;
			_remainder = ( _remainder << 1 ) | t0_;
			t2_ = _remainder - _divisor;
			t3_ = !( ( t2_ & highBit_ ) >> 127 );
			_dividend <<= 1;
			_quotient = ( _quotient << 1 ) | t3_;
			if ( uint128::ZERO != t3_ )
				_remainder = t2_;
		}
	}
}