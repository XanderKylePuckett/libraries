#include "stdafx.h"
#include "DynArray.h"
#include "Vector2.h"
#pragma warning(disable:4201)

struct Line2D
{
	Vector2 start, end;
	Line2D( void );
	Line2D( const Vector2&, const Vector2& );
	Line2D( float64, float64, float64, float64 );
	Line2D( const Line2D& );
	Line2D( Line2D&& );
	Line2D& operator=( const Line2D& );
	Line2D& operator=( Line2D&& );
	~Line2D( void );
	Line2D operator+( const Vector2& ) const;
	Line2D& operator+=( const Vector2& );
	Line2D operator-( const Vector2& ) const;
	Line2D& operator-=( const Vector2& );
	Line2D operator*( float64 ) const;
	Line2D& operator*=( float64 );
	Line2D operator/( float64 ) const;
	Line2D& operator/=( float64 );
	friend Line2D operator*( float64, const Line2D& );
	friend std::ostream& operator<<( std::ostream&, const Line2D& );
};

struct Letter
{
	DynArray<Line2D> lines;
	Letter& OptimizeSpace( void );
	Letter& Offset( const Vector2& );
	Letter( void );
	Letter( const Letter& );
	Letter( Letter&& );
	Letter& operator=( const Letter& );
	Letter& operator=( Letter&& );
	~Letter( void );
};
Letter::Letter( void )
{
	lines.Clear();
}
Letter::Letter( const Letter& _rhs ) :
	lines( _rhs.lines )
{
}
Letter::Letter( Letter&& _rhs ) :
	lines( std::move( _rhs.lines ) )
{
}
Letter& Letter::operator=( const Letter& _rhs )
{
	if ( this != &_rhs )
	{
		lines = _rhs.lines;
	}
	return *this;
}
Letter& Letter::operator=( Letter&& _rhs )
{
	if ( this != &_rhs )
	{
		lines = std::move( _rhs.lines );
	}
	return *this;
}
Letter::~Letter( void )
{
}
struct LettersXKP
{
	union
	{
		Letter letters[ 13 ];
		struct
		{
			Letter x, a, n, d, e, r, k, y, l, p, u, c, t;
		};
	};
	LettersXKP( void );
	void Setup( void );
	~LettersXKP( void );
};
LettersXKP::LettersXKP( void ) :
	x(), a(), n(), d(), e(), r(), k(), y(), l(), p(), u(), c(), t()
{
	Setup();
}
LettersXKP::~LettersXKP( void )
{
}
int LetterMain( int, char** )
{
	std::ofstream textFile;
	textFile.open( "XKP_Lines.txt", std::ios_base::out | std::ios_base::trunc );
	if ( !textFile.is_open() )
		return EXIT_FAILURE;
	std::ofstream binFile;
	binFile.open( "XKP_Lines.bin", std::ios_base::out | std::ios_base::trunc | std::ios_base::binary );
	if ( !binFile.is_open() )
	{
		textFile.close();
		return EXIT_FAILURE;
	}
	std::ofstream shortBinFile;
	shortBinFile.open( "XKP_Lines_Short.bin", std::ios_base::out | std::ios_base::trunc | std::ios_base::binary );
	if ( !shortBinFile.is_open() )
	{
		textFile.close();
		binFile.close();
		return EXIT_FAILURE;
	}
	DynArray<Line2D> allLines;
	LettersXKP xkp;
	Letter letter;
	allLines += ( letter = xkp.x ).Offset( Vector2( 288.0, 0.0 ) ).lines;
	allLines += ( letter = xkp.a ).Offset( Vector2( 864.0, 0.0 ) ).lines;
	allLines += ( letter = xkp.n ).Offset( Vector2( 1440.0, 0.0 ) ).lines;
	allLines += ( letter = xkp.d ).Offset( Vector2( 2016.0, 0.0 ) ).lines;
	allLines += ( letter = xkp.e ).Offset( Vector2( 2592.0, 0.0 ) ).lines;
	allLines += ( letter = xkp.r ).Offset( Vector2( 3168.0, 0.0 ) ).lines;
	allLines += ( letter = xkp.k ).Offset( Vector2( 864.0, 960.0 ) ).lines;
	allLines += ( letter = xkp.y ).Offset( Vector2( 1440.0, 960.0 ) ).lines;
	allLines += ( letter = xkp.l ).Offset( Vector2( 2016.0, 960.0 ) ).lines;
	allLines += ( letter = xkp.e ).Offset( Vector2( 2592.0, 960.0 ) ).lines;
	allLines += ( letter = xkp.p ).Offset( Vector2( 0.0, 1920.0 ) ).lines;
	allLines += ( letter = xkp.u ).Offset( Vector2( 576.0, 1920.0 ) ).lines;
	allLines += ( letter = xkp.c ).Offset( Vector2( 1152.0, 1920.0 ) ).lines;
	allLines += ( letter = xkp.k ).Offset( Vector2( 1728.0, 1920.0 ) ).lines;
	allLines += ( letter = xkp.e ).Offset( Vector2( 2304.0, 1920.0 ) ).lines;
	allLines += ( letter = xkp.t ).Offset( Vector2( 2880.0, 1920.0 ) ).lines;
	allLines += ( letter = xkp.t ).Offset( Vector2( 3456.0, 1920.0 ) ).lines;
	const uint32 size = allLines.GetSize();
	{
		const uint64 size64 = ( uint64 )size;
		binFile.write( ( char* )&size64, sizeof( uint64 ) );
	}
	uint16 buff16 = ( uint16 )size;
	shortBinFile.write( ( char* )&buff16, sizeof( uint16 ) );
	for ( uint32 i = 0u; i < size; ++i )
	{
		const Line2D& line = allLines[ i ];
		std::cout << line << std::endl;
		textFile << line << std::endl;
		binFile.write( ( char* )&line, sizeof( Line2D ) );
		shortBinFile.write( ( char* )&( buff16 = ( uint16 )( line.start.x ) ), sizeof( uint16 ) );
		shortBinFile.write( ( char* )&( buff16 = ( uint16 )( line.start.y ) ), sizeof( uint16 ) );
		shortBinFile.write( ( char* )&( buff16 = ( uint16 )( line.end.x ) ), sizeof( uint16 ) );
		shortBinFile.write( ( char* )&( buff16 = ( uint16 )( line.end.y ) ), sizeof( uint16 ) );
	}
	textFile.close();
	binFile.close();
	shortBinFile.close();
	return EXIT_SUCCESS;
}
Letter& Letter::OptimizeSpace( void )
{
	lines.OptimizeSpace();
	return *this;
}
Letter& Letter::Offset( const Vector2& offset )
{
	for ( uint32 i = 0u; i < lines.GetSize(); ++i )
		lines[ i ] += offset;
	return *this;
}
void LettersXKP::Setup( void )
{
	Vector2 buff[ 17 ];
	buff[ 0 ] = Vector2( 0.0, 0.0 );
	buff[ 1 ] = Vector2( 48.0, 0.0 );
	buff[ 2 ] = Vector2( 336.0, 0.0 );
	buff[ 3 ] = Vector2( 384.0, 0.0 );
	buff[ 4 ] = Vector2( 0.0, 48.0 );
	buff[ 5 ] = Vector2( 384.0, 48.0 );
	buff[ 6 ] = Vector2( 192.0, 312.0 );
	buff[ 7 ] = Vector2( 144.0, 384.0 );
	buff[ 8 ] = Vector2( 240.0, 384.0 );
	buff[ 9 ] = Vector2( 192.0, 456.0 );
	buff[ 10 ] = Vector2( 0.0, 720.0 );
	buff[ 11 ] = Vector2( 384.0, 720.0 );
	buff[ 12 ] = Vector2( 0.0, 768.0 );
	buff[ 13 ] = Vector2( 48.0, 768.0 );
	buff[ 14 ] = Vector2( 336.0, 768.0 );
	buff[ 15 ] = Vector2( 384.0, 768.0 );
	x.lines += Line2D( buff[ 0 ], buff[ 1 ] );
	x.lines += Line2D( buff[ 2 ], buff[ 3 ] );
	x.lines += Line2D( buff[ 0 ], buff[ 4 ] );
	x.lines += Line2D( buff[ 3 ], buff[ 5 ] );
	x.lines += Line2D( buff[ 1 ], buff[ 6 ] );
	x.lines += Line2D( buff[ 2 ], buff[ 6 ] );
	x.lines += Line2D( buff[ 4 ], buff[ 7 ] );
	x.lines += Line2D( buff[ 5 ], buff[ 8 ] );
	x.lines += Line2D( buff[ 7 ], buff[ 10 ] );
	x.lines += Line2D( buff[ 8 ], buff[ 11 ] );
	x.lines += Line2D( buff[ 9 ], buff[ 13 ] );
	x.lines += Line2D( buff[ 9 ], buff[ 14 ] );
	x.lines += Line2D( buff[ 10 ], buff[ 12 ] );
	x.lines += Line2D( buff[ 12 ], buff[ 13 ] );
	x.lines += Line2D( buff[ 11 ], buff[ 15 ] );
	x.lines += Line2D( buff[ 14 ], buff[ 15 ] );
	buff[ 0 ] = Vector2( 168.0, 0.0 );
	buff[ 1 ] = Vector2( 216.0, 0.0 );
	buff[ 2 ] = Vector2( 192.0, 72.0 );
	buff[ 3 ] = Vector2( 0.0, 336.0 );
	buff[ 4 ] = Vector2( 384.0, 336.0 );
	buff[ 5 ] = Vector2( 48.0, 360.0 );
	buff[ 6 ] = Vector2( 336.0, 360.0 );
	buff[ 7 ] = Vector2( 48.0, 432.0 );
	buff[ 8 ] = Vector2( 336.0, 432.0 );
	buff[ 9 ] = Vector2( 0.0, 768.0 );
	buff[ 10 ] = Vector2( 48.0, 768.0 );
	buff[ 11 ] = Vector2( 336.0, 768.0 );
	buff[ 12 ] = Vector2( 384.0, 768.0 );
	a.lines += Line2D( buff[ 0 ], buff[ 1 ] );
	a.lines += Line2D( buff[ 0 ], buff[ 3 ] );
	a.lines += Line2D( buff[ 1 ], buff[ 4 ] );
	a.lines += Line2D( buff[ 2 ], buff[ 5 ] );
	a.lines += Line2D( buff[ 2 ], buff[ 6 ] );
	a.lines += Line2D( buff[ 5 ], buff[ 6 ] );
	a.lines += Line2D( buff[ 7 ], buff[ 8 ] );
	a.lines += Line2D( buff[ 3 ], buff[ 9 ] );
	a.lines += Line2D( buff[ 4 ], buff[ 12 ] );
	a.lines += Line2D( buff[ 7 ], buff[ 10 ] );
	a.lines += Line2D( buff[ 8 ], buff[ 11 ] );
	a.lines += Line2D( buff[ 9 ], buff[ 10 ] );
	a.lines += Line2D( buff[ 11 ], buff[ 12 ] );
	buff[ 0 ] = Vector2( 0.0, 0.0 );
	buff[ 1 ] = Vector2( 48.0, 0.0 );
	buff[ 2 ] = Vector2( 336.0, 0.0 );
	buff[ 3 ] = Vector2( 384.0, 0.0 );
	buff[ 4 ] = Vector2( 48.0, 144.0 );
	buff[ 5 ] = Vector2( 336.0, 624.0 );
	buff[ 6 ] = Vector2( 0.0, 768.0 );
	buff[ 7 ] = Vector2( 48.0, 768.0 );
	buff[ 8 ] = Vector2( 336.0, 768.0 );
	buff[ 9 ] = Vector2( 384.0, 768.0 );
	n.lines += Line2D( buff[ 0 ], buff[ 1 ] );
	n.lines += Line2D( buff[ 2 ], buff[ 3 ] );
	n.lines += Line2D( buff[ 1 ], buff[ 5 ] );
	n.lines += Line2D( buff[ 4 ], buff[ 8 ] );
	n.lines += Line2D( buff[ 0 ], buff[ 6 ] );
	n.lines += Line2D( buff[ 4 ], buff[ 7 ] );
	n.lines += Line2D( buff[ 2 ], buff[ 5 ] );
	n.lines += Line2D( buff[ 3 ], buff[ 9 ] );
	n.lines += Line2D( buff[ 6 ], buff[ 7 ] );
	n.lines += Line2D( buff[ 8 ], buff[ 9 ] );
	buff[ 0 ] = Vector2( 0.0, 0.0 );
	buff[ 1 ] = Vector2( 192.0, 0.0 );
	buff[ 2 ] = Vector2( 48.0, 72.0 );
	buff[ 3 ] = Vector2( 192.0, 72.0 );
	buff[ 4 ] = Vector2( 336.0, 384.0 );
	buff[ 5 ] = Vector2( 384.0, 384.0 );
	buff[ 6 ] = Vector2( 48.0, 696.0 );
	buff[ 7 ] = Vector2( 192.0, 696.0 );
	buff[ 8 ] = Vector2( 0.0, 768.0 );
	buff[ 9 ] = Vector2( 192.0, 768.0 );
	d.lines += Line2D( buff[ 0 ], buff[ 1 ] );
	d.lines += Line2D( buff[ 2 ], buff[ 3 ] );
	d.lines += Line2D( buff[ 6 ], buff[ 7 ] );
	d.lines += Line2D( buff[ 8 ], buff[ 9 ] );
	d.lines += Line2D( buff[ 0 ], buff[ 8 ] );
	d.lines += Line2D( buff[ 2 ], buff[ 6 ] );
	d.lines += Line2D( buff[ 3 ], buff[ 4 ] );
	d.lines += Line2D( buff[ 4 ], buff[ 7 ] );
	d.lines += Line2D( buff[ 1 ], buff[ 5 ] );
	d.lines += Line2D( buff[ 5 ], buff[ 9 ] );
	buff[ 0 ] = Vector2( 0.0, 0.0 );
	buff[ 1 ] = Vector2( 384.0, 0.0 );
	buff[ 2 ] = Vector2( 48.0, 72.0 );
	buff[ 3 ] = Vector2( 384.0, 72.0 );
	buff[ 4 ] = Vector2( 48.0, 360.0 );
	buff[ 5 ] = Vector2( 288.0, 360.0 );
	buff[ 6 ] = Vector2( 48.0, 408.0 );
	buff[ 7 ] = Vector2( 288.0, 408.0 );
	buff[ 8 ] = Vector2( 48.0, 696.0 );
	buff[ 9 ] = Vector2( 384.0, 696.0 );
	buff[ 10 ] = Vector2( 0.0, 768.0 );
	buff[ 11 ] = Vector2( 384.0, 768.0 );
	e.lines += Line2D( buff[ 0 ], buff[ 1 ] );
	e.lines += Line2D( buff[ 2 ], buff[ 3 ] );
	e.lines += Line2D( buff[ 4 ], buff[ 5 ] );
	e.lines += Line2D( buff[ 6 ], buff[ 7 ] );
	e.lines += Line2D( buff[ 8 ], buff[ 9 ] );
	e.lines += Line2D( buff[ 10 ], buff[ 11 ] );
	e.lines += Line2D( buff[ 0 ], buff[ 10 ] );
	e.lines += Line2D( buff[ 2 ], buff[ 4 ] );
	e.lines += Line2D( buff[ 6 ], buff[ 8 ] );
	e.lines += Line2D( buff[ 5 ], buff[ 7 ] );
	e.lines += Line2D( buff[ 1 ], buff[ 3 ] );
	e.lines += Line2D( buff[ 9 ], buff[ 11 ] );
	buff[ 0 ] = Vector2( 0.0, 0.0 );
	buff[ 1 ] = Vector2( 216.0, 0.0 );
	buff[ 2 ] = Vector2( 48.0, 72.0 );
	buff[ 3 ] = Vector2( 216.0, 72.0 );
	buff[ 4 ] = Vector2( 384.0, 168.0 );
	buff[ 5 ] = Vector2( 336.0, 192.0 );
	buff[ 6 ] = Vector2( 384.0, 216.0 );
	buff[ 7 ] = Vector2( 48.0, 336.0 );
	buff[ 8 ] = Vector2( 192.0, 336.0 );
	buff[ 9 ] = Vector2( 216.0, 384.0 );
	buff[ 10 ] = Vector2( 48.0, 408.0 );
	buff[ 11 ] = Vector2( 168.0, 408.0 );
	buff[ 12 ] = Vector2( 384.0, 720.0 );
	buff[ 13 ] = Vector2( 0.0, 768.0 );
	buff[ 14 ] = Vector2( 48.0, 768.0 );
	buff[ 15 ] = Vector2( 336.0, 768.0 );
	buff[ 16 ] = Vector2( 384.0, 768.0 );
	r.lines += Line2D( buff[ 0 ], buff[ 1 ] );
	r.lines += Line2D( buff[ 2 ], buff[ 3 ] );
	r.lines += Line2D( buff[ 7 ], buff[ 8 ] );
	r.lines += Line2D( buff[ 10 ], buff[ 11 ] );
	r.lines += Line2D( buff[ 13 ], buff[ 14 ] );
	r.lines += Line2D( buff[ 15 ], buff[ 16 ] );
	r.lines += Line2D( buff[ 0 ], buff[ 13 ] );
	r.lines += Line2D( buff[ 2 ], buff[ 7 ] );
	r.lines += Line2D( buff[ 10 ], buff[ 14 ] );
	r.lines += Line2D( buff[ 1 ], buff[ 4 ] );
	r.lines += Line2D( buff[ 3 ], buff[ 5 ] );
	r.lines += Line2D( buff[ 4 ], buff[ 6 ] );
	r.lines += Line2D( buff[ 5 ], buff[ 8 ] );
	r.lines += Line2D( buff[ 6 ], buff[ 9 ] );
	r.lines += Line2D( buff[ 11 ], buff[ 15 ] );
	r.lines += Line2D( buff[ 9 ], buff[ 12 ] );
	r.lines += Line2D( buff[ 12 ], buff[ 16 ] );
	buff[ 0 ] = Vector2( 0.0, 0.0 );
	buff[ 1 ] = Vector2( 48.0, 0.0 );
	buff[ 2 ] = Vector2( 336.0, 0.0 );
	buff[ 3 ] = Vector2( 384.0, 0.0 );
	buff[ 4 ] = Vector2( 384.0, 48.0 );
	buff[ 5 ] = Vector2( 48.0, 336.0 );
	buff[ 6 ] = Vector2( 96.0, 384.0 );
	buff[ 7 ] = Vector2( 48.0, 432.0 );
	buff[ 8 ] = Vector2( 384.0, 720.0 );
	buff[ 9 ] = Vector2( 0.0, 768.0 );
	buff[ 10 ] = Vector2( 48.0, 768.0 );
	buff[ 11 ] = Vector2( 336.0, 768.0 );
	buff[ 12 ] = Vector2( 384.0, 768.0 );
	k.lines += Line2D( buff[ 0 ], buff[ 1 ] );
	k.lines += Line2D( buff[ 2 ], buff[ 3 ] );
	k.lines += Line2D( buff[ 9 ], buff[ 10 ] );
	k.lines += Line2D( buff[ 11 ], buff[ 12 ] );
	k.lines += Line2D( buff[ 0 ], buff[ 9 ] );
	k.lines += Line2D( buff[ 1 ], buff[ 5 ] );
	k.lines += Line2D( buff[ 7 ], buff[ 10 ] );
	k.lines += Line2D( buff[ 3 ], buff[ 4 ] );
	k.lines += Line2D( buff[ 8 ], buff[ 12 ] );
	k.lines += Line2D( buff[ 2 ], buff[ 5 ] );
	k.lines += Line2D( buff[ 4 ], buff[ 6 ] );
	k.lines += Line2D( buff[ 6 ], buff[ 8 ] );
	k.lines += Line2D( buff[ 7 ], buff[ 11 ] );
	buff[ 0 ] = Vector2( 0.0, 0.0 );
	buff[ 1 ] = Vector2( 48.0, 0.0 );
	buff[ 2 ] = Vector2( 336.0, 0.0 );
	buff[ 3 ] = Vector2( 384.0, 0.0 );
	buff[ 4 ] = Vector2( 192.0, 312.0 );
	buff[ 5 ] = Vector2( 168.0, 360.0 );
	buff[ 6 ] = Vector2( 216.0, 360.0 );
	buff[ 7 ] = Vector2( 168.0, 768.0 );
	buff[ 8 ] = Vector2( 216.0, 768.0 );
	y.lines += Line2D( buff[ 0 ], buff[ 1 ] );
	y.lines += Line2D( buff[ 2 ], buff[ 3 ] );
	y.lines += Line2D( buff[ 7 ], buff[ 8 ] );
	y.lines += Line2D( buff[ 5 ], buff[ 7 ] );
	y.lines += Line2D( buff[ 6 ], buff[ 8 ] );
	y.lines += Line2D( buff[ 0 ], buff[ 5 ] );
	y.lines += Line2D( buff[ 1 ], buff[ 4 ] );
	y.lines += Line2D( buff[ 2 ], buff[ 4 ] );
	y.lines += Line2D( buff[ 3 ], buff[ 6 ] );
	buff[ 0 ] = Vector2( 0.0, 0.0 );
	buff[ 1 ] = Vector2( 48.0, 0.0 );
	buff[ 2 ] = Vector2( 48.0, 696.0 );
	buff[ 3 ] = Vector2( 384.0, 696.0 );
	buff[ 4 ] = Vector2( 0.0, 768.0 );
	buff[ 5 ] = Vector2( 384.0, 768.0 );
	l.lines += Line2D( buff[ 0 ], buff[ 1 ] );
	l.lines += Line2D( buff[ 2 ], buff[ 3 ] );
	l.lines += Line2D( buff[ 4 ], buff[ 5 ] );
	l.lines += Line2D( buff[ 0 ], buff[ 4 ] );
	l.lines += Line2D( buff[ 1 ], buff[ 2 ] );
	l.lines += Line2D( buff[ 3 ], buff[ 5 ] );
	buff[ 0 ] = Vector2( 0.0, 0.0 );
	buff[ 1 ] = Vector2( 192.0, 0.0 );
	buff[ 2 ] = Vector2( 48.0, 72.0 );
	buff[ 3 ] = Vector2( 192.0, 72.0 );
	buff[ 4 ] = Vector2( 384.0, 168.0 );
	buff[ 5 ] = Vector2( 336.0, 192.0 );
	buff[ 6 ] = Vector2( 384.0, 216.0 );
	buff[ 7 ] = Vector2( 48.0, 336.0 );
	buff[ 8 ] = Vector2( 168.0, 336.0 );
	buff[ 9 ] = Vector2( 48.0, 408.0 );
	buff[ 10 ] = Vector2( 168.0, 408.0 );
	buff[ 11 ] = Vector2( 0.0, 768.0 );
	buff[ 12 ] = Vector2( 48.0, 768.0 );
	p.lines += Line2D( buff[ 0 ], buff[ 1 ] );
	p.lines += Line2D( buff[ 2 ], buff[ 3 ] );
	p.lines += Line2D( buff[ 7 ], buff[ 8 ] );
	p.lines += Line2D( buff[ 9 ], buff[ 10 ] );
	p.lines += Line2D( buff[ 11 ], buff[ 12 ] );
	p.lines += Line2D( buff[ 0 ], buff[ 11 ] );
	p.lines += Line2D( buff[ 2 ], buff[ 7 ] );
	p.lines += Line2D( buff[ 9 ], buff[ 12 ] );
	p.lines += Line2D( buff[ 1 ], buff[ 4 ] );
	p.lines += Line2D( buff[ 3 ], buff[ 5 ] );
	p.lines += Line2D( buff[ 5 ], buff[ 8 ] );
	p.lines += Line2D( buff[ 6 ], buff[ 10 ] );
	p.lines += Line2D( buff[ 4 ], buff[ 6 ] );
	buff[ 0 ] = Vector2( 0.0, 0.0 );
	buff[ 1 ] = Vector2( 48.0, 0.0 );
	buff[ 2 ] = Vector2( 336.0, 0.0 );
	buff[ 3 ] = Vector2( 384.0, 0.0 );
	buff[ 4 ] = Vector2( 48.0, 552.0 );
	buff[ 5 ] = Vector2( 0.0, 576.0 );
	buff[ 6 ] = Vector2( 192.0, 696.0 );
	buff[ 7 ] = Vector2( 336.0, 696.0 );
	buff[ 8 ] = Vector2( 192.0, 768.0 );
	buff[ 9 ] = Vector2( 384.0, 768.0 );
	u.lines += Line2D( buff[ 0 ], buff[ 1 ] );
	u.lines += Line2D( buff[ 2 ], buff[ 3 ] );
	u.lines += Line2D( buff[ 6 ], buff[ 7 ] );
	u.lines += Line2D( buff[ 8 ], buff[ 9 ] );
	u.lines += Line2D( buff[ 0 ], buff[ 5 ] );
	u.lines += Line2D( buff[ 1 ], buff[ 4 ] );
	u.lines += Line2D( buff[ 2 ], buff[ 7 ] );
	u.lines += Line2D( buff[ 3 ], buff[ 9 ] );
	u.lines += Line2D( buff[ 4 ], buff[ 6 ] );
	u.lines += Line2D( buff[ 5 ], buff[ 8 ] );
	buff[ 0 ] = Vector2( 336.0, 0.0 );
	buff[ 1 ] = Vector2( 384.0, 0.0 );
	buff[ 2 ] = Vector2( 384.0, 48.0 );
	buff[ 3 ] = Vector2( 0.0, 168.0 );
	buff[ 4 ] = Vector2( 48.0, 216.0 );
	buff[ 5 ] = Vector2( 48.0, 552.0 );
	buff[ 6 ] = Vector2( 0.0, 600.0 );
	buff[ 7 ] = Vector2( 384.0, 720.0 );
	buff[ 8 ] = Vector2( 336.0, 768.0 );
	buff[ 9 ] = Vector2( 384.0, 768.0 );
	c.lines += Line2D( buff[ 0 ], buff[ 1 ] );
	c.lines += Line2D( buff[ 1 ], buff[ 2 ] );
	c.lines += Line2D( buff[ 7 ], buff[ 9 ] );
	c.lines += Line2D( buff[ 8 ], buff[ 9 ] );
	c.lines += Line2D( buff[ 3 ], buff[ 6 ] );
	c.lines += Line2D( buff[ 4 ], buff[ 5 ] );
	c.lines += Line2D( buff[ 0 ], buff[ 3 ] );
	c.lines += Line2D( buff[ 2 ], buff[ 4 ] );
	c.lines += Line2D( buff[ 5 ], buff[ 7 ] );
	c.lines += Line2D( buff[ 6 ], buff[ 8 ] );
	buff[ 0 ] = Vector2( 0.0, 0.0 );
	buff[ 1 ] = Vector2( 384.0, 0.0 );
	buff[ 2 ] = Vector2( 0.0, 72.0 );
	buff[ 3 ] = Vector2( 168.0, 72.0 );
	buff[ 4 ] = Vector2( 216.0, 72.0 );
	buff[ 5 ] = Vector2( 384.0, 72.0 );
	buff[ 6 ] = Vector2( 168.0, 768.0 );
	buff[ 7 ] = Vector2( 216.0, 768.0 );
	t.lines += Line2D( buff[ 0 ], buff[ 1 ] );
	t.lines += Line2D( buff[ 2 ], buff[ 3 ] );
	t.lines += Line2D( buff[ 4 ], buff[ 5 ] );
	t.lines += Line2D( buff[ 6 ], buff[ 7 ] );
	t.lines += Line2D( buff[ 0 ], buff[ 2 ] );
	t.lines += Line2D( buff[ 3 ], buff[ 6 ] );
	t.lines += Line2D( buff[ 4 ], buff[ 7 ] );
	t.lines += Line2D( buff[ 1 ], buff[ 5 ] );
	for ( uint32 i = 0u; i < 13u; ++i )
		letters[ i ].OptimizeSpace();
}
Line2D::Line2D( void )
{
}
Line2D::Line2D( const Vector2& _start, const Vector2& _end ) :
	start( _start ), end( _end )
{
}
Line2D::Line2D( float64 _startX, float64 _startY, float64 _endX, float64 _endY ) :
	start( _startX, _startY ), end( _endX, _endY )
{
}
Line2D::Line2D( const Line2D& _rhs ) :
	start( _rhs.start ), end( _rhs.end )
{
}
Line2D::Line2D( Line2D&& _rhs ) :
	start( std::move( _rhs.start ) ), end( std::move( _rhs.end ) )
{
}
Line2D& Line2D::operator=( const Line2D& _rhs )
{
	if ( this != &_rhs )
	{
		start = _rhs.start;
		end = _rhs.end;
	}
	return *this;
}
Line2D& Line2D::operator=( Line2D&& _rhs )
{
	if ( this != &_rhs )
	{
		start = std::move( _rhs.start );
		end = std::move( _rhs.end );
	}
	return *this;
}
Line2D::~Line2D( void )
{
}
Line2D Line2D::operator+( const Vector2& _rhs ) const
{
	return Line2D( start + _rhs, end + _rhs );
}
Line2D& Line2D::operator+=( const Vector2& _rhs )
{
	start += _rhs;
	end += _rhs;
	return *this;
}
Line2D Line2D::operator-( const Vector2& _rhs ) const
{
	return Line2D( start - _rhs, end - _rhs );
}
Line2D& Line2D::operator-=( const Vector2& _rhs )
{
	start -= _rhs;
	end -= _rhs;
	return *this;
}
Line2D Line2D::operator*( float64 _rhs ) const
{
	return Line2D( start * _rhs, end * _rhs );
}
Line2D& Line2D::operator*=( float64 _rhs )
{
	start *= _rhs;
	end *= _rhs;
	return *this;
}
Line2D Line2D::operator/( float64 _rhs ) const
{
	_rhs = 1.0 / _rhs;
	return Line2D( start * _rhs, end * _rhs );
}
Line2D& Line2D::operator/=( float64 _rhs )
{
	_rhs = 1.0 / _rhs;
	start *= _rhs;
	end *= _rhs;
	return *this;
}
Line2D operator*( float64 _lhs, const Line2D& _rhs )
{
	return Line2D( _lhs * _rhs.start, _lhs * _rhs.end );
}
std::ostream& operator<<( std::ostream& _os, const Line2D& _rhs )
{
	return _os << '('
		<< _rhs.start.x << ',' << _rhs.start.y << ','
		<< _rhs.end.x << ',' << _rhs.end.y << ')';
}