#include "stdafx.h"
#include "Complex.h"
#include "Math.h"

Complex::Complex( void )
{
}
Complex::Complex( float64 _r, float64 _i ) :
	real( _r ), imaginary( _i )
{
}
Complex Complex::Conjugate( void ) const
{
	return Complex( real, -imaginary );
}
float64 Complex::Argument( void ) const
{
	return atan2( imaginary, real );
}
Complex Complex::Pow( const Complex& _e ) const
{
	const float64 t_ =
		_e.real * atan2( imaginary, real ) + 0.5 * _e.imaginary *
		log( real * real + imaginary * imaginary );
	return Complex( cos( t_ ), sin( t_ ) );
}
Complex Complex::Pow( float64 _e ) const
{
	const float64 t_ = _e * atan2( imaginary, real );
	return Complex( cos( t_ ), sin( t_ ) );
}
float64 Complex::Norm( void ) const
{
	return sqrt( real * real + imaginary * imaginary );
}
float64 Complex::NormSq( void ) const
{
	return real * real + imaginary * imaginary;
}
Complex Complex::Square( void ) const
{
	return Complex( real * real - imaginary * imaginary, ( real + real ) * imaginary );
}
Complex Complex::Conjugate( const Complex& _x )
{
	return _x.Conjugate();
}
float64 Complex::Argument( const Complex& _x )
{
	return _x.Argument();
}
Complex Complex::Pow( const Complex& _b, const Complex& _e )
{
	return _b.Pow( _e );
}
Complex Complex::Pow( const Complex& _b, float64 _e )
{
	return _b.Pow( _e );
}
Complex Complex::Pow( float64 _b, const Complex& _e )
{
	const float64 t_ = ( ( _b < 0.0 ) ? ( _e.real * Const_pi ) : ( 0.0 ) )
		+ 0.5 * _e.imaginary * log( _b * _b );
	return Complex( cos( t_ ), sin( t_ ) );
}
float64 Complex::Norm( const Complex& _x )
{
	return _x.Norm();
}
float64 Complex::NormSq( const Complex& _x )
{
	return _x.NormSq();
}
Complex Complex::Square( const Complex& _x )
{
	return _x.Square();
}
Complex Complex::operator+( const Complex& _rhs ) const
{
	return Complex( real + _rhs.real, imaginary + _rhs.imaginary );
}
Complex Complex::operator-( const Complex& _rhs ) const
{
	return Complex( real - _rhs.real, imaginary - _rhs.imaginary );
}
Complex Complex::operator*( const Complex& _rhs ) const
{
	return Complex( real * _rhs.real - imaginary * _rhs.imaginary,
					real * _rhs.imaginary + imaginary * _rhs.real );
}
Complex Complex::operator/( const Complex& _rhs ) const
{
	const float64& r_ = _rhs.real;
	const float64& i_ = _rhs.imaginary;
	const float64 d_ = 1.0 / ( r_ * r_ + i_ * i_ );
	return Complex(
		( real * r_ + imaginary * i_ ) * d_,
		( imaginary * r_ - real * i_ ) * d_ );
}
Complex Complex::operator+( void ) const
{
	return *this;
}
Complex Complex::operator-( void ) const
{
	return Complex( -real, -imaginary );
}
Complex& Complex::operator+=( const Complex& _rhs )
{
	real += _rhs.real;
	imaginary += _rhs.imaginary;
	return *this;
}
Complex& Complex::operator-=( const Complex& _rhs )
{
	real -= _rhs.real;
	imaginary -= _rhs.imaginary;
	return *this;
}
Complex& Complex::operator*=( const Complex& _rhs )
{
	return *this = Complex( real * _rhs.real - imaginary * _rhs.imaginary,
							real * _rhs.imaginary + imaginary * _rhs.real );
}
Complex& Complex::operator/=( const Complex& _rhs )
{
	return *this = *this / _rhs;
}
Complex Complex::operator+( float64 _rhs ) const
{
	return Complex( real + _rhs, imaginary );
}
Complex Complex::operator-( float64 _rhs ) const
{
	return Complex( real - _rhs, imaginary );
}
Complex Complex::operator*( float64 _rhs ) const
{
	return Complex( real * _rhs, imaginary * _rhs );
}
Complex Complex::operator/( float64 _rhs ) const
{
	_rhs = 1.0 / _rhs;
	return Complex( real * _rhs, imaginary * _rhs );
}
Complex& Complex::operator+=( float64 _rhs )
{
	real += _rhs;
	return *this;
}
Complex& Complex::operator-=( float64 _rhs )
{
	real -= _rhs;
	return *this;
}
Complex& Complex::operator*=( float64 _rhs )
{
	real *= _rhs;
	imaginary *= _rhs;
	return *this;
}
Complex& Complex::operator/=( float64 _rhs )
{
	_rhs = 1.0 / _rhs;
	real *= _rhs;
	imaginary *= _rhs;
	return *this;
}
bool Complex::operator==( const Complex& _rhs ) const
{
	return real == _rhs.real && imaginary == _rhs.imaginary;
}
bool Complex::operator!=( const Complex& _rhs ) const
{
	return real != _rhs.real || imaginary != _rhs.imaginary;
}
bool Complex::operator<( const Complex& _rhs ) const
{
	return NormSq() < _rhs.NormSq();
}
bool Complex::operator>( const Complex& _rhs ) const
{
	return NormSq() > _rhs.NormSq();
}
bool Complex::operator<=( const Complex& _rhs ) const
{
	return NormSq() <= _rhs.NormSq();
}
bool Complex::operator>=( const Complex& _rhs ) const
{
	return NormSq() >= _rhs.NormSq();
}
bool Complex::operator==( float64 _rhs ) const
{
	return NormSq() == _rhs * _rhs;
}
bool Complex::operator!=( float64 _rhs ) const
{
	return NormSq() != _rhs * _rhs;
}
bool Complex::operator<( float64 _rhs ) const
{
	return NormSq() < _rhs * _rhs;
}
bool Complex::operator>( float64 _rhs ) const
{
	return NormSq() > _rhs * _rhs;
}
bool Complex::operator<=( float64 _rhs ) const
{
	return NormSq() <= _rhs * _rhs;
}
bool Complex::operator>=( float64 _rhs ) const
{
	return NormSq() <= _rhs * _rhs;
}
Complex operator+( float64 _lhs, const Complex& _rhs )
{
	return Complex( _lhs + _rhs.real, _rhs.imaginary );
}
Complex operator-( float64 _lhs, const Complex& _rhs )
{
	return Complex( _lhs - _rhs.real, -_rhs.imaginary );
}
Complex operator*( float64 _lhs, const Complex& _rhs )
{
	return Complex( _lhs * _rhs.real, _lhs * _rhs.imaginary );
}
Complex operator/( float64 _lhs, const Complex& _rhs )
{
	const float64& r_ = _rhs.real;
	const float64& i_ = _rhs.imaginary;
	const float64 d_ = 1.0 / ( r_ * r_ + i_ * i_ );
	return Complex( ( _lhs * r_ ) * d_, ( ( -_lhs ) * i_ ) * d_ );
}
bool operator==( float64 _lhs, const Complex& _rhs )
{
	return _lhs * _lhs == _rhs.NormSq();
}
bool operator!=( float64 _lhs, const Complex& _rhs )
{
	return _lhs * _lhs != _rhs.NormSq();
}
bool operator<( float64 _lhs, const Complex& _rhs )
{
	return _lhs * _lhs < _rhs.NormSq();
}
bool operator>( float64 _lhs, const Complex& _rhs )
{
	return _lhs * _lhs > _rhs.NormSq();
}
bool operator<=( float64 _lhs, const Complex& _rhs )
{
	return _lhs * _lhs <= _rhs.NormSq();
}
bool operator>=( float64 _lhs, const Complex& _rhs )
{
	return _lhs * _lhs >= _rhs.NormSq();
}
std::ostream& operator<<( std::ostream& _os, const Complex& _rhs )
{
	const float64& real_ = _rhs.real;
	const float64& imaginary_ = _rhs.imaginary;
	if ( real_ < -0.0 || real_ > 0.0 )
		_os << real_;
	else if ( imaginary_ >= -0.0 && imaginary_ <= 0.0 )
		return _os << 0.0;
	if ( ( real_ < -0.0 || real_ > 0.0 ) && imaginary_ > 0.0 )
		_os << '+';
	else if ( imaginary_ >= -0.0 && imaginary_ <= 0.0 )
		return _os;
	return _os << imaginary_;
}