#pragma once
#include "DynArray.h"

class String
{
private:
	DynArray<char> m_str;
public:
	String( void );
	String( const String& );
	String( String&& );
	String( const char* const );
	String( char );
	String& operator=( const String& );
	String& operator=( String&& );
	~String( void );
	String operator+( const String& ) const;
	String& operator+=( const String& );
	String operator*( uint32 ) const;
	friend String operator*( uint32, const String& );
	String& operator*=( uint32 );
	const char& operator[]( uint32 ) const;
	char& operator[]( uint32 );
	const char* const GetString( void ) const;
	friend std::ostream& operator<<( std::ostream&, const String& );
	uint32 GetLength( void ) const;
	String& OptimizeSpace( void );
	bool operator==( const String& ) const;
	bool operator!=( const String& ) const;
	bool operator<( const String& ) const;
	bool operator<=( const String& ) const;
	bool operator>( const String& ) const;
	bool operator>=( const String& ) const;
};