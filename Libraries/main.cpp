#include "stdafx.h"
#if 1
#include "List.h"
extern int MinesweeperMain( int, char** );
int main( int argc, char** argv )
{
	List<float32> lol;
	lol += 69.0f;
	std::cout << **lol;
	lol.Clear();
	return MinesweeperMain( argc, argv );
}
#elif 0
#include "Queue.h"
#include "Stack.h"
struct nope
{
private:
	int dat;
public:
	nope( int );
	nope( const nope& );
	friend std::ostream& operator<<( std::ostream&, const nope& );
};
nope::nope( int d ) : dat( d )
{
}
nope::nope( const nope& r ) : dat( r.dat )
{
}
std::ostream& operator<<( std::ostream& _os, const nope& _n )
{
	return _os << _n.dat;
}
int main( int, char** )
{
	for ( uint32 i = 0u; i < 100u; ++i )
	{
		Queue<Queue<int>> plebs;
		plebs.Clear();
		plebs.Enqueue( Queue<int>() );
		plebs.Front().Enqueue( 17 );
		Queue<Queue<int>> plebs2;
		plebs2.Enqueue( plebs.Front() );
		plebs2 = plebs;
		std::cout << plebs.Front().Front() << std::endl;
		plebs.Clear();
		std::cout << plebs2.Front().Front() << std::endl;
		plebs2.Clear();
	}

	const Queue<nope> q1;
	Queue<nope> q2( q1 );
	Queue<nope> q3( std::move( q2 ) );
	Queue<nope> q4, q5;
	q4 = q3;
	q5 = std::move( q4 );
	q5.Clear();
	nope i1 = 15, i2 = 17;
	q5 += i1;
	q3.Enqueue( std::move( i2 ) );
	q3.Dequeue();
	std::cout << q5.Front();
	const Queue<nope> q6 = q5;
	std::cout << q6.Front();
	std::cout << q6.GetSize();
	auto r1 = q5.Begin();
	auto r2 = q6.Begin();
	std::cout << r2.Get();
	q5.Enqueue( 88 );
	q5.Remove( r1 );
	std::cout << q5.Front() << r1.GetConstIterator().Get();
	q5.Enqueue( 99 );
	r1 = q5.Begin().Next();
	q5.Insert( r1, i1 );
	q5.Insert( r1, std::move( i1 ) );
	std::cout << r1.Next().Get() << std::endl;
	std::cout << "Size: " << q5.GetSize() << std::endl;
	for ( Queue<nope>::ConstIterator iter = q5.Begin(); !iter.End(); ++iter )
		std::cout << iter.Get() << std::endl;
	Stack<nope> nopes;
	nopes.Push( 17 );
	nopes.Push( 12 );
	std::cout << "Stack Test (17):" << nopes.Begin().GetConstIterator().Next().Get() << std::endl;
	return EXIT_SUCCESS;
}
#elif 0
#include <queue>
static unsigned long long callCount_Uint16ToTTTBoard = 0ull;
static unsigned long long callCount_TTTBoardToUint16 = 0ull;
static unsigned long long callCount_GetTileChar = 0ull;
static unsigned long long callCount_PrintBoard = 0ull;
static unsigned long long callCount_GetNumXTiles = 0ull;
static unsigned long long callCount_GetNumOTiles = 0ull;
static unsigned long long callCount_GetNumEmptyTiles = 0ull;
static unsigned long long callCount_WhoseTurn = 0ull;
static unsigned long long callCount_IsXWin = 0ull;
static unsigned long long callCount_IsOWin = 0ull;
static unsigned long long callCount_IsEndGame = 0ull;
static unsigned long long callCount_GetNextStates = 0ull;
static unsigned long long callCount_TicTacToeBoard_DefaultCtor = 0ull;
static unsigned long long callCount_TicTacToeBoard_CopyCtor = 0ull;
static unsigned long long callCount_TicTacToeBoard_MoveCtor = 0ull;
static unsigned long long callCount_TicTacToeBoard_CopyAssign = 0ull;
static unsigned long long callCount_TicTacToeBoard_MoveAssign = 0ull;
static unsigned long long callCount_TicTacToeBoard_Dtor = 0ull;
enum TicTacToeTile
{
	TTT_EMPTY = 0, TTT_X = 1, TTT_O = 2
};
class TicTacToeBoard
{
public:
	TicTacToeTile* m_tiles;
	TicTacToeBoard( void );
	TicTacToeBoard( const TicTacToeBoard& );
	TicTacToeBoard( TicTacToeBoard&& );
	TicTacToeBoard& operator=( const TicTacToeBoard& );
	TicTacToeBoard& operator=( TicTacToeBoard&& );
	~TicTacToeBoard( void );
};
static TicTacToeBoard Uint16ToTTTBoard( uint16 _inBoard )
{
	++callCount_Uint16ToTTTBoard;
	TicTacToeBoard outBoard_;
	outBoard_.m_tiles[ 8u ] = ( TicTacToeTile )( _inBoard % 3ui16 );
	for ( int8 i = 7i8; i >= 0i8; --i )
		outBoard_.m_tiles[ i ] = ( TicTacToeTile )( ( _inBoard /= 3ui16 ) % 3ui16 );
	return outBoard_;
}
static uint16 TTTBoardToUint16( const TicTacToeBoard& _inBoard )
{
	++callCount_TTTBoardToUint16;
	uint16 outBoard_ = ( uint16 )( _inBoard.m_tiles[ 0u ] );
	for ( uint8 i = 1ui8; i < 9ui8; ++i )
		outBoard_ += ( outBoard_ << 1ui16 ) + ( uint16 )( _inBoard.m_tiles[ i ] );
	return outBoard_;
}
static char GetTileChar( TicTacToeTile _tile )
{
	++callCount_GetTileChar;
	switch ( _tile )
	{
		case TTT_EMPTY:
			return ' ';
		case TTT_X:
			return 'X';
		case TTT_O:
			return 'O';
		default:
			return '\0';
	}
}
static void PrintBoard( const TicTacToeBoard& _board )
{
	++callCount_PrintBoard;
	std::cout
		<< GetTileChar( _board.m_tiles[ 0 ] ) << '|' << GetTileChar( _board.m_tiles[ 1 ] ) << '|' << GetTileChar( _board.m_tiles[ 2 ] ) << "\n-+-+-\n"
		<< GetTileChar( _board.m_tiles[ 3 ] ) << '|' << GetTileChar( _board.m_tiles[ 4 ] ) << '|' << GetTileChar( _board.m_tiles[ 5 ] ) << "\n-+-+-\n"
		<< GetTileChar( _board.m_tiles[ 6 ] ) << '|' << GetTileChar( _board.m_tiles[ 7 ] ) << '|' << GetTileChar( _board.m_tiles[ 8 ] );
}
static uint8 GetNumXTiles( const TicTacToeBoard& _board )
{
	++callCount_GetNumXTiles;
	uint8 numTiles_ = 0ui8;
	for ( uint8 i = 0ui8; i < 9ui8; ++i )
		if ( TicTacToeTile::TTT_X == _board.m_tiles[ i ] )
			++numTiles_;
	return numTiles_;
}
static uint8 GetNumOTiles( const TicTacToeBoard& _board )
{
	++callCount_GetNumOTiles;
	uint8 numTiles_ = 0ui8;
	for ( uint8 i = 0ui8; i < 9ui8; ++i )
		if ( TicTacToeTile::TTT_O == _board.m_tiles[ i ] )
			++numTiles_;
	return numTiles_;
}
static uint8 GetNumEmptyTiles( const TicTacToeBoard& _board )
{
	++callCount_GetNumEmptyTiles;
	uint8 numTiles_ = 0ui8;
	for ( uint8 i = 0ui8; i < 9ui8; ++i )
		if ( TicTacToeTile::TTT_EMPTY == _board.m_tiles[ i ] )
			++numTiles_;
	return numTiles_;
}
static TicTacToeTile WhoseTurn( const TicTacToeBoard& _board )
{
	++callCount_WhoseTurn;
	return GetNumXTiles( _board ) == GetNumOTiles( _board ) ? TicTacToeTile::TTT_X : TicTacToeTile::TTT_O;
}
static bool IsXWin( const TicTacToeBoard& _board )
{
	++callCount_IsXWin;
	return (
		( TicTacToeTile::TTT_X == _board.m_tiles[ 0u ] && TicTacToeTile::TTT_X == _board.m_tiles[ 1u ] && TicTacToeTile::TTT_X == _board.m_tiles[ 2u ] ) ||
		( TicTacToeTile::TTT_X == _board.m_tiles[ 3u ] && TicTacToeTile::TTT_X == _board.m_tiles[ 4u ] && TicTacToeTile::TTT_X == _board.m_tiles[ 5u ] ) ||
		( TicTacToeTile::TTT_X == _board.m_tiles[ 6u ] && TicTacToeTile::TTT_X == _board.m_tiles[ 7u ] && TicTacToeTile::TTT_X == _board.m_tiles[ 8u ] ) ||
		( TicTacToeTile::TTT_X == _board.m_tiles[ 0u ] && TicTacToeTile::TTT_X == _board.m_tiles[ 3u ] && TicTacToeTile::TTT_X == _board.m_tiles[ 6u ] ) ||
		( TicTacToeTile::TTT_X == _board.m_tiles[ 1u ] && TicTacToeTile::TTT_X == _board.m_tiles[ 4u ] && TicTacToeTile::TTT_X == _board.m_tiles[ 7u ] ) ||
		( TicTacToeTile::TTT_X == _board.m_tiles[ 2u ] && TicTacToeTile::TTT_X == _board.m_tiles[ 5u ] && TicTacToeTile::TTT_X == _board.m_tiles[ 8u ] ) ||
		( TicTacToeTile::TTT_X == _board.m_tiles[ 0u ] && TicTacToeTile::TTT_X == _board.m_tiles[ 4u ] && TicTacToeTile::TTT_X == _board.m_tiles[ 8u ] ) ||
		( TicTacToeTile::TTT_X == _board.m_tiles[ 2u ] && TicTacToeTile::TTT_X == _board.m_tiles[ 4u ] && TicTacToeTile::TTT_X == _board.m_tiles[ 6u ] ) );
}
static bool IsOWin( const TicTacToeBoard& _board )
{
	++callCount_IsOWin;
	return (
		( TicTacToeTile::TTT_O == _board.m_tiles[ 0u ] && TicTacToeTile::TTT_O == _board.m_tiles[ 1u ] && TicTacToeTile::TTT_O == _board.m_tiles[ 2u ] ) ||
		( TicTacToeTile::TTT_O == _board.m_tiles[ 3u ] && TicTacToeTile::TTT_O == _board.m_tiles[ 4u ] && TicTacToeTile::TTT_O == _board.m_tiles[ 5u ] ) ||
		( TicTacToeTile::TTT_O == _board.m_tiles[ 6u ] && TicTacToeTile::TTT_O == _board.m_tiles[ 7u ] && TicTacToeTile::TTT_O == _board.m_tiles[ 8u ] ) ||
		( TicTacToeTile::TTT_O == _board.m_tiles[ 0u ] && TicTacToeTile::TTT_O == _board.m_tiles[ 3u ] && TicTacToeTile::TTT_O == _board.m_tiles[ 6u ] ) ||
		( TicTacToeTile::TTT_O == _board.m_tiles[ 1u ] && TicTacToeTile::TTT_O == _board.m_tiles[ 4u ] && TicTacToeTile::TTT_O == _board.m_tiles[ 7u ] ) ||
		( TicTacToeTile::TTT_O == _board.m_tiles[ 2u ] && TicTacToeTile::TTT_O == _board.m_tiles[ 5u ] && TicTacToeTile::TTT_O == _board.m_tiles[ 8u ] ) ||
		( TicTacToeTile::TTT_O == _board.m_tiles[ 0u ] && TicTacToeTile::TTT_O == _board.m_tiles[ 4u ] && TicTacToeTile::TTT_O == _board.m_tiles[ 8u ] ) ||
		( TicTacToeTile::TTT_O == _board.m_tiles[ 2u ] && TicTacToeTile::TTT_O == _board.m_tiles[ 4u ] && TicTacToeTile::TTT_O == _board.m_tiles[ 6u ] ) );
}
static bool IsEndGame( const TicTacToeBoard& _board, TicTacToeTile& _winState )
{
	++callCount_IsEndGame;
	if ( IsXWin( _board ) )
	{
		_winState = TicTacToeTile::TTT_X;
		return true;
	}
	else if ( IsOWin( _board ) )
	{
		_winState = TicTacToeTile::TTT_O;
		return true;
	}
	else if ( 0ui8 == GetNumEmptyTiles( _board ) )
	{
		_winState = TicTacToeTile::TTT_EMPTY;
		return true;
	}
	return false;
}
static std::vector<TicTacToeBoard> GetNextStates( const TicTacToeBoard& _board )
{
	++callCount_GetNextStates;
	std::vector<TicTacToeBoard> nextStates_;
	const TicTacToeTile tile_ = WhoseTurn( _board );
	nextStates_.reserve( ( size_t )GetNumEmptyTiles( _board ) );
	TicTacToeBoard board_;
	for ( uint8 i = 0ui8; i < 9ui8; ++i ) if ( TicTacToeTile::TTT_EMPTY == _board.m_tiles[ i ] )
	{
		board_ = _board;
		board_.m_tiles[ i ] = tile_;
		nextStates_.push_back( board_ );
	}
	return nextStates_;
}
int main( int, char** )
{
	std::queue<uint16> boardQueue_;
	uint32 numDraws_ = 0u, numXWins_ = 0u, numOWins_ = 0u;
	TicTacToeTile winState_;
	TicTacToeBoard board_ = Uint16ToTTTBoard( 0ui16 );
	boardQueue_.push( TTTBoardToUint16( board_ ) );
	while ( !boardQueue_.empty() )
	{
		board_ = Uint16ToTTTBoard( boardQueue_.front() );
		if ( IsEndGame( board_, winState_ ) ) switch ( winState_ )
		{
			case TTT_EMPTY:
				++numDraws_;
				break;
			case TTT_X:
				++numXWins_;
				break;
			case TTT_O:
				++numOWins_;
		}
		else
		{
			const std::vector<TicTacToeBoard> nextStates_ = GetNextStates( board_ );
			const uint32 numNextStates_ = ( uint32 )nextStates_.size();
			for ( uint32 i = 0u; i < numNextStates_; ++i )
				boardQueue_.push( TTTBoardToUint16( nextStates_[ i ] ) );
		}
		boardQueue_.pop();
	}
	std::cout << "Draws: " << numDraws_ << "\nX Wins: " << numXWins_ << "\nO Wins: " << numOWins_ << std::endl;

	std::cout << "Uint16ToTTTBoard:" << callCount_Uint16ToTTTBoard << std::endl;
	std::cout << "TTTBoardToUint16:" << callCount_TTTBoardToUint16 << std::endl;
	std::cout << "GetTileChar:" << callCount_GetTileChar << std::endl;
	std::cout << "PrintBoard:" << callCount_PrintBoard << std::endl;
	std::cout << "GetNumXTiles:" << callCount_GetNumXTiles << std::endl;
	std::cout << "GetNumOTiles:" << callCount_GetNumOTiles << std::endl;
	std::cout << "GetNumEmptyTiles:" << callCount_GetNumEmptyTiles << std::endl;
	std::cout << "WhoseTurn:" << callCount_WhoseTurn << std::endl;
	std::cout << "IsXWin:" << callCount_IsXWin << std::endl;
	std::cout << "IsOWin:" << callCount_IsOWin << std::endl;
	std::cout << "IsEndGame:" << callCount_IsEndGame << std::endl;
	std::cout << "GetNextStates:" << callCount_GetNextStates << std::endl;
	std::cout << "TicTacToeBoard_DefaultCtor:" << callCount_TicTacToeBoard_DefaultCtor << std::endl;
	std::cout << "TicTacToeBoard_CopyCtor:" << callCount_TicTacToeBoard_CopyCtor << std::endl;
	std::cout << "TicTacToeBoard_MoveCtor:" << callCount_TicTacToeBoard_MoveCtor << std::endl;
	std::cout << "TicTacToeBoard_CopyAssign:" << callCount_TicTacToeBoard_CopyAssign << std::endl;
	std::cout << "TicTacToeBoard_MoveAssign:" << callCount_TicTacToeBoard_MoveAssign << std::endl;
	std::cout << "TicTacToeBoard_Dtor:" << callCount_TicTacToeBoard_Dtor << std::endl;

	system( "pause" );
	return EXIT_SUCCESS;
}
TicTacToeBoard::TicTacToeBoard( void )
{
	++callCount_TicTacToeBoard_DefaultCtor;
	m_tiles = new TicTacToeTile[ 9u ];
	for ( uint8 i = 0ui8; i < 9ui8; ++i )
		m_tiles[ i ] = TicTacToeTile::TTT_EMPTY;
}
TicTacToeBoard::TicTacToeBoard( const TicTacToeBoard& _rhs )
{
	++callCount_TicTacToeBoard_CopyCtor;
	m_tiles = new TicTacToeTile[ 9u ];
	for ( uint8 i = 0ui8; i < 9ui8; ++i )
		m_tiles[ i ] = _rhs.m_tiles[ i ];
}
TicTacToeBoard::TicTacToeBoard( TicTacToeBoard&& _rhs )
{
	++callCount_TicTacToeBoard_MoveCtor;
	m_tiles = _rhs.m_tiles;
	_rhs.m_tiles = nullptr;
}
TicTacToeBoard& TicTacToeBoard::operator=( const TicTacToeBoard& _rhs )
{
	++callCount_TicTacToeBoard_CopyAssign;
	if ( this != &_rhs )
	{
		for ( uint8 i = 0ui8; i < 9ui8; ++i )
			m_tiles[ i ] = _rhs.m_tiles[ i ];
	}
	return *this;
}
TicTacToeBoard& TicTacToeBoard::operator=( TicTacToeBoard&& _rhs )
{
	++callCount_TicTacToeBoard_MoveAssign;
	if ( this != &_rhs )
	{
		delete[ ] m_tiles;
		m_tiles = _rhs.m_tiles;
		_rhs.m_tiles = nullptr;
	}
	return *this;
}
TicTacToeBoard::~TicTacToeBoard( void )
{
	++callCount_TicTacToeBoard_Dtor;
	delete[ ] m_tiles;
}
#elif 0
#define MINESWEEPER_IS_PRESSED (0b00010000ui8)
#define MINESWEEPER_IS_MINE (0b00100000ui8)
#define MINESWEEPER_IS_FLAG (0b01000000ui8)
#define MINESWEEPER_PREINIT (0b10000000ui8)
#define MINESWEEPER_WIDTH (30u)
#define MINESWEEPER_HEIGHT (16u)
#define MINESWEEPER_NUM_MINES (99u)
#define MINESWEEPER_NUM_TILES ((MINESWEEPER_WIDTH)*(MINESWEEPER_HEIGHT))
class Minesweeper
{
private:
	uint8* m_board;
	uint32 m_rows, m_cols, m_numMines, m_numFlags, m_numPressed;
	void PreInitBoard( void );
	void PostInitBoard( uint32, uint32 );
public:
	Minesweeper( void );
	Minesweeper( uint32, uint32, uint32 = 0u );
	Minesweeper( const Minesweeper& );
	Minesweeper( Minesweeper&& );
	Minesweeper& operator=( const Minesweeper& );
	Minesweeper& operator=( Minesweeper&& );
	~Minesweeper( void );
	void ClearBoard( void );
	void InitMines( uint32 );
	void ResetSize( uint32, uint32 );
	uint32 GetWidth( void ) const;
	uint32 GetHeight( void ) const;
	uint32 GetTotalNumMines( void ) const;
	uint32 GetNumFlags( void ) const;
	uint32 GetNumPressed( void ) const;
	void ClearFlags( void );
	bool IsMine( uint32, uint32 ) const;
	bool IsFlag( uint32, uint32 ) const;
	bool IsPressed( uint32, uint32 ) const;
	uint32 GetNum( uint32, uint32 ) const;
	void SetMine( uint32, uint32, bool = true );
	void SetFlag( uint32, uint32, bool = true );
	void SetPressed( uint32, uint32, bool = true );
	void SpreadPress( uint32, uint32 );
	void SpecialPress( uint32, uint32 );
};
int main( int, char** )
{
	Minesweeper minesweeper_( MINESWEEPER_HEIGHT, MINESWEEPER_WIDTH, MINESWEEPER_NUM_MINES );
	return EXIT_SUCCESS;
}
#elif 0
inline void SwapChar( char& _a, char& _b )
{
	_a ^= _b;
	_b ^= _a;
	_a ^= _b;
}
inline std::string& ReverseString( std::string& _str )
{
	const uint32 length_ = ( uint32 )( _str.length() );
	for ( uint32 i = 0u; i < ( length_ >> 1u ); ++i )
		SwapChar( _str[ i ], _str[ length_ - i - 1u ] );
	return _str;
}
std::string ConvertBase( uint32 _num, const std::string& _numerals )
{
	const uint32 base_ = ( uint32 )( _numerals.length() );
	if ( 0u == base_ )
		return "";
	if ( 0u == _num || 1u == base_ )
		return std::string() + _numerals[ 0u ];
	std::string outString_ = "";
	while ( 0u != _num )
	{
		outString_ += _numerals[ _num % base_ ];
		_num /= base_;
	}
	return ReverseString( outString_ );
}
inline std::string GetDoz( uint32 _num )
{
	return ConvertBase( _num, "0123456789XE" );
}
int main( int, char** )
{
	std::ofstream file_;
	file_.open( "DozenalStuff.txt", std::ios_base::out | std::ios_base::trunc );
	if ( !file_.is_open() )
		return EXIT_FAILURE;
	uint32 a, b;
	for ( a = 1u; a <= 18u; ++a )
	{
		for ( b = 1u; b <= 18u; ++b )
		{
			std::cout << GetDoz( a * b ) << '\t';
			file_ << GetDoz( a * b ) << '\t';
		}
		std::cout << std::endl;
		file_ << std::endl;
	}
	file_.close();
	return EXIT_SUCCESS;
}
#elif 0
#include "Raster.h"
#include "TextRenderer.h"
#include "Memory.h"
#include "Random.h"
#define RASTER_WIDTH (1910u)
#define RASTER_HEIGHT (1055u)
int main( int, char** )
{
	Raster::InitWindow( RASTER_WIDTH, RASTER_HEIGHT );
	Raster::GetTexture() = Color( 0xffffffffu );
	Raster::GetTexture().SetPixel( 0u, 2u, Color( 0xcdcdcdcdu ) );
	uint32 i;
	do
	{
		Texture2D::TextureToTexture( TextRenderer( AsciiFont( AsciiFont::GenerateAscfFileFromBmp( "FONT3.bmp" ),
									 Color( g_random.GetRandomUint32() ), Color( g_random.GetRandomUint32() ) ) ).MakeTextBlock(
									 ( char* )( &Raster::GetTexture().GetPixel_Ref( g_random.GetRandom( 0u, RASTER_WIDTH ), g_random.GetRandom( 0u, RASTER_HEIGHT ) ) ),
									 RASTER_WIDTH, RASTER_HEIGHT ), Raster::GetTexture() );
		for ( i = 0u; i < ( RASTER_HEIGHT << 1u ); ++i )
			( &Raster::GetTexture().GetPixel_Ref( 0u, 0u ) )[ g_random.GetRandom( 0u, ( ( RASTER_WIDTH )*( RASTER_HEIGHT ) ) - 1u ) ].m_argb = g_random.GetRandomUint32();
		for ( i = 0u; i < ( RASTER_HEIGHT << 1u ); ++i )
			( &Raster::GetTexture().GetPixel_Ref( 0u, 0u ) )[ g_random.GetRandom( 0u, ( ( RASTER_WIDTH )*( RASTER_HEIGHT ) ) - 1u ) ].m_argb = 0x00000000u;
	} while ( Raster::UpdateWindow() );
	Raster::ShutdownWindow();
	return EXIT_SUCCESS;
}
#elif 1
#include "Texture2D.h"
extern const float64& Const_sqrt2Div2;
void ProcessFile( std::string );
int main( int argc, char** argv )
{
	for ( int i = 1; i < argc; ++i )
		ProcessFile( argv[ i ] );
	return EXIT_SUCCESS;
}
void ProcessFile( std::string _filename )
{
	Texture2D image1_ = Texture2D( _filename );
	Texture2D image2_ = image1_, image3_ = image1_;
	const uint32 width_ = image1_.GetWidth();
	if ( 0u == width_ )
		return;
	const uint32 height_ = image1_.GetHeight();
	static const float64 threshold1_ = 1.0 - Const_sqrt2Div2;
	static const float64 threshold2_ = 0.5;
	static const float64 threshold3_ = Const_sqrt2Div2;
	float64 lightness_;
	uint32 y_, x_; for ( y_ = 0u; y_ < height_; ++y_ ) for ( x_ = 0u; x_ < width_; ++x_ )
	{
		lightness_ = HSLColor( image1_.GetPixel_Ref( x_, y_ ) ).lightness;
		image1_.SetPixel( x_, y_, ( ( lightness_ >= threshold1_ ) ? ( Color::WHITE ) : ( Color::BLACK ) ) );
		image2_.SetPixel( x_, y_, ( ( lightness_ >= threshold2_ ) ? ( Color::WHITE ) : ( Color::BLACK ) ) );
		image3_.SetPixel( x_, y_, ( ( lightness_ >= threshold3_ ) ? ( Color::WHITE ) : ( Color::BLACK ) ) );
	}
	uint32 x = ( uint32 )_filename.length();
	_filename[ x - 1u ] = 'N';
	_filename[ x - 2u ] = 'O';
	_filename[ x - 3u ] = 'M';
	_filename[ x - 4u ] = '_';
	_filename += "OCHROME";
	image1_.WriteToBmp( _filename + "1.bmp" );
	image2_.WriteToBmp( _filename + "2.bmp" );
	image3_.WriteToBmp( _filename + "3.bmp" );
}
#elif 1
int main( int argc, char** argv )
{
	for ( int i = 0; i < argc; ++i )
		std::cout << "Argument " << i << ":\n\t" << argv[ i ] << std::endl;
	system( "pause" );
	return EXIT_SUCCESS;
}
#elif 1
#include "Raster.h"
#include "Random.h"
#include "AsciiFont.h"
int main( int, char** )
{
	AsciiFont font_ = AsciiFont( AsciiFont::GenerateAscfFileFromBmp( "FONT3.bmp" ) );
	Raster::InitWindow( 960u, 540u );
	for ( uint32 i = 0u; i < 6000u; ++i )
	{
		Texture2D::TextureToTexture( font_.GetCharacter( g_random.GetRandom( 'A', 'Z' ) ), Raster::GetTexture(),
									 g_random.GetRandom( 0u, 960u ), g_random.GetRandom( 0u, 540u ) );
		if ( !Raster::UpdateWindow() )
			break;
		Sleep( 10ul );
	}
	system( "pause" );
	Raster::ShutdownWindow();
	return EXIT_SUCCESS;
}
#elif 1
#include "Texture2D.h"
extern const float64& Const_sqrt2Div2;
int main( int argc, char** argv )
{
	if ( 2u != argc )
		return EXIT_FAILURE;
	std::string filename_ = argv[ 1u ];
	Texture2D image1_ = Texture2D( filename_ );
	Texture2D image2_ = image1_, image3_ = image1_;
	const uint32 width_ = image1_.GetWidth();
	if ( 0u == width_ )
		return EXIT_FAILURE;
	const uint32 height_ = image1_.GetHeight();
	static const float64 threshold1_ = 1.0 - Const_sqrt2Div2;
	static const float64 threshold2_ = 0.5;
	static const float64 threshold3_ = Const_sqrt2Div2;
	float64 ligtness_;
	uint32 y_, x_; for ( y_ = 0u; y_ < height_; ++y_ ) for ( x_ = 0u; x_ < width_; ++x_ )
	{
		float64 lightness_ = HSLColor( image1_.GetPixel_Ref( x_, y_ ) ).lightness;
		image1_.SetPixel( x_, y_, ( ( lightness_ >= threshold1_ ) ? ( Color::WHITE ) : ( Color::BLACK ) ) );
		image2_.SetPixel( x_, y_, ( ( lightness_ >= threshold2_ ) ? ( Color::WHITE ) : ( Color::BLACK ) ) );
		image3_.SetPixel( x_, y_, ( ( lightness_ >= threshold3_ ) ? ( Color::WHITE ) : ( Color::BLACK ) ) );
	}
	uint32 x = ( uint32 )filename_.length();
	filename_[ x - 1u ] = 'N';
	filename_[ x - 2u ] = 'O';
	filename_[ x - 3u ] = 'M';
	filename_[ x - 4u ] = '_';
	filename_ += "OCHROME";
	image1_.WriteToBmp( filename_ + "1.bmp" );
	image2_.WriteToBmp( filename_ + "2.bmp" );
	image3_.WriteToBmp( filename_ + "3.bmp" );
	return EXIT_SUCCESS;
}
#elif 1
#include <chrono>
int main( int, char** )
{
	long long times[ 64ull ];
	Sleep( 10000ul ); times[ 0ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 1ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 2ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 3ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 4ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 5ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 6ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 7ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 8ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 9ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 10ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 11ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 12ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 13ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 14ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 15ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 16ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 17ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 18ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 19ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 20ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 21ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 22ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 23ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 24ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 25ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 26ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 27ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 28ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 29ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 30ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 31ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 32ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 33ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 34ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 35ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 36ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 37ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 38ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 39ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 40ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 41ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 42ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 43ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 44ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 45ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 46ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 47ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 48ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 49ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 50ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 51ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 52ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 53ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 54ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 55ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 56ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 57ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 58ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 59ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 60ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 61ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 62ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 10000ul ); times[ 63ull ] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	for ( uint64 i16 = 1ull; i16 < 64ull; ++i16 )
		std::cout << times[ i16 ] << " (" << ( times[ i16 ] - times[ i16 - 1ull ] ) << ")\n";
	float64 avg_ = 0.0;
	for ( uint64 i16 = 1ull; i16 < 64ull; ++i16 )
		avg_ += ( float64 )( times[ i16 ] - times[ i16 - 1ull ] );
	avg_ /= 630.0;
	const long long avgll_ = ( long long )avg_;
	std::cout << "\nPer Second (AVG) = " << avgll_ << std::endl;
	system( "pause" );
	return EXIT_SUCCESS;
}
#elif 1
#include "Texture2D.h"
#include "BitmapImage.h"
#include "Avi.h"
#include "Math.h"

#define VALL (0.999)
#define RASTERSIZE (2000u)
static const float64 markiplier = RASTERSIZE / 60.0;
static const uint64 maxCount = ( uint64 )( 10000ull * 3.1415926535897932384626433832795 * markiplier / VALL );
#define RASTER_WIDTH (RASTERSIZE)
#define RASTER_HEIGHT (RASTERSIZE)

Color getColor( uint64 count )
{
	const float64 t = ( float64 )count / ( float64 )maxCount;
	RGBAColor pix;
	pix.alpha = 1.0;
	pix.green = 0.0;

	if ( t <= 0.25 || t >= 0.75 ) pix.blue = 1.0;
	else if ( t < 0.5 ) pix.blue = ( 0.5 - t ) * 4.0;
	else pix.blue = ( t - 0.5 ) * 4.0;

	if ( t >= 0.25 && t <= 0.75 ) pix.red = 1.0;
	else if ( t < 0.5 ) pix.red = t * 4.0;
	else pix.red = ( 1.0 - t ) * 4.0;
	return Color( pix );
}
void NdcToScreen( float64 _inX, float64 _inY, uint32& _outX, uint32& _outY )
{
	_outX = ( uint32 )( ( int32 )( ( ( 0.5 ) * ( 1.0 + _inX ) ) * ( ( float64 )( RASTER_WIDTH - 1u ) ) ) );
	_outY = ( uint32 )( ( int32 )( ( ( 0.5 ) * ( 1.0 - _inY ) ) * ( ( float64 )( RASTER_HEIGHT - 1u ) ) ) );
}

int main( int, char** )
{
	float64 tA = 0.0, tB = 0.0, x, y;
	static const float64 dtA = 0.5 / markiplier;
	static const float64 dtB = 0.0002 / markiplier;
	uint64 count = 0ull;
	uint32 sX, sY;
	BitmapImage bmpOut( RASTER_WIDTH, RASTER_HEIGHT );
	for ( uint32 i = 0u; i < RASTER_HEIGHT; ++i )
		for ( uint32 j = 0u; j < RASTER_WIDTH; ++j )
			bmpOut.SetPixel( j, i, Color::BLACK );
	Avi aviOut( "videoOut.avi" );
	static const uint64 numFrames = 120ull;
	while ( count <= maxCount )
	{
		x = Math::Sin_Rad( tA * VALL ) * Math::Cos_Rad( tB );
		y = Math::Cos_Rad( tA ) * Math::Sin_Rad( tB * 2.5 );
		NdcToScreen( x * 0.9, y * 0.9, sX, sY );
		if ( sX < RASTER_WIDTH && sY < RASTER_HEIGHT )
			bmpOut.SetPixel( sX, sY, getColor( count ) );
		tA += dtA;
		tB += dtB;
		++count;
		if ( 0ull == count % ( maxCount / numFrames ) )
		{
			bmpOut.SaveImage( "outframe.bmp" );
			aviOut.AddFrame( "outframe.bmp" );
		}
	}
	return EXIT_SUCCESS;
	// aviOut.AddFrame( BitmapImage::SaveFrame( Raster::GetTexture() ) );
}
#endif