#pragma once
class Fuzzy
{
private:
	float64 m_fuzzy;
public:
	Fuzzy( void );
	Fuzzy( bool );
	Fuzzy( float64 );
	Fuzzy( const Fuzzy& );
	Fuzzy( Fuzzy&& );
	Fuzzy& operator=( const Fuzzy& );
	Fuzzy& operator=( Fuzzy&& );
	~Fuzzy( void );
	Fuzzy operator!( void ) const;
	Fuzzy operator&&( const Fuzzy& ) const;
	Fuzzy operator||( const Fuzzy& ) const;
	bool GetBool( float64 = 0.5 ) const;
	float64 GetValue( void ) const;
	Fuzzy& SetValue( float64 );
	operator bool( void ) const;
	operator float64( void ) const;
	Fuzzy operator==( const Fuzzy& ) const;
	Fuzzy operator!=( const Fuzzy& ) const;
};