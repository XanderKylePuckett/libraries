#include "stdafx.h"
#include "Mesh.h"

Mesh::Mesh( void )
{
}
Mesh::Mesh( const Mesh& _rhs ) :
	m_triangles( _rhs.m_triangles )
{
}
Mesh::Mesh( Mesh&& _rhs ) :
	m_triangles( std::move( _rhs.m_triangles ) )
{
}
Mesh& Mesh::operator=( const Mesh& _rhs )
{
	if ( this != &_rhs )
	{
		m_triangles = _rhs.m_triangles;
	}
	return *this;
}
Mesh& Mesh::operator=( Mesh&& _rhs )
{
	if ( this != &_rhs )
	{
		m_triangles = std::move( _rhs.m_triangles );
	}
	return *this;
}
Mesh::~Mesh( void )
{
}
void Mesh::AddTriangle( const PositionColorTriangle& _triangle )
{
	m_triangles.AddItem( _triangle );
}
Mesh& Mesh::operator+=( const PositionColorTriangle& _triangle )
{
	m_triangles.AddItem( _triangle );
	return *this;
}
Mesh Mesh::operator+( const Mesh& _rhs ) const
{
	Mesh outMesh_ = *this;
	outMesh_ += _rhs;
	return outMesh_;
}
Mesh& Mesh::operator+=( const Mesh& _rhs )
{
	m_triangles += _rhs.m_triangles;
	return *this;
}
const DynArray<PositionColorTriangle>& Mesh::GetTriangles( void ) const
{
	return m_triangles;
}
void Mesh::ClearTriangles( void )
{
	m_triangles.Clear();
}