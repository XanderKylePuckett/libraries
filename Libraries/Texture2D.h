#pragma once
#include "Color.h"

class Texture2D
{
	friend class Raster;
	friend class BitmapImage;

private:
	Color* m_pixels;
	uint32 m_width, m_height, m_numPixels;
	bool m_sizeLocked = false;

public:
	Texture2D( void );
	Texture2D( uint32 _squareSize, const Color& _initColor = Color::ZERO );
	Texture2D( uint32 _width, uint32 _height, const Color& _initColor = Color::ZERO );
	Texture2D( const Texture2D& _rhs );
	Texture2D( Texture2D&& _rhs );
	Texture2D& operator=( const Texture2D& _rhs );
	Texture2D& operator=( Texture2D&& _rhs );
	Texture2D& operator=( const Color& _rhs );
	~Texture2D( void );
	uint32 GetWidth( void ) const;
	uint32 GetHeight( void ) const;
	uint32 GetNumPixels( void ) const;
	void SetSize( uint32 _width, uint32 _height, const Color& _initColor = Color::ZERO );
	void SetSize_Copy( uint32 _width, uint32 _height, const Color& _fillColor = Color::ZERO );
	const Color* GetPixels( void ) const;
	void SetPixel( uint32 _x, uint32 _y, const Color& _color );
	void BlendPixel( uint32 _x, uint32 _y, const Color& _color );
	Color GetPixel( uint32 _x, uint32 _y ) const;
	const Color& GetPixel_Ref( uint32 _x, uint32 _y ) const;
	Color& GetPixel_Ref( uint32 _x, uint32 _y );
	static void TextureToTexture( const Texture2D& _from, Texture2D& _to );
	static void TextureToTexture( const Texture2D& _from, Texture2D& _to, uint32 _x, uint32 _y );
	void SetPixelNdc( float64 _x, float64 _y, const Color& _color );
	void NdcToScreen( float64 _inX, float64 _inY, int32& _outX, int32& _outY ) const;
	void NdcToScreen( float64 _inX, float64 _inY, uint32& _outX, uint32& _outY ) const;
	void WriteToBmp( const std::string& ) const;
	static Texture2D GetTexture2DFromBmp( const std::string& );
	Texture2D( const std::string& );
};