#include "stdafx.h"
#include "Rasterizer.h"
#include "Math.h"
#include "Raster.h"

static inline float64 ImplicitLineEquation( const Vector3& _point, const Line& _line )
{
	return ( _line.m_start.m_position.y - _line.m_end.m_position.y ) * _point.x +
		( _line.m_start.m_position.x - _line.m_end.m_position.x ) * _point.y +
		_line.m_start.m_position.x * _line.m_end.m_position.y - _line.m_start.m_position.y * _line.m_end.m_position.x;
}

void DrawLine( Texture2D& _texture, const Line& _line )
{
	RasterLine rasterLine_;
	_texture.NdcToScreen( _line.m_start.m_position.x, _line.m_start.m_position.y,
						  rasterLine_.m_start.m_posX, rasterLine_.m_start.m_posY );
	_texture.NdcToScreen( _line.m_end.m_position.x, _line.m_end.m_position.y,
						  rasterLine_.m_end.m_posX, rasterLine_.m_end.m_posY );
	DrawRasterLine( _texture, rasterLine_, _line.m_start.m_color, _line.m_end.m_color );
}

void DrawRasterPoint( Texture2D& _texture, const RasterPoint& _rasterPoint, const Color& _color )
{
	_texture.SetPixel( ( uint32 )_rasterPoint.m_posX, ( uint32 )_rasterPoint.m_posY, _color );
}

void DrawRasterLine( Texture2D& _texture, const RasterLine& _rasterLine, const Color& _color )
{
	const float64 largest_ = ( float64 )Math::Max( ( float64 )Math::Abs( ( float64 )_rasterLine.m_end.m_posX - ( float64 )_rasterLine.m_start.m_posX ),
		( float64 )Math::Abs( ( float64 )_rasterLine.m_end.m_posY - ( float64 )_rasterLine.m_start.m_posY ) );
	float64 ratio_;
	for ( float64 i = 0.0; ( float64 )i <= ( float64 )largest_; i += 0.5 )
	{
		ratio_ = ( float64 )i / ( float64 )largest_;
		_texture.SetPixel(
			( uint32 )Math::Lerp( ( float64 )_rasterLine.m_start.m_posX, ( float64 )_rasterLine.m_end.m_posX, ratio_ ),
			( uint32 )Math::Lerp( ( float64 )_rasterLine.m_start.m_posY, ( float64 )_rasterLine.m_end.m_posY, ratio_ ), _color );
	}
}

void DrawRasterLine( Texture2D& _texture, const RasterLine& _rasterLine,
					 const Color& _startColor, const Color& _endColor )
{
	const float64 largest_ = ( float64 )Math::Max( ( float64 )Math::Abs( ( float64 )_rasterLine.m_end.m_posX - ( float64 )_rasterLine.m_start.m_posX ),
		( float64 )Math::Abs( ( float64 )_rasterLine.m_end.m_posY - ( float64 )_rasterLine.m_start.m_posY ) );
	float64 ratio_;
	for ( float64 i = 0.0; ( float64 )i <= ( float64 )largest_; i += 0.5 )
	{
		ratio_ = ( float64 )i / ( float64 )largest_;
		_texture.SetPixel(
			( uint32 )Math::Lerp( ( float64 )_rasterLine.m_start.m_posX, ( float64 )_rasterLine.m_end.m_posX, ratio_ ),
			( uint32 )Math::Lerp( ( float64 )_rasterLine.m_start.m_posY, ( float64 )_rasterLine.m_end.m_posY, ratio_ ),
#if USE_SQRT_FORMULA
			Math::Lerp_SqrtFormula( _startColor, _endColor, ratio_ ) );
#else
			Math::Lerp( _startColor, _endColor, ratio_ ) );
#endif
	}
}
void DrawLine( const Line& _l )
{
	DrawLine( Raster::GetTexture(), _l );
}
void DrawRasterPoint( const RasterPoint& _rp, const Color& _c )
{
	DrawRasterPoint( Raster::GetTexture(), _rp, _c );
}
void DrawRasterLine( const RasterLine& _rl, const Color& _c )
{
	DrawRasterLine( Raster::GetTexture(), _rl, _c );
}
void DrawRasterLine( const RasterLine& _rl, const Color& _c1, const Color& _c2 )
{
	DrawRasterLine( Raster::GetTexture(), _rl, _c1, _c2 );
}