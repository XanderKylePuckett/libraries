#include "stdafx.h"
#include "WordDictionary.h"

#define NUM_BUCKETS (1995u)
#define BUFFER_SIZE (128u)

WordDictionary::WordDictionary( void ) :
	m_words( NUM_BUCKETS, [ ] ( const String& _str ) -> uint32
{
	int64 buck_ = 0ll;
	int64 len_ = ( int64 )_str.GetLength();
	for ( int64 i = 0ll; i < len_; ++i ) buck_
		+= ( ( int64 )_str[ ( uint32 )i ] )
		* ( ( int64 )_str[ ( uint32 )i ] + i * i )
		* ( ( int64 )_str[ ( uint32 )i ] - i * i )
		* ( ( int64 )_str[ ( uint32 )i ] + i )
		* ( ( int64 )_str[ ( uint32 )i ] - i );
	return ( uint32 )( ( ( uint64 )( ( int64 )( buck_ ) ) ) % ( ( uint64 )( NUM_BUCKETS ) ) );
} )
{
}
WordDictionary::WordDictionary( const WordDictionary& _rhs ) :
	m_words( _rhs.m_words )
{
}
WordDictionary::WordDictionary( WordDictionary&& _rhs ) :
	m_words( std::move( _rhs.m_words ) )
{
}
WordDictionary& WordDictionary::operator=( const WordDictionary& _rhs )
{
	if ( this != &_rhs )
	{
		m_words = _rhs.m_words;
	}
	return *this;
}
WordDictionary& WordDictionary::operator=( WordDictionary&& _rhs )
{
	if ( this != &_rhs )
	{
		m_words = std::move( _rhs.m_words );
	}
	return *this;
}
WordDictionary& WordDictionary::operator+=( const WordDictionary& _rhs )
{
	const DynArray<String>* table_ = _rhs.m_words.GetTable();
	for ( uint32 i = 0u; i < NUM_BUCKETS; ++i )
		for ( uint32 j = 0u; j < table_[ i ].GetSize(); ++j )
			AddWord( table_[ i ][ j ] );
	return *this;
}
WordDictionary::~WordDictionary( void )
{
}
void WordDictionary::AddWord( const String& _word )
{
	if ( !m_words.Has( _word ) )
		m_words.Add( _word );
}
bool WordDictionary::HasWord( const String& _word ) const
{
	return m_words.Has( _word );
}
WordDictionary WordDictionary::Import(
	const char* const _file,
	bool( *_validator )( const String& ),
	void( *_conformer )( String& ) )
{
	WordDictionary dictionary_;
	std::ifstream file_;
	file_.open( _file, std::ios_base::in );
	String str_;
	if ( file_.is_open() )
	{
		char buffer_[ BUFFER_SIZE ];
		while ( !file_.eof() )
		{
			file_.getline( buffer_, BUFFER_SIZE, '\n' );
			str_ = buffer_;
			if ( _validator( str_.OptimizeSpace() ) )
			{
				_conformer( str_.OptimizeSpace() );
				dictionary_.AddWord( str_.OptimizeSpace() );
			}
		}
		file_.close();
	}
	return dictionary_;
}
WordDictionary WordDictionary::Import(
	const char* const _file,
	void( *_conformer )( String& ),
	bool( *_validator )( const String& ) )
{
	WordDictionary dictionary_;
	std::ifstream file_;
	file_.open( _file, std::ios_base::in );
	String str_;
	if ( file_.is_open() )
	{
		char buffer_[ BUFFER_SIZE ];
		while ( !file_.eof() )
		{
			file_.getline( buffer_, BUFFER_SIZE, '\n' );
			str_ = buffer_;
			_conformer( str_.OptimizeSpace() );
			if ( _validator( str_.OptimizeSpace() ) )
				dictionary_.AddWord( str_.OptimizeSpace() );
		}
		file_.close();
	}
	return dictionary_;
}
void WordDictionary::Export( const WordDictionary& _dict, const char* const _file, bool _append )
{
	std::ofstream file_;
	file_.open( _file, std::ios_base::out |
		( ( _append ) ? ( std::ios_base::app ) : ( std::ios_base::trunc ) ) );
	const DynArray<String>* table_ = _dict.m_words.GetTable();
	if ( file_.is_open() )
		for ( uint32 i = 0u; i < NUM_BUCKETS; ++i )
			for ( uint32 j = 0u; j < table_[ i ].GetSize(); ++j )
				file_ << table_[ i ][ j ] << '\n';
	else return;
	file_.close();
}
WordDictionary& WordDictionary::OptimizeSpace( void )
{
	m_words.OptimizeSpace();
	return *this;
}